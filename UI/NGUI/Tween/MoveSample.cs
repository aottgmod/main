using UnityEngine;

public class MoveSample : MonoBehaviour
{
    private void Start()
    {
        object[] args = { "x", 2, "easeType", "easeInOutExpo", "loopType", "pingPong", "delay", 0.1 };
        iTween.MoveBy(target: gameObject, iTween.Hash(args: args));
    }
}