using System;
using System.Collections;
using UnityEngine;

public class BTN_save_snapshot : MonoBehaviour
{
    public GameObject info;
    public GameObject targetTexture;
    public GameObject[] thingsNeedToHide;

    private void OnClick()
    {
        var array = thingsNeedToHide;
        for (var i = 0; i < array.Length; i++)
            array[i].transform.position += Vector3.up * 10000f;

        StartCoroutine(ScreenshotEncode());
        info.GetComponent<UILabel>().text = "trying..";
    }

    private IEnumerator ScreenshotEncode()
    {
        yield return new WaitForEndOfFrame();
        var num = Screen.height / 600f;
        var texture2D = new Texture2D((int)(num * targetTexture.transform.localScale.x), (int)(num * targetTexture.transform.localScale.y), format: TextureFormat.RGB24, false);
        texture2D.ReadPixels(new Rect(Screen.width * 0.5f - texture2D.width * 0.5f, Screen.height * 0.5f - texture2D.height * 0.5f - num * 0f, width: texture2D.width, height: texture2D.height), 0, 0);
        texture2D.Apply();
        yield return 0;
        foreach (var gameObject in thingsNeedToHide)
            gameObject.transform.position -= Vector3.up * 10000f;

        var textArray = new[]
        {
            "aottg_ss-",  DateTime.Today.Month.ToString(),
            "_",
            DateTime.Today.Day.ToString(),
            "_",
            DateTime.Today.Year.ToString(),
            "-",
            DateTime.Now.Hour.ToString(),
            "_",
            DateTime.Now.Minute.ToString(),
            "_",
            DateTime.Now.Second.ToString(),
            ".png"
        };
        var text = string.Concat(values: textArray);
        var args = new object[]
        {
            text,
            texture2D.width,
            texture2D.height,
            Convert.ToBase64String(texture2D.EncodeToPNG())
        };
        Application.ExternalCall("SaveImg", args: args);
        DestroyObject(obj: texture2D);
    }
}