using UnityEngine;

public class PopuplistCharacterSelection : MonoBehaviour
{
    public GameObject ACL;
    public GameObject BLA;
    public GameObject GAS;
    public GameObject SPD;

    private void onCharacterChange()
    {
        HeroStat stat;
        var selection = GetComponent<UIPopupList>().selection;
        if (!(selection == "Set 1") && !(selection == "Set 2") && !(selection == "Set 3"))
        {
            stat = HeroStat.getInfo(name: GetComponent<UIPopupList>().selection);
        }
        else
        {
            var costume = CostumeConeveter.LocalDataToHeroCostume(selection.ToUpper());
            if (costume == null)
                stat = new HeroStat();
            else
                stat = costume.stat;
        }

        SPD.transform.localScale = new Vector3(x: stat.SPD, 20f, 0f);
        GAS.transform.localScale = new Vector3(x: stat.GAS, 20f, 0f);
        BLA.transform.localScale = new Vector3(x: stat.BLA, 20f, 0f);
        ACL.transform.localScale = new Vector3(stat.ACL, 20f, 0f);
    }
}