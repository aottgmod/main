using UnityEngine;

public class SynchronizeLights : MonoBehaviour
{
    public Light light0;
    public Light light1;

    private void LateUpdate()
    {
        if (light0 != null)
        {
            var vector = light0.transform.rotation * new Vector3(0f, 0f, -1f);
            renderer.material.SetVector("_LightDirection0", new Vector4(x: vector.x, y: vector.y, z: vector.z, 0f));
            renderer.material.SetColor("_MyLightColor0", color: light0.color);
        }

        if (light1 != null)
        {
            var vector2 = light1.transform.rotation * new Vector3(0f, 0f, -1f);
            renderer.material.SetVector("_LightDirection1", new Vector4(x: vector2.x, y: vector2.y, z: vector2.z, 0f));
            renderer.material.SetColor("_MyLightColor1", color: light1.color);
        }
    }
}