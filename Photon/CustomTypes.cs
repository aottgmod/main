using ExitGames.Client.Photon;
using UnityEngine;

internal static class CustomTypes
{
    private static object DeserializePhotonPlayer(byte[] bytes)
    {
        int num2;
        var offset = 0;
        Protocol.Deserialize(value: out num2, source: bytes, offset: ref offset);
        if (PhotonNetwork.networkingPeer.mActors.ContainsKey(key: num2))
            return PhotonNetwork.networkingPeer.mActors[key: num2];
        return null;
    }

    private static object DeserializeQuaternion(byte[] bytes)
    {
        var quaternion = new Quaternion();
        var offset = 0;
        Protocol.Deserialize(value: out quaternion.w, source: bytes, offset: ref offset);
        Protocol.Deserialize(value: out quaternion.x, source: bytes, offset: ref offset);
        Protocol.Deserialize(value: out quaternion.y, source: bytes, offset: ref offset);
        Protocol.Deserialize(value: out quaternion.z, source: bytes, offset: ref offset);
        return quaternion;
    }

    private static object DeserializeVector2(byte[] bytes)
    {
        var vector = new Vector2();
        var offset = 0;
        Protocol.Deserialize(value: out vector.x, source: bytes, offset: ref offset);
        Protocol.Deserialize(value: out vector.y, source: bytes, offset: ref offset);
        return vector;
    }

    private static object DeserializeVector3(byte[] bytes)
    {
        var vector = new Vector3();
        var offset = 0;
        Protocol.Deserialize(value: out vector.x, source: bytes, offset: ref offset);
        Protocol.Deserialize(value: out vector.y, source: bytes, offset: ref offset);
        Protocol.Deserialize(value: out vector.z, source: bytes, offset: ref offset);
        return vector;
    }

    internal static void Register()
    {
        PhotonPeer.RegisterType(typeof(Vector2), 0x57, serializeMethod: SerializeVector2, constructor: DeserializeVector2);
        PhotonPeer.RegisterType(typeof(Vector3), 0x56, serializeMethod: SerializeVector3, constructor: DeserializeVector3);
        PhotonPeer.RegisterType(typeof(Quaternion), 0x51, serializeMethod: SerializeQuaternion, constructor: DeserializeQuaternion);
        PhotonPeer.RegisterType(typeof(PhotonPlayer), 80, serializeMethod: SerializePhotonPlayer, constructor: DeserializePhotonPlayer);
    }

    private static byte[] SerializePhotonPlayer(object customobject)
    {
        var iD = ((PhotonPlayer)customobject).ID;
        var target = new byte[4];
        var targetOffset = 0;
        Protocol.Serialize(value: iD, target: target, targetOffset: ref targetOffset);
        return target;
    }

    private static byte[] SerializeQuaternion(object obj)
    {
        var quaternion = (Quaternion)obj;
        var target = new byte[0x10];
        var targetOffset = 0;
        Protocol.Serialize(value: quaternion.w, target: target, targetOffset: ref targetOffset);
        Protocol.Serialize(value: quaternion.x, target: target, targetOffset: ref targetOffset);
        Protocol.Serialize(value: quaternion.y, target: target, targetOffset: ref targetOffset);
        Protocol.Serialize(value: quaternion.z, target: target, targetOffset: ref targetOffset);
        return target;
    }

    private static byte[] SerializeVector2(object customobject)
    {
        var vector = (Vector2)customobject;
        var target = new byte[8];
        var targetOffset = 0;
        Protocol.Serialize(value: vector.x, target: target, targetOffset: ref targetOffset);
        Protocol.Serialize(value: vector.y, target: target, targetOffset: ref targetOffset);
        return target;
    }

    private static byte[] SerializeVector3(object customobject)
    {
        var vector = (Vector3)customobject;
        var targetOffset = 0;
        var target = new byte[12];
        Protocol.Serialize(value: vector.x, target: target, targetOffset: ref targetOffset);
        Protocol.Serialize(value: vector.y, target: target, targetOffset: ref targetOffset);
        Protocol.Serialize(value: vector.z, target: target, targetOffset: ref targetOffset);
        return target;
    }
}