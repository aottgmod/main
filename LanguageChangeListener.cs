using UnityEngine;

public class LanguageChangeListener : MonoBehaviour
{
    private void OnSelectionChange()
    {
        Language.type = Language.GetLangIndex(txt: GetComponent<UIPopupList>().selection);
        PlayerPrefs.SetInt("language", value: Language.type);
    }

    private void Start()
    {
        if (Language.type == -1)
        {
            if (PlayerPrefs.HasKey("language"))
            {
                Language.type = PlayerPrefs.GetInt("language");
            }
            else
            {
                PlayerPrefs.SetInt("language", 0);
                Language.type = 0;
            }

            Language.init();
            GetComponent<UIPopupList>().selection = Language.GetLang(id: Language.type);
        }
        else
        {
            GetComponent<UIPopupList>().selection = Language.GetLang(id: Language.type);
        }
    }
}