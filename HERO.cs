using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Xft;
using Hashtable = ExitGames.Client.Photon.Hashtable;
using MonoBehaviour = Photon.MonoBehaviour;
using Random = UnityEngine.Random;

public class HERO : MonoBehaviour
{
    private readonly int bulletMAX = 7;
    private readonly float flareTotalCD = 30f;
    private readonly float gravity = 20f;
    private readonly int totalBladeNum = 5;
    private readonly float useGasSpeed = 0.2f;
    private HERO_STATE _state;
    private bool almostSingleHook;
    private string attackAnimation;
    private int attackLoop;
    private bool attackMove;
    private bool attackReleased;
    public AudioSource audio_ally;
    public AudioSource audio_hitwall;
    private GameObject badGuy;
    public Animation baseAnimation;
    public Rigidbody baseRigidBody;
    public Transform baseTransform;
    public bool bigLean;
    public float bombCD;
    public bool bombImmune;
    public float bombRadius;
    public float bombSpeed;
    public float bombTime;
    public float bombTimeMax;
    private float buffTime;
    public GameObject bulletLeft;
    public GameObject bulletRight;
    private bool buttonAttackRelease;
    public Dictionary<string, UISprite> cachedSprites;
    public float CameraMultiplier;
    public bool canJump = true;
    public GameObject checkBoxLeft;
    public GameObject checkBoxRight;
    public GameObject cross1;
    public GameObject cross2;
    public GameObject crossL1;
    public GameObject crossL2;
    public GameObject crossR1;
    public GameObject crossR2;
    public string currentAnimation;
    private int currentBladeNum = 5;
    private float currentBladeSta = 100f;
    private BUFF currentBuff;
    public Camera currentCamera;
    private float currentGas = 100f;
    public float currentSpeed;
    public Vector3 currentV;
    private bool dashD;
    private Vector3 dashDirection;
    private bool dashL;
    private bool dashR;
    private float dashTime;
    private bool dashU;
    private Vector3 dashV;
    public bool detonate;
    private float dTapTime = -1f;
    private bool EHold;
    private GameObject eren_titan;
    private int escapeTimes = 1;
    private float facingDirection;
    private float flare1CD;
    private float flare2CD;
    private float flare3CD;
    private Transform forearmL;
    private Transform forearmR;
    private bool grounded;
    private GameObject gunDummy;
    private Vector3 gunTarget;
    private Transform handL;
    private Transform handR;
    private bool hasDied;
    public bool hasspawn;
    private bool hookBySomeOne = true;
    public GameObject hookRefL1;
    public GameObject hookRefL2;
    public GameObject hookRefR1;
    public GameObject hookRefR2;
    private bool hookSomeOne;
    private GameObject hookTarget;
    public FengCustomInputs inputManager;
    private float invincible = 3f;
    public bool isCannon;
    private bool isLaunchLeft;
    private bool isLaunchRight;
    private bool isLeftHandHooked;
    private bool isMounted;
    public bool isPhotonCamera;
    private bool isRightHandHooked;
    public float jumpHeight = 2f;
    private bool justGrounded;
    public GameObject LabelDistance;
    public Transform lastHook;
    private float launchElapsedTimeL;
    private float launchElapsedTimeR;
    private Vector3 launchForce;
    private Vector3 launchPointLeft;
    private Vector3 launchPointRight;
    private bool leanLeft;
    private bool leftArmAim;
    public XWeaponTrail leftbladetrail;
    public XWeaponTrail leftbladetrail2;
    private int leftBulletLeft = 7;
    private bool leftGunHasBullet = true;
    private float lTapTime = -1f;
    public GameObject maincamera;
    public float maxVelocityChange = 10f;
    public AudioSource meatDie;
    public Bomb myBomb;
    public GameObject myCannon;
    public Transform myCannonBase;
    public Transform myCannonPlayer;
    public CannonPropRegion myCannonRegion;
    public GROUP myGroup;
    private GameObject myHorse;
    public GameObject myNetWorkName;
    public float myScale = 1f;
    public int myTeam = 1;
    public List<TITAN> myTitans;
    private bool needLean;
    private Quaternion oldHeadRotation;
    private float originVM;
    private bool QHold;
    private string reloadAnimation = string.Empty;
    private bool rightArmAim;
    public XWeaponTrail rightbladetrail;
    public XWeaponTrail rightbladetrail2;
    private int rightBulletLeft = 7;
    private bool rightGunHasBullet = true;
    public AudioSource rope;
    private float rTapTime = -1f;
    public HERO_SETUP setup;
    private GameObject skillCD;
    public float skillCDDuration;
    public float skillCDLast;
    public float skillCDLastCannon;
    private string skillId;
    public string skillIDHUD;
    public AudioSource slash;
    public AudioSource slashHit;
    private ParticleSystem smoke_3dmg;
    private ParticleSystem sparks;
    public float speed = 10f;
    public GameObject speedFX;
    public GameObject speedFX1;
    private ParticleSystem speedFXPS;
    public bool spinning;
    private string standAnimation = "stand";
    private Quaternion targetHeadRotation;
    private Quaternion targetRotation;
    public Vector3 targetV;
    private bool throwedBlades;
    public bool titanForm;
    private GameObject titanWhoGrabMe;
    private int titanWhoGrabMeID;
    public float totalBladeSta = 100f;
    public float totalGas = 100f;
    private Transform upperarmL;
    private Transform upperarmR;
    public bool useGun;
    private float uTapTime = -1f;
    private bool wallJump;
    private float wallRunTime;

    public bool isGrabbed => state == HERO_STATE.Grab;

    private HERO_STATE state
    {
        get => _state;
        set
        {
            if (_state == HERO_STATE.AirDodge || _state == HERO_STATE.GroundDodge)
                dashTime = 0f;
            _state = value;
        }
    }

    private void applyForceToBody(GameObject GO, Vector3 v)
    {
        GO.rigidbody.AddForce(force: v);
        GO.rigidbody.AddTorque(Random.Range(-10f, 10f), Random.Range(-10f, 10f), Random.Range(-10f, 10f));
    }

    public void attackAccordingToMouse()
    {
        if (Input.mousePosition.x < Screen.width * 0.5)
            attackAnimation = "attack2";
        else
            attackAnimation = "attack1";
    }

    public void attackAccordingToTarget(Transform a)
    {
        var vector = a.position - transform.position;
        var current = -Mathf.Atan2(y: vector.z, x: vector.x) * 57.29578f;
        var f = -Mathf.DeltaAngle(current: current, transform.rotation.eulerAngles.y - 90f);
        if (Mathf.Abs(f: f) < 90f && vector.magnitude < 6f && a.position.y <= transform.position.y + 2f && a.position.y >= transform.position.y - 5f)
            attackAnimation = "attack4";
        else if (f > 0f)
            attackAnimation = "attack1";
        else
            attackAnimation = "attack2";
    }

    private void Awake()
    {
        cache();
        setup = gameObject.GetComponent<HERO_SETUP>();
        baseRigidBody.freezeRotation = true;
        baseRigidBody.useGravity = false;
        handL = baseTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L");
        handR = baseTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R");
        forearmL = baseTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L");
        forearmR = baseTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R");
        upperarmL = baseTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L");
        upperarmR = baseTransform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R");
    }

    public void backToHuman()
    {
        gameObject.GetComponent<SmoothSyncMovement>().disabled = false;
        rigidbody.velocity = Vector3.zero;
        titanForm = false;
        ungrabbed();
        falseAttack();
        skillCDDuration = skillCDLast;
        GameObject.Find("MainCamera").GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj: gameObject);
        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE)
            photonView.RPC("backToHumanRPC", target: PhotonTargets.Others);
    }

    [RPC]
    private void backToHumanRPC()
    {
        titanForm = false;
        eren_titan = null;
        gameObject.GetComponent<SmoothSyncMovement>().disabled = false;
    }

    [RPC]
    public void badGuyReleaseMe()
    {
        hookBySomeOne = false;
        badGuy = null;
    }

    [RPC]
    public void blowAway(Vector3 force)
    {
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
        {
            rigidbody.AddForce(force: force, mode: ForceMode.Impulse);
            transform.LookAt(worldPosition: transform.position);
        }
    }

    private void bodyLean()
    {
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
        {
            var z = 0f;
            needLean = false;
            if (!useGun && state == HERO_STATE.Attack && attackAnimation != "attack3_1" && attackAnimation != "attack3_2")
            {
                var y = rigidbody.velocity.y;
                var x = rigidbody.velocity.x;
                var num4 = rigidbody.velocity.z;
                var num5 = Mathf.Sqrt(x * x + num4 * num4);
                var num6 = Mathf.Atan2(y: y, x: num5) * 57.29578f;
                targetRotation = Quaternion.Euler(-num6 * (1f - Vector3.Angle(from: rigidbody.velocity, to: transform.forward) / 90f), y: facingDirection, 0f);
                if (isLeftHandHooked && bulletLeft != null || isRightHandHooked && bulletRight != null)
                    transform.rotation = targetRotation;
            }
            else
            {
                if (isLeftHandHooked && bulletLeft != null && isRightHandHooked && bulletRight != null)
                {
                    if (almostSingleHook)
                    {
                        needLean = true;
                        z = getLeanAngle(p: bulletRight.transform.position, true);
                    }
                }
                else if (isLeftHandHooked && bulletLeft != null)
                {
                    needLean = true;
                    z = getLeanAngle(p: bulletLeft.transform.position, true);
                }
                else if (isRightHandHooked && bulletRight != null)
                {
                    needLean = true;
                    z = getLeanAngle(p: bulletRight.transform.position, false);
                }

                if (needLean)
                {
                    var a = 0f;
                    if (!useGun && state != HERO_STATE.Attack)
                    {
                        a = currentSpeed * 0.1f;
                        a = Mathf.Min(a: a, 20f);
                    }

                    targetRotation = Quaternion.Euler(x: -a, y: facingDirection, z: z);
                }
                else if (state != HERO_STATE.Attack)
                {
                    targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                }
            }
        }
    }

    public void bombInit()
    {
        skillIDHUD = skillId;
        skillCDDuration = skillCDLast;
        if (RCSettings.bombMode == 1)
        {
            var num = (int)FengGameManagerMKII.settings[250];
            var num2 = (int)FengGameManagerMKII.settings[0xfb];
            var num3 = (int)FengGameManagerMKII.settings[0xfc];
            var num4 = (int)FengGameManagerMKII.settings[0xfd];
            if (num < 0 || num > 10)
            {
                num = 5;
                FengGameManagerMKII.settings[250] = 5;
            }

            if (num2 < 0 || num2 > 10)
            {
                num2 = 5;
                FengGameManagerMKII.settings[0xfb] = 5;
            }

            if (num3 < 0 || num3 > 10)
            {
                num3 = 5;
                FengGameManagerMKII.settings[0xfc] = 5;
            }

            if (num4 < 0 || num4 > 10)
            {
                num4 = 5;
                FengGameManagerMKII.settings[0xfd] = 5;
            }

            if (num + num2 + num3 + num4 > 20)
            {
                num = 5;
                num2 = 5;
                num3 = 5;
                num4 = 5;
                FengGameManagerMKII.settings[250] = 5;
                FengGameManagerMKII.settings[0xfb] = 5;
                FengGameManagerMKII.settings[0xfc] = 5;
                FengGameManagerMKII.settings[0xfd] = 5;
            }

            bombTimeMax = (num2 * 60f + 200f) / (num3 * 60f + 200f);
            bombRadius = num * 4f + 20f;
            bombCD = num4 * -0.4f + 5f;
            bombSpeed = num3 * 60f + 200f;
            var propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.RCBombR, (float)FengGameManagerMKII.settings[0xf6]);
            propertiesToSet.Add(key: PhotonPlayerProperty.RCBombG, (float)FengGameManagerMKII.settings[0xf7]);
            propertiesToSet.Add(key: PhotonPlayerProperty.RCBombB, (float)FengGameManagerMKII.settings[0xf8]);
            propertiesToSet.Add(key: PhotonPlayerProperty.RCBombA, (float)FengGameManagerMKII.settings[0xf9]);
            propertiesToSet.Add(key: PhotonPlayerProperty.RCBombRadius, value: bombRadius);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
            skillId = "bomb";
            skillIDHUD = "armin";
            skillCDLast = bombCD;
            skillCDDuration = 10f;
            if (FengGameManagerMKII.instance.roundTime > 10f)
                skillCDDuration = 5f;
        }
    }

    private void breakApart(Vector3 v, bool isBite)
    {
        GameObject obj6;
        GameObject obj7;
        GameObject obj8;
        GameObject obj9;
        GameObject obj10;
        var obj2 = (GameObject)Instantiate(Resources.Load("Character_parts/AOTTG_HERO_body"), position: this.transform.position, rotation: this.transform.rotation);
        obj2.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
        obj2.GetComponent<HERO_DEAD_BODY_SETUP>().init(aniname: currentAnimation, time: animation[name: currentAnimation].normalizedTime, part: BODY_PARTS.ARM_R);
        if (!isBite)
        {
            var gO = (GameObject)Instantiate(Resources.Load("Character_parts/AOTTG_HERO_body"), position: this.transform.position, rotation: this.transform.rotation);
            var obj4 = (GameObject)Instantiate(Resources.Load("Character_parts/AOTTG_HERO_body"), position: this.transform.position, rotation: this.transform.rotation);
            var obj5 = (GameObject)Instantiate(Resources.Load("Character_parts/AOTTG_HERO_body"), position: this.transform.position, rotation: this.transform.rotation);
            gO.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            obj4.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            obj5.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            gO.GetComponent<HERO_DEAD_BODY_SETUP>().init(aniname: currentAnimation, time: animation[name: currentAnimation].normalizedTime, part: BODY_PARTS.UPPER);
            obj4.GetComponent<HERO_DEAD_BODY_SETUP>().init(aniname: currentAnimation, time: animation[name: currentAnimation].normalizedTime, part: BODY_PARTS.LOWER);
            obj5.GetComponent<HERO_DEAD_BODY_SETUP>().init(aniname: currentAnimation, time: animation[name: currentAnimation].normalizedTime, part: BODY_PARTS.ARM_L);
            applyForceToBody(GO: gO, v: v);
            applyForceToBody(GO: obj4, v: v);
            applyForceToBody(GO: obj5, v: v);
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
                currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj: gO, false);
        }
        else if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
        {
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj: obj2, false);
        }

        applyForceToBody(GO: obj2, v: v);
        var transform = this.transform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L").transform;
        var transform2 = this.transform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R").transform;
        if (useGun)
        {
            obj6 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_l"), position: transform.position, rotation: transform.rotation);
            obj7 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_r"), position: transform2.position, rotation: transform2.rotation);
            obj8 = (GameObject)Instantiate(Resources.Load("Character_parts/character_3dmg_2"), position: this.transform.position, rotation: this.transform.rotation);
            obj9 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_mag_l"), position: this.transform.position, rotation: this.transform.rotation);
            obj10 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_mag_r"), position: this.transform.position, rotation: this.transform.rotation);
        }
        else
        {
            obj6 = (GameObject)Instantiate(Resources.Load("Character_parts/character_blade_l"), position: transform.position, rotation: transform.rotation);
            obj7 = (GameObject)Instantiate(Resources.Load("Character_parts/character_blade_r"), position: transform2.position, rotation: transform2.rotation);
            obj8 = (GameObject)Instantiate(Resources.Load("Character_parts/character_3dmg"), position: this.transform.position, rotation: this.transform.rotation);
            obj9 = (GameObject)Instantiate(Resources.Load("Character_parts/character_3dmg_gas_l"), position: this.transform.position, rotation: this.transform.rotation);
            obj10 = (GameObject)Instantiate(Resources.Load("Character_parts/character_3dmg_gas_r"), position: this.transform.position, rotation: this.transform.rotation);
        }

        obj6.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj7.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj8.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj9.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj10.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        applyForceToBody(GO: obj6, v: v);
        applyForceToBody(GO: obj7, v: v);
        applyForceToBody(GO: obj8, v: v);
        applyForceToBody(GO: obj9, v: v);
        applyForceToBody(GO: obj10, v: v);
    }

    private void breakApart2(Vector3 v, bool isBite)
    {
        GameObject obj6;
        GameObject obj7;
        GameObject obj8;
        GameObject obj9;
        GameObject obj10;
        var obj2 = (GameObject)Instantiate(Resources.Load("Character_parts/AOTTG_HERO_body"), position: this.transform.position, rotation: this.transform.rotation);
        obj2.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
        obj2.GetComponent<HERO_SETUP>().isDeadBody = true;
        obj2.GetComponent<HERO_DEAD_BODY_SETUP>().init(aniname: currentAnimation, time: animation[name: currentAnimation].normalizedTime, part: BODY_PARTS.ARM_R);
        if (!isBite)
        {
            var gO = (GameObject)Instantiate(Resources.Load("Character_parts/AOTTG_HERO_body"), position: this.transform.position, rotation: this.transform.rotation);
            var obj4 = (GameObject)Instantiate(Resources.Load("Character_parts/AOTTG_HERO_body"), position: this.transform.position, rotation: this.transform.rotation);
            var obj5 = (GameObject)Instantiate(Resources.Load("Character_parts/AOTTG_HERO_body"), position: this.transform.position, rotation: this.transform.rotation);
            gO.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            obj4.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            obj5.gameObject.GetComponent<HERO_SETUP>().myCostume = setup.myCostume;
            gO.GetComponent<HERO_SETUP>().isDeadBody = true;
            obj4.GetComponent<HERO_SETUP>().isDeadBody = true;
            obj5.GetComponent<HERO_SETUP>().isDeadBody = true;
            gO.GetComponent<HERO_DEAD_BODY_SETUP>().init(aniname: currentAnimation, time: animation[name: currentAnimation].normalizedTime, part: BODY_PARTS.UPPER);
            obj4.GetComponent<HERO_DEAD_BODY_SETUP>().init(aniname: currentAnimation, time: animation[name: currentAnimation].normalizedTime, part: BODY_PARTS.LOWER);
            obj5.GetComponent<HERO_DEAD_BODY_SETUP>().init(aniname: currentAnimation, time: animation[name: currentAnimation].normalizedTime, part: BODY_PARTS.ARM_L);
            applyForceToBody(GO: gO, v: v);
            applyForceToBody(GO: obj4, v: v);
            applyForceToBody(GO: obj5, v: v);
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
                currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj: gO, false);
        }
        else if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
        {
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj: obj2, false);
        }

        applyForceToBody(GO: obj2, v: v);
        var transform = this.transform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_L/upper_arm_L/forearm_L/hand_L").transform;
        var transform2 = this.transform.Find("Amarture/Controller_Body/hip/spine/chest/shoulder_R/upper_arm_R/forearm_R/hand_R").transform;
        if (useGun)
        {
            obj6 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_l"), position: transform.position, rotation: transform.rotation);
            obj7 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_r"), position: transform2.position, rotation: transform2.rotation);
            obj8 = (GameObject)Instantiate(Resources.Load("Character_parts/character_3dmg_2"), position: this.transform.position, rotation: this.transform.rotation);
            obj9 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_mag_l"), position: this.transform.position, rotation: this.transform.rotation);
            obj10 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_mag_r"), position: this.transform.position, rotation: this.transform.rotation);
        }
        else
        {
            obj6 = (GameObject)Instantiate(Resources.Load("Character_parts/character_blade_l"), position: transform.position, rotation: transform.rotation);
            obj7 = (GameObject)Instantiate(Resources.Load("Character_parts/character_blade_r"), position: transform2.position, rotation: transform2.rotation);
            obj8 = (GameObject)Instantiate(Resources.Load("Character_parts/character_3dmg"), position: this.transform.position, rotation: this.transform.rotation);
            obj9 = (GameObject)Instantiate(Resources.Load("Character_parts/character_3dmg_gas_l"), position: this.transform.position, rotation: this.transform.rotation);
            obj10 = (GameObject)Instantiate(Resources.Load("Character_parts/character_3dmg_gas_r"), position: this.transform.position, rotation: this.transform.rotation);
        }

        obj6.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj7.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj8.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj9.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj10.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        applyForceToBody(GO: obj6, v: v);
        applyForceToBody(GO: obj7, v: v);
        applyForceToBody(GO: obj8, v: v);
        applyForceToBody(GO: obj9, v: v);
        applyForceToBody(GO: obj10, v: v);
    }

    private void bufferUpdate()
    {
        if (buffTime > 0f)
        {
            buffTime -= Time.deltaTime;
            if (buffTime <= 0f)
            {
                buffTime = 0f;
                if (currentBuff == BUFF.SpeedUp && animation.IsPlaying("run_sasha"))
                    crossFade("run", 0.1f);
                currentBuff = BUFF.NoBuff;
            }
        }
    }

    public void cache()
    {
        baseTransform = transform;
        baseRigidBody = rigidbody;
        maincamera = GameObject.Find("MainCamera");
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
        {
            baseAnimation = animation;
            cross1 = GameObject.Find("cross1");
            cross2 = GameObject.Find("cross2");
            crossL1 = GameObject.Find("crossL1");
            crossL2 = GameObject.Find("crossL2");
            crossR1 = GameObject.Find("crossR1");
            crossR2 = GameObject.Find("crossR2");
            LabelDistance = GameObject.Find("LabelDistance");
            cachedSprites = new Dictionary<string, UISprite>();
            foreach (GameObject obj2 in FindObjectsOfType(typeof(GameObject)))
            {
                if (obj2.GetComponent<UISprite>() != null && obj2.activeInHierarchy)
                {
                    var name = obj2.name;
                    if (!(name.Contains("blade") || name.Contains("bullet") || name.Contains("gas") || name.Contains("flare") || name.Contains("skill_cd") ? cachedSprites.ContainsKey(key: name) : true))
                        cachedSprites.Add(key: name, obj2.GetComponent<UISprite>());
                }
            }
        }
    }

    private void calcFlareCD()
    {
        if (flare1CD > 0f)
        {
            flare1CD -= Time.deltaTime;
            if (flare1CD < 0f)
                flare1CD = 0f;
        }

        if (flare2CD > 0f)
        {
            flare2CD -= Time.deltaTime;
            if (flare2CD < 0f)
                flare2CD = 0f;
        }

        if (flare3CD > 0f)
        {
            flare3CD -= Time.deltaTime;
            if (flare3CD < 0f)
                flare3CD = 0f;
        }
    }

    private void calcSkillCD()
    {
        if (skillCDDuration > 0f)
        {
            skillCDDuration -= Time.deltaTime;
            if (skillCDDuration < 0f)
                skillCDDuration = 0f;
        }
    }

    private float CalculateJumpVerticalSpeed()
    {
        return Mathf.Sqrt(2f * jumpHeight * gravity);
    }

    private void changeBlade()
    {
        if (!useGun || grounded || LevelInfo.getInfo(name: FengGameManagerMKII.level).type != GAMEMODE.PVP_AHSS)
        {
            state = HERO_STATE.ChangeBlade;
            throwedBlades = false;
            if (useGun)
            {
                if (!leftGunHasBullet && !rightGunHasBullet)
                {
                    if (grounded)
                        reloadAnimation = "AHSS_gun_reload_both";
                    else
                        reloadAnimation = "AHSS_gun_reload_both_air";
                }
                else if (!leftGunHasBullet)
                {
                    if (grounded)
                        reloadAnimation = "AHSS_gun_reload_l";
                    else
                        reloadAnimation = "AHSS_gun_reload_l_air";
                }
                else if (!rightGunHasBullet)
                {
                    if (grounded)
                        reloadAnimation = "AHSS_gun_reload_r";
                    else
                        reloadAnimation = "AHSS_gun_reload_r_air";
                }
                else
                {
                    if (grounded)
                        reloadAnimation = "AHSS_gun_reload_both";
                    else
                        reloadAnimation = "AHSS_gun_reload_both_air";
                    rightGunHasBullet = false;
                    leftGunHasBullet = false;
                }

                crossFade(aniName: reloadAnimation, 0.05f);
            }
            else
            {
                if (!grounded)
                    reloadAnimation = "changeBlade_air";
                else
                    reloadAnimation = "changeBlade";
                crossFade(aniName: reloadAnimation, 0.1f);
            }
        }
    }

    private void checkDashDoubleTap()
    {
        if (uTapTime >= 0f)
        {
            uTapTime += Time.deltaTime;
            if (uTapTime > 0.2f)
                uTapTime = -1f;
        }

        if (dTapTime >= 0f)
        {
            dTapTime += Time.deltaTime;
            if (dTapTime > 0.2f)
                dTapTime = -1f;
        }

        if (lTapTime >= 0f)
        {
            lTapTime += Time.deltaTime;
            if (lTapTime > 0.2f)
                lTapTime = -1f;
        }

        if (rTapTime >= 0f)
        {
            rTapTime += Time.deltaTime;
            if (rTapTime > 0.2f)
                rTapTime = -1f;
        }

        if (inputManager.isInputDown[InputCode.up])
        {
            if (uTapTime == -1f)
                uTapTime = 0f;
            if (uTapTime != 0f)
                dashU = true;
        }

        if (inputManager.isInputDown[InputCode.down])
        {
            if (dTapTime == -1f)
                dTapTime = 0f;
            if (dTapTime != 0f)
                dashD = true;
        }

        if (inputManager.isInputDown[InputCode.left])
        {
            if (lTapTime == -1f)
                lTapTime = 0f;
            if (lTapTime != 0f)
                dashL = true;
        }

        if (inputManager.isInputDown[InputCode.right])
        {
            if (rTapTime == -1f)
                rTapTime = 0f;
            if (rTapTime != 0f)
                dashR = true;
        }
    }

    private void checkDashRebind()
    {
        if (FengGameManagerMKII.inputRC.isInputHuman(code: InputCodeRC.dash))
        {
            if (inputManager.isInput[InputCode.up])
                dashU = true;
            else if (inputManager.isInput[InputCode.down])
                dashD = true;
            else if (inputManager.isInput[InputCode.left])
                dashL = true;
            else if (inputManager.isInput[InputCode.right])
                dashR = true;
        }
    }

    public void checkTitan()
    {
        int count;
        var ray = Camera.main.ScreenPointToRay(position: Input.mousePosition);
        LayerMask mask = 1 << LayerMask.NameToLayer("PlayerAttackBox");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask3 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask4 = mask | mask2 | mask3;
        var hitArray = Physics.RaycastAll(ray: ray, 180f, layerMask: mask4.value);
        var list = new List<RaycastHit>();
        var list2 = new List<TITAN>();
        for (count = 0; count < hitArray.Length; count++)
        {
            var item = hitArray[count];
            list.Add(item: item);
        }

        list.Sort((x, y) => x.distance.CompareTo(value: y.distance));
        var num2 = 180f;
        for (count = 0; count < list.Count; count++)
        {
            var hit2 = list[index: count];
            var gameObject = hit2.collider.gameObject;
            if (gameObject.layer == 0x10)
            {
                if (gameObject.name.Contains("PlayerDetectorRC") && (hit2 = list[index: count]).distance < num2)
                {
                    num2 -= 60f;
                    if (num2 <= 60f)
                        count = list.Count;
                    var component = gameObject.transform.root.gameObject.GetComponent<TITAN>();
                    if (component != null)
                        list2.Add(item: component);
                }
            }
            else
            {
                count = list.Count;
            }
        }

        for (count = 0; count < myTitans.Count; count++)
        {
            var titan2 = myTitans[index: count];
            if (!list2.Contains(item: titan2))
                titan2.isLook = false;
        }

        for (count = 0; count < list2.Count; count++)
        {
            var titan3 = list2[index: count];
            titan3.isLook = true;
        }

        myTitans = list2;
    }

    public void ClearPopup()
    {
        FengGameManagerMKII.instance.ShowHUDInfoCenter(content: string.Empty);
    }

    public void continueAnimation()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                if (current != null && current.speed == 1f)
                    return;
                current.speed = 1f;
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }

        customAnimationSpeed();
        playAnimation(currentPlayingClipName());
        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && photonView.isMine)
            photonView.RPC("netContinueAnimation", target: PhotonTargets.Others);
    }

    public void crossFade(string aniName, float time)
    {
        currentAnimation = aniName;
        animation.CrossFade(animation: aniName, fadeLength: time);
        if (PhotonNetwork.connected && photonView.isMine)
        {
            object[] parameters = { aniName, time };
            photonView.RPC("netCrossFade", target: PhotonTargets.Others, parameters: parameters);
        }
    }

    public string currentPlayingClipName()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                if (current != null && animation.IsPlaying(name: current.name))
                    return current.name;
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }

        return string.Empty;
    }

    private void customAnimationSpeed()
    {
        animation["attack5"].speed = 1.85f;
        animation["changeBlade"].speed = 1.2f;
        animation["air_release"].speed = 0.6f;
        animation["changeBlade_air"].speed = 0.8f;
        animation["AHSS_gun_reload_both"].speed = 0.38f;
        animation["AHSS_gun_reload_both_air"].speed = 0.5f;
        animation["AHSS_gun_reload_l"].speed = 0.4f;
        animation["AHSS_gun_reload_l_air"].speed = 0.5f;
        animation["AHSS_gun_reload_r"].speed = 0.4f;
        animation["AHSS_gun_reload_r_air"].speed = 0.5f;
    }

    private void dash(float horizontal, float vertical)
    {
        print(dashTime + " " + currentGas);
        if (dashTime <= 0f && currentGas > 0f && !isMounted)
        {
            useGas(totalGas * 0.04f);
            facingDirection = getGlobalFacingDirection(horizontal: horizontal, vertical: vertical);
            dashV = getGlobaleFacingVector3(resultAngle: facingDirection);
            originVM = currentSpeed;
            var quaternion = Quaternion.Euler(0f, y: facingDirection, 0f);
            rigidbody.rotation = quaternion;
            targetRotation = quaternion;
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                Instantiate(Resources.Load("FX/boost_smoke"), position: transform.position, rotation: transform.rotation);
            else
                PhotonNetwork.Instantiate("FX/boost_smoke", position: transform.position, rotation: transform.rotation, 0);
            dashTime = 0.5f;
            crossFade("dash", 0.1f);
            animation["dash"].time = 0.1f;
            state = HERO_STATE.AirDodge;
            falseAttack();
            rigidbody.AddForce(dashV * 40f, mode: ForceMode.VelocityChange);
        }
    }

    public void die(Vector3 v, bool isBite)
    {
        if (invincible <= 0f)
        {
            if (titanForm && eren_titan != null)
                eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
            if (bulletLeft != null)
                bulletLeft.GetComponent<Bullet>().removeMe();
            if (bulletRight != null)
                bulletRight.GetComponent<Bullet>().removeMe();
            meatDie.Play();
            if ((IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine) && !useGun)
            {
                leftbladetrail.Deactivate();
                rightbladetrail.Deactivate();
                leftbladetrail2.Deactivate();
                rightbladetrail2.Deactivate();
            }

            breakApart2(v: v, isBite: isBite);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().gameLose2();
            falseAttack();
            hasDied = true;
            var transform = this.transform.Find("audio_die");
            transform.parent = null;
            transform.GetComponent<AudioSource>().Play();
            if (PlayerPrefs.HasKey("EnableSS") && PlayerPrefs.GetInt("EnableSS") == 1)
                GameObject.Find("MainCamera").GetComponent<IN_GAME_MAIN_CAMERA>().startSnapShot2(p: this.transform.position, 0, null, 0.02f);
            Destroy(obj: gameObject);
        }
    }

    public void die2(Transform tf)
    {
        if (invincible <= 0f)
        {
            if (titanForm && eren_titan != null)
                eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
            if (bulletLeft != null)
                bulletLeft.GetComponent<Bullet>().removeMe();
            if (bulletRight != null)
                bulletRight.GetComponent<Bullet>().removeMe();
            var transform = this.transform.Find("audio_die");
            transform.parent = null;
            transform.GetComponent<AudioSource>().Play();
            meatDie.Play();
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(null);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().gameLose2();
            falseAttack();
            hasDied = true;
            var obj2 = (GameObject)Instantiate(Resources.Load("hitMeat2"));
            obj2.transform.position = this.transform.position;
            Destroy(obj: gameObject);
        }
    }

    private void dodge(bool offTheWall = false)
    {
        if (myHorse != null && !isMounted && Vector3.Distance(a: myHorse.transform.position, b: transform.position) < 15f)
        {
            getOnHorse();
        }
        else
        {
            state = HERO_STATE.GroundDodge;
            if (!offTheWall)
            {
                float num;
                float num2;
                if (inputManager.isInput[InputCode.up])
                    num = 1f;
                else if (inputManager.isInput[InputCode.down])
                    num = -1f;
                else
                    num = 0f;
                if (inputManager.isInput[InputCode.left])
                    num2 = -1f;
                else if (inputManager.isInput[InputCode.right])
                    num2 = 1f;
                else
                    num2 = 0f;
                var num3 = getGlobalFacingDirection(horizontal: num2, vertical: num);
                if (num2 != 0f || num != 0f)
                {
                    facingDirection = num3 + 180f;
                    targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                }

                crossFade("dodge", 0.1f);
            }
            else
            {
                playAnimation("dodge");
                playAnimationAt("dodge", 0.2f);
            }

            sparks.enableEmission = false;
        }
    }

    private void dodge2(bool offTheWall = false)
    {
        if (!FengGameManagerMKII.inputRC.isInputHorse(code: InputCodeRC.horseMount) || myHorse == null || isMounted || Vector3.Distance(a: myHorse.transform.position, b: transform.position) >= 15f)
        {
            state = HERO_STATE.GroundDodge;
            if (!offTheWall)
            {
                float num;
                float num2;
                if (inputManager.isInput[InputCode.up])
                    num = 1f;
                else if (inputManager.isInput[InputCode.down])
                    num = -1f;
                else
                    num = 0f;
                if (inputManager.isInput[InputCode.left])
                    num2 = -1f;
                else if (inputManager.isInput[InputCode.right])
                    num2 = 1f;
                else
                    num2 = 0f;
                var num3 = getGlobalFacingDirection(horizontal: num2, vertical: num);
                if (num2 != 0f || num != 0f)
                {
                    facingDirection = num3 + 180f;
                    targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                }

                crossFade("dodge", 0.1f);
            }
            else
            {
                playAnimation("dodge");
                playAnimationAt("dodge", 0.2f);
            }

            sparks.enableEmission = false;
        }
    }

    private void erenTransform()
    {
        skillCDDuration = skillCDLast;
        if (bulletLeft != null)
            bulletLeft.GetComponent<Bullet>().removeMe();
        if (bulletRight != null)
            bulletRight.GetComponent<Bullet>().removeMe();
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
            eren_titan = (GameObject)Instantiate(Resources.Load("TITAN_EREN"), position: transform.position, rotation: transform.rotation);
        else
            eren_titan = PhotonNetwork.Instantiate("TITAN_EREN", position: transform.position, rotation: transform.rotation, 0);
        eren_titan.GetComponent<TITAN_EREN>().realBody = gameObject;
        GameObject.Find("MainCamera").GetComponent<IN_GAME_MAIN_CAMERA>().flashBlind();
        GameObject.Find("MainCamera").GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj: eren_titan);
        eren_titan.GetComponent<TITAN_EREN>().born();
        eren_titan.rigidbody.velocity = rigidbody.velocity;
        rigidbody.velocity = Vector3.zero;
        transform.position = eren_titan.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck").position;
        titanForm = true;
        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE)
        {
            object[] parameters = { eren_titan.GetPhotonView().viewID };
            photonView.RPC("whoIsMyErenTitan", target: PhotonTargets.Others, parameters: parameters);
        }

        if (smoke_3dmg.enableEmission && IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && photonView.isMine)
        {
            object[] objArray2 = { false };
            photonView.RPC("net3DMGSMOKE", target: PhotonTargets.Others, parameters: objArray2);
        }

        smoke_3dmg.enableEmission = false;
    }

    private void escapeFromGrab() { }

    public void falseAttack()
    {
        attackMove = false;
        if (useGun)
        {
            if (!attackReleased)
            {
                continueAnimation();
                attackReleased = true;
            }
        }
        else
        {
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
            {
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                leftbladetrail.StopSmoothly(0.2f);
                rightbladetrail.StopSmoothly(0.2f);
                leftbladetrail2.StopSmoothly(0.2f);
                rightbladetrail2.StopSmoothly(0.2f);
            }

            attackLoop = 0;
            if (!attackReleased)
            {
                continueAnimation();
                attackReleased = true;
            }
        }
    }

    public void fillGas()
    {
        currentGas = totalGas;
    }

    private GameObject findNearestTitan()
    {
        var objArray = GameObject.FindGameObjectsWithTag("titan");
        GameObject obj2 = null;
        var positiveInfinity = float.PositiveInfinity;
        var position = transform.position;
        foreach (var obj3 in objArray)
        {
            var vector2 = obj3.transform.position - position;
            var sqrMagnitude = vector2.sqrMagnitude;
            if (sqrMagnitude < positiveInfinity)
            {
                obj2 = obj3;
                positiveInfinity = sqrMagnitude;
            }
        }

        return obj2;
    }

    private void FixedUpdate()
    {
        if (!titanForm && !isCannon && (!IN_GAME_MAIN_CAMERA.isPausing || IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE))
        {
            currentSpeed = baseRigidBody.velocity.magnitude;
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
            {
                if (!(baseAnimation.IsPlaying("attack3_2") || baseAnimation.IsPlaying("attack5") || baseAnimation.IsPlaying("special_petra")))
                    baseRigidBody.rotation = Quaternion.Lerp(from: gameObject.transform.rotation, to: targetRotation, Time.deltaTime * 6f);
                if (state == HERO_STATE.Grab)
                {
                    baseRigidBody.AddForce(force: -baseRigidBody.velocity, mode: ForceMode.VelocityChange);
                }
                else
                {
                    if (IsGrounded())
                    {
                        if (!grounded)
                            justGrounded = true;
                        grounded = true;
                    }
                    else
                    {
                        grounded = false;
                    }

                    if (hookSomeOne)
                    {
                        if (hookTarget != null)
                        {
                            var vector2 = hookTarget.transform.position - baseTransform.position;
                            var magnitude = vector2.magnitude;
                            if (magnitude > 2f)
                                baseRigidBody.AddForce(vector2.normalized * Mathf.Pow(f: magnitude, 0.15f) * 30f - baseRigidBody.velocity * 0.95f, mode: ForceMode.VelocityChange);
                        }
                        else
                        {
                            hookSomeOne = false;
                        }
                    }
                    else if (hookBySomeOne && badGuy != null)
                    {
                        if (badGuy != null)
                        {
                            var vector3 = badGuy.transform.position - baseTransform.position;
                            var f = vector3.magnitude;
                            if (f > 5f)
                                baseRigidBody.AddForce(vector3.normalized * Mathf.Pow(f: f, 0.15f) * 0.2f, mode: ForceMode.Impulse);
                        }
                        else
                        {
                            hookBySomeOne = false;
                        }
                    }

                    var x = 0f;
                    var z = 0f;
                    if (!IN_GAME_MAIN_CAMERA.isTyping)
                    {
                        if (inputManager.isInput[InputCode.up])
                            z = 1f;
                        else if (inputManager.isInput[InputCode.down])
                            z = -1f;
                        else
                            z = 0f;
                        if (inputManager.isInput[InputCode.left])
                            x = -1f;
                        else if (inputManager.isInput[InputCode.right])
                            x = 1f;
                        else
                            x = 0f;
                    }

                    var flag2 = false;
                    var flag3 = false;
                    var flag4 = false;
                    isLeftHandHooked = false;
                    isRightHandHooked = false;
                    if (isLaunchLeft)
                    {
                        if (bulletLeft != null && bulletLeft.GetComponent<Bullet>().isHooked())
                        {
                            isLeftHandHooked = true;
                            var to = bulletLeft.transform.position - baseTransform.position;
                            to.Normalize();
                            to = to * 10f;
                            if (!isLaunchRight)
                                to = to * 2f;
                            if (Vector3.Angle(from: baseRigidBody.velocity, to: to) > 90f && inputManager.isInput[InputCode.jump])
                            {
                                flag3 = true;
                                flag2 = true;
                            }

                            if (!flag3)
                            {
                                baseRigidBody.AddForce(force: to);
                                if (Vector3.Angle(from: baseRigidBody.velocity, to: to) > 90f)
                                    baseRigidBody.AddForce(-baseRigidBody.velocity * 2f, mode: ForceMode.Acceleration);
                            }
                        }

                        launchElapsedTimeL += Time.deltaTime;
                        if (QHold && currentGas > 0f)
                        {
                            useGas(useGasSpeed * Time.deltaTime);
                        }
                        else if (launchElapsedTimeL > 0.3f)
                        {
                            isLaunchLeft = false;
                            if (bulletLeft != null)
                            {
                                bulletLeft.GetComponent<Bullet>().disable();
                                releaseIfIHookSb();
                                bulletLeft = null;
                                flag3 = false;
                            }
                        }
                    }

                    if (isLaunchRight)
                    {
                        if (bulletRight != null && bulletRight.GetComponent<Bullet>().isHooked())
                        {
                            isRightHandHooked = true;
                            var vector5 = bulletRight.transform.position - baseTransform.position;
                            vector5.Normalize();
                            vector5 = vector5 * 10f;
                            if (!isLaunchLeft)
                                vector5 = vector5 * 2f;
                            if (Vector3.Angle(from: baseRigidBody.velocity, to: vector5) > 90f && inputManager.isInput[InputCode.jump])
                            {
                                flag4 = true;
                                flag2 = true;
                            }

                            if (!flag4)
                            {
                                baseRigidBody.AddForce(force: vector5);
                                if (Vector3.Angle(from: baseRigidBody.velocity, to: vector5) > 90f)
                                    baseRigidBody.AddForce(-baseRigidBody.velocity * 2f, mode: ForceMode.Acceleration);
                            }
                        }

                        launchElapsedTimeR += Time.deltaTime;
                        if (EHold && currentGas > 0f)
                        {
                            useGas(useGasSpeed * Time.deltaTime);
                        }
                        else if (launchElapsedTimeR > 0.3f)
                        {
                            isLaunchRight = false;
                            if (bulletRight != null)
                            {
                                bulletRight.GetComponent<Bullet>().disable();
                                releaseIfIHookSb();
                                bulletRight = null;
                                flag4 = false;
                            }
                        }
                    }

                    if (grounded)
                    {
                        Vector3 vector7;
                        var zero = Vector3.zero;
                        if (state == HERO_STATE.Attack)
                        {
                            if (attackAnimation == "attack5")
                            {
                                if (baseAnimation[name: attackAnimation].normalizedTime > 0.4f && baseAnimation[name: attackAnimation].normalizedTime < 0.61f)
                                    baseRigidBody.AddForce(gameObject.transform.forward * 200f);
                            }
                            else if (attackAnimation == "special_petra")
                            {
                                if (baseAnimation[name: attackAnimation].normalizedTime > 0.35f && baseAnimation[name: attackAnimation].normalizedTime < 0.48f)
                                    baseRigidBody.AddForce(gameObject.transform.forward * 200f);
                            }
                            else if (baseAnimation.IsPlaying("attack3_2"))
                            {
                                zero = Vector3.zero;
                            }
                            else if (baseAnimation.IsPlaying("attack1") || baseAnimation.IsPlaying("attack2"))
                            {
                                baseRigidBody.AddForce(gameObject.transform.forward * 200f);
                            }

                            if (baseAnimation.IsPlaying("attack3_2"))
                                zero = Vector3.zero;
                        }

                        if (justGrounded)
                        {
                            if (state != HERO_STATE.Attack || attackAnimation != "attack3_1" && attackAnimation != "attack5" && attackAnimation != "special_petra")
                            {
                                if (state != HERO_STATE.Attack && x == 0f && z == 0f && bulletLeft == null && bulletRight == null && state != HERO_STATE.FillGas)
                                {
                                    state = HERO_STATE.Land;
                                    crossFade("dash_land", 0.01f);
                                }
                                else
                                {
                                    buttonAttackRelease = true;
                                    if (state != HERO_STATE.Attack && baseRigidBody.velocity.x * baseRigidBody.velocity.x + baseRigidBody.velocity.z * baseRigidBody.velocity.z > speed * speed * 1.5f && state != HERO_STATE.FillGas)
                                    {
                                        state = HERO_STATE.Slide;
                                        crossFade("slide", 0.05f);
                                        facingDirection = Mathf.Atan2(y: baseRigidBody.velocity.x, x: baseRigidBody.velocity.z) * 57.29578f;
                                        targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                                        sparks.enableEmission = true;
                                    }
                                }
                            }

                            justGrounded = false;
                            zero = baseRigidBody.velocity;
                        }

                        if (state == HERO_STATE.Attack && attackAnimation == "attack3_1" && baseAnimation[name: attackAnimation].normalizedTime >= 1f)
                        {
                            playAnimation("attack3_2");
                            resetAnimationSpeed();
                            vector7 = Vector3.zero;
                            baseRigidBody.velocity = vector7;
                            zero = vector7;
                            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().startShake(0.2f, 0.3f);
                        }

                        if (state == HERO_STATE.GroundDodge)
                        {
                            if (baseAnimation["dodge"].normalizedTime >= 0.2f && baseAnimation["dodge"].normalizedTime < 0.8f)
                                zero = -baseTransform.forward * 2.4f * speed;
                            if (baseAnimation["dodge"].normalizedTime > 0.8f)
                                zero = baseRigidBody.velocity * 0.9f;
                        }
                        else if (state == HERO_STATE.Idle)
                        {
                            var vector8 = new Vector3(x: x, 0f, z: z);
                            var resultAngle = getGlobalFacingDirection(horizontal: x, vertical: z);
                            zero = getGlobaleFacingVector3(resultAngle: resultAngle);
                            var num6 = vector8.magnitude <= 0.95f ? vector8.magnitude >= 0.25f ? vector8.magnitude : 0f : 1f;
                            zero = zero * num6;
                            zero = zero * speed;
                            if (buffTime > 0f && currentBuff == BUFF.SpeedUp)
                                zero = zero * 4f;
                            if (x != 0f || z != 0f)
                            {
                                if (!baseAnimation.IsPlaying("run") && !baseAnimation.IsPlaying("jump") && !baseAnimation.IsPlaying("run_sasha") && (!baseAnimation.IsPlaying("horse_geton") || baseAnimation["horse_geton"].normalizedTime >= 0.5f))
                                {
                                    if (buffTime > 0f && currentBuff == BUFF.SpeedUp)
                                        crossFade("run_sasha", 0.1f);
                                    else
                                        crossFade("run", 0.1f);
                                }
                            }
                            else
                            {
                                if (!(baseAnimation.IsPlaying(name: standAnimation) || state == HERO_STATE.Land || baseAnimation.IsPlaying("jump") || baseAnimation.IsPlaying("horse_geton") || baseAnimation.IsPlaying("grabbed")))
                                {
                                    crossFade(aniName: standAnimation, 0.1f);
                                    zero = zero * 0f;
                                }

                                resultAngle = -874f;
                            }

                            if (resultAngle != -874f)
                            {
                                facingDirection = resultAngle;
                                targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                            }
                        }
                        else if (state == HERO_STATE.Land)
                        {
                            zero = baseRigidBody.velocity * 0.96f;
                        }
                        else if (state == HERO_STATE.Slide)
                        {
                            zero = baseRigidBody.velocity * 0.99f;
                            if (currentSpeed < speed * 1.2f)
                            {
                                idle();
                                sparks.enableEmission = false;
                            }
                        }

                        var velocity = baseRigidBody.velocity;
                        var force = zero - velocity;
                        force.x = Mathf.Clamp(value: force.x, min: -maxVelocityChange, max: maxVelocityChange);
                        force.z = Mathf.Clamp(value: force.z, min: -maxVelocityChange, max: maxVelocityChange);
                        force.y = 0f;
                        if (baseAnimation.IsPlaying("jump") && baseAnimation["jump"].normalizedTime > 0.18f)
                            force.y += 8f;
                        if (baseAnimation.IsPlaying("horse_geton") && baseAnimation["horse_geton"].normalizedTime > 0.18f && baseAnimation["horse_geton"].normalizedTime < 1f)
                        {
                            var num7 = 6f;
                            force = -baseRigidBody.velocity;
                            force.y = num7;
                            var num8 = Vector3.Distance(a: myHorse.transform.position, b: baseTransform.position);
                            var num9 = 0.6f * gravity * num8 / 12f;
                            vector7 = myHorse.transform.position - baseTransform.position;
                            force += num9 * vector7.normalized;
                        }

                        if (!(state == HERO_STATE.Attack && useGun))
                        {
                            baseRigidBody.AddForce(force: force, mode: ForceMode.VelocityChange);
                            baseRigidBody.rotation = Quaternion.Lerp(from: gameObject.transform.rotation, Quaternion.Euler(0f, y: facingDirection, 0f), Time.deltaTime * 10f);
                        }
                    }
                    else
                    {
                        if (sparks.enableEmission)
                            sparks.enableEmission = false;
                        if (myHorse != null && (baseAnimation.IsPlaying("horse_geton") || baseAnimation.IsPlaying("air_fall")) && baseRigidBody.velocity.y < 0f && Vector3.Distance(myHorse.transform.position + Vector3.up * 1.65f, b: baseTransform.position) < 0.5f)
                        {
                            baseTransform.position = myHorse.transform.position + Vector3.up * 1.65f;
                            baseTransform.rotation = myHorse.transform.rotation;
                            isMounted = true;
                            crossFade("horse_idle", 0.1f);
                            myHorse.GetComponent<Horse>().mounted();
                        }

                        if (!(state != HERO_STATE.Idle || baseAnimation.IsPlaying("dash") || baseAnimation.IsPlaying("wallrun") || baseAnimation.IsPlaying("toRoof") || baseAnimation.IsPlaying("horse_geton") || baseAnimation.IsPlaying("horse_getoff") || baseAnimation.IsPlaying("air_release") || isMounted
                              || baseAnimation.IsPlaying("air_hook_l_just") && baseAnimation["air_hook_l_just"].normalizedTime < 1f || baseAnimation.IsPlaying("air_hook_r_just") && baseAnimation["air_hook_r_just"].normalizedTime < 1f
                            ? baseAnimation["dash"].normalizedTime < 0.99f
                            : false))
                        {
                            if (!isLeftHandHooked && !isRightHandHooked && (baseAnimation.IsPlaying("air_hook_l") || baseAnimation.IsPlaying("air_hook_r") || baseAnimation.IsPlaying("air_hook")) && baseRigidBody.velocity.y > 20f)
                            {
                                baseAnimation.CrossFade("air_release");
                            }
                            else
                            {
                                var flag5 = Mathf.Abs(f: baseRigidBody.velocity.x) + Mathf.Abs(f: baseRigidBody.velocity.z) > 25f;
                                var flag6 = baseRigidBody.velocity.y < 0f;
                                if (!flag5)
                                {
                                    if (flag6)
                                    {
                                        if (!baseAnimation.IsPlaying("air_fall"))
                                            crossFade("air_fall", 0.2f);
                                    }
                                    else if (!baseAnimation.IsPlaying("air_rise"))
                                    {
                                        crossFade("air_rise", 0.2f);
                                    }
                                }
                                else if (!isLeftHandHooked && !isRightHandHooked)
                                {
                                    var current = -Mathf.Atan2(y: baseRigidBody.velocity.z, x: baseRigidBody.velocity.x) * 57.29578f;
                                    var num11 = -Mathf.DeltaAngle(current: current, baseTransform.rotation.eulerAngles.y - 90f);
                                    if (Mathf.Abs(f: num11) < 45f)
                                    {
                                        if (!baseAnimation.IsPlaying("air2"))
                                            crossFade("air2", 0.2f);
                                    }
                                    else if (num11 < 135f && num11 > 0f)
                                    {
                                        if (!baseAnimation.IsPlaying("air2_right"))
                                            crossFade("air2_right", 0.2f);
                                    }
                                    else if (num11 > -135f && num11 < 0f)
                                    {
                                        if (!baseAnimation.IsPlaying("air2_left"))
                                            crossFade("air2_left", 0.2f);
                                    }
                                    else if (!baseAnimation.IsPlaying("air2_backward"))
                                    {
                                        crossFade("air2_backward", 0.2f);
                                    }
                                }
                                else if (useGun)
                                {
                                    if (!isRightHandHooked)
                                    {
                                        if (!baseAnimation.IsPlaying("AHSS_hook_forward_l"))
                                            crossFade("AHSS_hook_forward_l", 0.1f);
                                    }
                                    else if (!isLeftHandHooked)
                                    {
                                        if (!baseAnimation.IsPlaying("AHSS_hook_forward_r"))
                                            crossFade("AHSS_hook_forward_r", 0.1f);
                                    }
                                    else if (!baseAnimation.IsPlaying("AHSS_hook_forward_both"))
                                    {
                                        crossFade("AHSS_hook_forward_both", 0.1f);
                                    }
                                }
                                else if (!isRightHandHooked)
                                {
                                    if (!baseAnimation.IsPlaying("air_hook_l"))
                                        crossFade("air_hook_l", 0.1f);
                                }
                                else if (!isLeftHandHooked)
                                {
                                    if (!baseAnimation.IsPlaying("air_hook_r"))
                                        crossFade("air_hook_r", 0.1f);
                                }
                                else if (!baseAnimation.IsPlaying("air_hook"))
                                {
                                    crossFade("air_hook", 0.1f);
                                }
                            }
                        }

                        if (state == HERO_STATE.Idle && baseAnimation.IsPlaying("air_release") && baseAnimation["air_release"].normalizedTime >= 1f)
                            crossFade("air_rise", 0.2f);
                        if (baseAnimation.IsPlaying("horse_getoff") && baseAnimation["horse_getoff"].normalizedTime >= 1f)
                            crossFade("air_rise", 0.2f);
                        if (baseAnimation.IsPlaying("toRoof"))
                        {
                            if (baseAnimation["toRoof"].normalizedTime < 0.22f)
                            {
                                baseRigidBody.velocity = Vector3.zero;
                                baseRigidBody.AddForce(new Vector3(0f, gravity * baseRigidBody.mass, 0f));
                            }
                            else
                            {
                                if (!wallJump)
                                {
                                    wallJump = true;
                                    baseRigidBody.AddForce(Vector3.up * 8f, mode: ForceMode.Impulse);
                                }

                                baseRigidBody.AddForce(baseTransform.forward * 0.05f, mode: ForceMode.Impulse);
                            }

                            if (baseAnimation["toRoof"].normalizedTime >= 1f)
                                playAnimation("air_rise");
                        }
                        else if (!(state != HERO_STATE.Idle || !isPressDirectionTowardsHero(h: x, v: z) || inputManager.isInput[InputCode.jump] || inputManager.isInput[InputCode.leftRope] || inputManager.isInput[InputCode.rightRope] || inputManager.isInput[InputCode.bothRope] || !IsFrontGrounded()
                                   || baseAnimation.IsPlaying("wallrun") || baseAnimation.IsPlaying("dodge")))
                        {
                            crossFade("wallrun", 0.1f);
                            wallRunTime = 0f;
                        }
                        else if (baseAnimation.IsPlaying("wallrun"))
                        {
                            baseRigidBody.AddForce(Vector3.up * speed - baseRigidBody.velocity, mode: ForceMode.VelocityChange);
                            wallRunTime += Time.deltaTime;
                            if (wallRunTime > 1f || z == 0f && x == 0f)
                            {
                                baseRigidBody.AddForce(-baseTransform.forward * speed * 0.75f, mode: ForceMode.Impulse);
                                dodge2(true);
                            }
                            else if (!IsUpFrontGrounded())
                            {
                                wallJump = false;
                                crossFade("toRoof", 0.1f);
                            }
                            else if (!IsFrontGrounded())
                            {
                                crossFade("air_fall", 0.1f);
                            }
                        }
                        else if (!baseAnimation.IsPlaying("attack5") && !baseAnimation.IsPlaying("special_petra") && !baseAnimation.IsPlaying("dash") && !baseAnimation.IsPlaying("jump"))
                        {
                            var vector11 = new Vector3(x: x, 0f, z: z);
                            var num12 = getGlobalFacingDirection(horizontal: x, vertical: z);
                            var vector12 = getGlobaleFacingVector3(resultAngle: num12);
                            var num13 = vector11.magnitude <= 0.95f ? vector11.magnitude >= 0.25f ? vector11.magnitude : 0f : 1f;
                            vector12 = vector12 * num13;
                            vector12 = vector12 * (setup.myCostume.stat.ACL / 10f * 2f);
                            if (x == 0f && z == 0f)
                            {
                                if (state == HERO_STATE.Attack)
                                    vector12 = vector12 * 0f;
                                num12 = -874f;
                            }

                            if (num12 != -874f)
                            {
                                facingDirection = num12;
                                targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                            }

                            if (!flag3 && !flag4 && !isMounted && inputManager.isInput[InputCode.jump] && currentGas > 0f)
                            {
                                if (x != 0f || z != 0f)
                                    baseRigidBody.AddForce(force: vector12, mode: ForceMode.Acceleration);
                                else
                                    baseRigidBody.AddForce(baseTransform.forward * vector12.magnitude, mode: ForceMode.Acceleration);
                                flag2 = true;
                            }
                        }

                        if (baseAnimation.IsPlaying("air_fall") && currentSpeed < 0.2f && IsFrontGrounded())
                            crossFade("onWall", 0.3f);
                    }

                    spinning = false;
                    if (flag3 && flag4)
                    {
                        var num14 = currentSpeed + 0.1f;
                        baseRigidBody.AddForce(force: -baseRigidBody.velocity, mode: ForceMode.VelocityChange);
                        var vector13 = (bulletRight.transform.position + bulletLeft.transform.position) * 0.5f - baseTransform.position;
                        var num15 = 0f;
                        if ((int)FengGameManagerMKII.settings[0x61] == 1 && FengGameManagerMKII.inputRC.isInputHuman(code: InputCodeRC.reelin))
                            num15 = -1f;
                        else if ((int)FengGameManagerMKII.settings[0x74] == 1 && FengGameManagerMKII.inputRC.isInputHuman(code: InputCodeRC.reelout))
                            num15 = 1f;
                        else
                            num15 = Input.GetAxis("Mouse ScrollWheel") * 5555f;
                        num15 = Mathf.Clamp(value: num15, -0.8f, 0.8f);
                        var num16 = 1f + num15;
                        var vector14 = Vector3.RotateTowards(current: vector13, target: baseRigidBody.velocity, 1.53938f * num16, 1.53938f * num16);
                        vector14.Normalize();
                        spinning = true;
                        baseRigidBody.velocity = vector14 * num14;
                    }
                    else if (flag3)
                    {
                        var num17 = currentSpeed + 0.1f;
                        baseRigidBody.AddForce(force: -baseRigidBody.velocity, mode: ForceMode.VelocityChange);
                        var vector15 = bulletLeft.transform.position - baseTransform.position;
                        var num18 = 0f;
                        if ((int)FengGameManagerMKII.settings[0x61] == 1 && FengGameManagerMKII.inputRC.isInputHuman(code: InputCodeRC.reelin))
                            num18 = -1f;
                        else if ((int)FengGameManagerMKII.settings[0x74] == 1 && FengGameManagerMKII.inputRC.isInputHuman(code: InputCodeRC.reelout))
                            num18 = 1f;
                        else
                            num18 = Input.GetAxis("Mouse ScrollWheel") * 5555f;
                        num18 = Mathf.Clamp(value: num18, -0.8f, 0.8f);
                        var num19 = 1f + num18;
                        var vector16 = Vector3.RotateTowards(current: vector15, target: baseRigidBody.velocity, 1.53938f * num19, 1.53938f * num19);
                        vector16.Normalize();
                        spinning = true;
                        baseRigidBody.velocity = vector16 * num17;
                    }
                    else if (flag4)
                    {
                        var num20 = currentSpeed + 0.1f;
                        baseRigidBody.AddForce(force: -baseRigidBody.velocity, mode: ForceMode.VelocityChange);
                        var vector17 = bulletRight.transform.position - baseTransform.position;
                        var num21 = 0f;
                        if ((int)FengGameManagerMKII.settings[0x61] == 1 && FengGameManagerMKII.inputRC.isInputHuman(code: InputCodeRC.reelin))
                            num21 = -1f;
                        else if ((int)FengGameManagerMKII.settings[0x74] == 1 && FengGameManagerMKII.inputRC.isInputHuman(code: InputCodeRC.reelout))
                            num21 = 1f;
                        else
                            num21 = Input.GetAxis("Mouse ScrollWheel") * 5555f;
                        num21 = Mathf.Clamp(value: num21, -0.8f, 0.8f);
                        var num22 = 1f + num21;
                        var vector18 = Vector3.RotateTowards(current: vector17, target: baseRigidBody.velocity, 1.53938f * num22, 1.53938f * num22);
                        vector18.Normalize();
                        spinning = true;
                        baseRigidBody.velocity = vector18 * num20;
                    }

                    if (state == HERO_STATE.Attack && (attackAnimation == "attack5" || attackAnimation == "special_petra") && baseAnimation[name: attackAnimation].normalizedTime > 0.4f && !attackMove)
                    {
                        attackMove = true;
                        if (launchPointRight.magnitude > 0f)
                        {
                            var vector19 = launchPointRight - baseTransform.position;
                            vector19.Normalize();
                            vector19 = vector19 * 13f;
                            baseRigidBody.AddForce(force: vector19, mode: ForceMode.Impulse);
                        }

                        if (attackAnimation == "special_petra" && launchPointLeft.magnitude > 0f)
                        {
                            var vector20 = launchPointLeft - baseTransform.position;
                            vector20.Normalize();
                            vector20 = vector20 * 13f;
                            baseRigidBody.AddForce(force: vector20, mode: ForceMode.Impulse);
                            if (bulletRight != null)
                            {
                                bulletRight.GetComponent<Bullet>().disable();
                                releaseIfIHookSb();
                            }

                            if (bulletLeft != null)
                            {
                                bulletLeft.GetComponent<Bullet>().disable();
                                releaseIfIHookSb();
                            }
                        }

                        baseRigidBody.AddForce(Vector3.up * 2f, mode: ForceMode.Impulse);
                    }

                    var flag7 = false;
                    if (bulletLeft != null || bulletRight != null)
                    {
                        if (bulletLeft != null && bulletLeft.transform.position.y > gameObject.transform.position.y && isLaunchLeft && bulletLeft.GetComponent<Bullet>().isHooked())
                            flag7 = true;
                        if (bulletRight != null && bulletRight.transform.position.y > gameObject.transform.position.y && isLaunchRight && bulletRight.GetComponent<Bullet>().isHooked())
                            flag7 = true;
                    }

                    if (flag7)
                        baseRigidBody.AddForce(new Vector3(0f, -10f * baseRigidBody.mass, 0f));
                    else
                        baseRigidBody.AddForce(new Vector3(0f, -gravity * baseRigidBody.mass, 0f));
                    if (currentSpeed > 10f)
                        currentCamera.GetComponent<Camera>().fieldOfView = Mathf.Lerp(from: currentCamera.GetComponent<Camera>().fieldOfView, Mathf.Min(100f, currentSpeed + 40f), 0.1f);
                    else
                        currentCamera.GetComponent<Camera>().fieldOfView = Mathf.Lerp(from: currentCamera.GetComponent<Camera>().fieldOfView, 50f, 0.1f);
                    if (flag2)
                    {
                        useGas(useGasSpeed * Time.deltaTime);
                        if (!smoke_3dmg.enableEmission && IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && photonView.isMine)
                        {
                            object[] parameters = { true };
                            photonView.RPC("net3DMGSMOKE", target: PhotonTargets.Others, parameters: parameters);
                        }

                        smoke_3dmg.enableEmission = true;
                    }
                    else
                    {
                        if (smoke_3dmg.enableEmission && IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && photonView.isMine)
                        {
                            object[] objArray3 = { false };
                            photonView.RPC("net3DMGSMOKE", target: PhotonTargets.Others, parameters: objArray3);
                        }

                        smoke_3dmg.enableEmission = false;
                    }

                    if (currentSpeed > 80f)
                    {
                        if (!speedFXPS.enableEmission)
                            speedFXPS.enableEmission = true;
                        speedFXPS.startSpeed = currentSpeed;
                        speedFX.transform.LookAt(baseTransform.position + baseRigidBody.velocity);
                    }
                    else if (speedFXPS.enableEmission)
                    {
                        speedFXPS.enableEmission = false;
                    }
                }
            }
        }
    }

    public string getDebugInfo()
    {
        var str = "\n";
        str = "Left:" + isLeftHandHooked + " ";
        if (isLeftHandHooked && bulletLeft != null)
        {
            var vector = bulletLeft.transform.position - transform.position;
            str = str + (int)(Mathf.Atan2(y: vector.x, x: vector.z) * 57.29578f);
        }

        var str2 = str;
        object[] objArray1 = { str2, "\nRight:", isRightHandHooked, " " };
        str = string.Concat(args: objArray1);
        if (isRightHandHooked && bulletRight != null)
        {
            var vector2 = bulletRight.transform.position - transform.position;
            str = str + (int)(Mathf.Atan2(y: vector2.x, x: vector2.z) * 57.29578f);
        }

        str = str + "\nfacingDirection:" + (int)facingDirection + "\nActual facingDirection:" + (int)transform.rotation.eulerAngles.y + "\nState:" + state + "\n\n\n\n\n";
        if (state == HERO_STATE.Attack)
            targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
        return str;
    }

    private Vector3 getGlobaleFacingVector3(float resultAngle)
    {
        var num = -resultAngle + 90f;
        var x = Mathf.Cos(num * 0.01745329f);
        return new Vector3(x: x, 0f, Mathf.Sin(num * 0.01745329f));
    }

    private Vector3 getGlobaleFacingVector3(float horizontal, float vertical)
    {
        var num = -getGlobalFacingDirection(horizontal: horizontal, vertical: vertical) + 90f;
        var x = Mathf.Cos(num * 0.01745329f);
        return new Vector3(x: x, 0f, Mathf.Sin(num * 0.01745329f));
    }

    private float getGlobalFacingDirection(float horizontal, float vertical)
    {
        if (vertical == 0f && horizontal == 0f)
            return transform.rotation.eulerAngles.y;
        var y = currentCamera.transform.rotation.eulerAngles.y;
        var num2 = Mathf.Atan2(y: vertical, x: horizontal) * 57.29578f;
        num2 = -num2 + 90f;
        return y + num2;
    }

    private float getLeanAngle(Vector3 p, bool left)
    {
        if (!useGun && state == HERO_STATE.Attack)
            return 0f;
        var num = p.y - transform.position.y;
        var num2 = Vector3.Distance(a: p, b: transform.position);
        var a = Mathf.Acos(num / num2) * 57.29578f;
        a *= 0.1f;
        a *= 1f + Mathf.Pow(f: rigidbody.velocity.magnitude, 0.2f);
        var vector3 = p - transform.position;
        var current = Mathf.Atan2(y: vector3.x, x: vector3.z) * 57.29578f;
        var target = Mathf.Atan2(y: rigidbody.velocity.x, x: rigidbody.velocity.z) * 57.29578f;
        var num6 = Mathf.DeltaAngle(current: current, target: target);
        a += Mathf.Abs(num6 * 0.5f);
        if (state != HERO_STATE.Attack)
            a = Mathf.Min(a: a, 80f);
        if (num6 > 0f)
            leanLeft = true;
        else
            leanLeft = false;
        if (useGun)
            return a * (num6 >= 0f ? 1 : -1);
        var num7 = 0f;
        if (left && num6 < 0f || !left && num6 > 0f)
            num7 = 0.1f;
        else
            num7 = 0.5f;
        return a * (num6 >= 0f ? num7 : -num7);
    }

    private void getOffHorse()
    {
        playAnimation("horse_getoff");
        rigidbody.AddForce(Vector3.up * 10f - transform.forward * 2f - transform.right * 1f, mode: ForceMode.VelocityChange);
        unmounted();
    }

    private void getOnHorse()
    {
        playAnimation("horse_geton");
        facingDirection = myHorse.transform.rotation.eulerAngles.y;
        targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
    }

    public void getSupply()
    {
        if ((animation.IsPlaying(name: standAnimation) || animation.IsPlaying("run") || animation.IsPlaying("run_sasha")) && (currentBladeSta != totalBladeSta || currentBladeNum != totalBladeNum || currentGas != totalGas || leftBulletLeft != bulletMAX || rightBulletLeft != bulletMAX))
        {
            state = HERO_STATE.FillGas;
            crossFade("supply", 0.1f);
        }
    }

    public void grabbed(GameObject titan, bool leftHand)
    {
        if (isMounted)
            unmounted();
        state = HERO_STATE.Grab;
        GetComponent<CapsuleCollider>().isTrigger = true;
        falseAttack();
        titanWhoGrabMe = titan;
        if (titanForm && eren_titan != null)
            eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
        if (!useGun && (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine))
        {
            leftbladetrail.Deactivate();
            rightbladetrail.Deactivate();
            leftbladetrail2.Deactivate();
            rightbladetrail2.Deactivate();
        }

        smoke_3dmg.enableEmission = false;
        sparks.enableEmission = false;
    }

    public bool HasDied()
    {
        return hasDied || isInvincible();
    }

    private void headMovement()
    {
        var transform = this.transform.Find("Amarture/Controller_Body/hip/spine/chest/neck/head");
        var transform2 = this.transform.Find("Amarture/Controller_Body/hip/spine/chest/neck");
        var x = Mathf.Sqrt((gunTarget.x - this.transform.position.x) * (gunTarget.x - this.transform.position.x) + (gunTarget.z - this.transform.position.z) * (gunTarget.z - this.transform.position.z));
        targetHeadRotation = transform.rotation;
        var vector5 = gunTarget - this.transform.position;
        var current = -Mathf.Atan2(y: vector5.z, x: vector5.x) * 57.29578f;
        var num3 = -Mathf.DeltaAngle(current: current, this.transform.rotation.eulerAngles.y - 90f);
        num3 = Mathf.Clamp(value: num3, -40f, 40f);
        var y = transform2.position.y - gunTarget.y;
        var num5 = Mathf.Atan2(y: y, x: x) * 57.29578f;
        num5 = Mathf.Clamp(value: num5, -40f, 30f);
        targetHeadRotation = Quaternion.Euler(transform.rotation.eulerAngles.x + num5, transform.rotation.eulerAngles.y + num3, z: transform.rotation.eulerAngles.z);
        oldHeadRotation = Quaternion.Lerp(from: oldHeadRotation, to: targetHeadRotation, Time.deltaTime * 60f);
        transform.rotation = oldHeadRotation;
    }

    public void hookedByHuman(int hooker, Vector3 hookPosition)
    {
        object[] parameters = { hooker, hookPosition };
        photonView.RPC("RPCHookedByHuman", targetPlayer: photonView.owner, parameters: parameters);
    }

    [RPC]
    public void hookFail()
    {
        hookTarget = null;
        hookSomeOne = false;
    }

    public void hookToHuman(GameObject target, Vector3 hookPosition)
    {
        releaseIfIHookSb();
        hookTarget = target;
        hookSomeOne = true;
        if (target.GetComponent<HERO>() != null)
            target.GetComponent<HERO>().hookedByHuman(hooker: photonView.viewID, hookPosition: hookPosition);
        launchForce = hookPosition - transform.position;
        var num = Mathf.Pow(f: launchForce.magnitude, 0.1f);
        if (grounded)
            rigidbody.AddForce(Vector3.up * Mathf.Min(launchForce.magnitude * 0.2f, 10f), mode: ForceMode.Impulse);
        rigidbody.AddForce(launchForce * num * 0.1f, mode: ForceMode.Impulse);
    }

    private void idle()
    {
        if (state == HERO_STATE.Attack)
            falseAttack();
        state = HERO_STATE.Idle;
        crossFade(aniName: standAnimation, 0.1f);
    }

    private bool IsFrontGrounded()
    {
        LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask3 = mask2 | mask;
        return Physics.Raycast(gameObject.transform.position + gameObject.transform.up * 1f, direction: gameObject.transform.forward, 1f, layerMask: mask3.value);
    }

    public bool IsGrounded()
    {
        LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask3 = mask2 | mask;
        return Physics.Raycast(gameObject.transform.position + Vector3.up * 0.1f, direction: -Vector3.up, 0.3f, layerMask: mask3.value);
    }

    public bool isInvincible()
    {
        return invincible > 0f;
    }

    private bool isPressDirectionTowardsHero(float h, float v)
    {
        if (h == 0f && v == 0f)
            return false;
        return Mathf.Abs(Mathf.DeltaAngle(getGlobalFacingDirection(horizontal: h, vertical: v), target: transform.rotation.eulerAngles.y)) < 45f;
    }

    private bool IsUpFrontGrounded()
    {
        LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
        LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
        LayerMask mask3 = mask2 | mask;
        return Physics.Raycast(gameObject.transform.position + gameObject.transform.up * 3f, direction: gameObject.transform.forward, 1.2f, layerMask: mask3.value);
    }

    [RPC]
    private void killObject()
    {
        Destroy(obj: gameObject);
    }

    public void lateUpdate()
    {
        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && myNetWorkName != null)
        {
            if (titanForm && eren_titan != null)
                myNetWorkName.transform.localPosition = Vector3.up * Screen.height * 2f;
            var start = new Vector3(x: transform.position.x, transform.position.y + 2f, z: transform.position.z);
            var obj2 = GameObject.Find("MainCamera");
            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask3 = mask2 | mask;
            if (Vector3.Angle(from: obj2.transform.forward, start - obj2.transform.position) <= 90f && !Physics.Linecast(start: start, end: obj2.transform.position, layerMask: mask3))
            {
                Vector2 vector5 = GameObject.Find("MainCamera").GetComponent<Camera>().WorldToScreenPoint(position: start);
                myNetWorkName.transform.localPosition = new Vector3((int)(vector5.x - Screen.width * 0.5f), (int)(vector5.y - Screen.height * 0.5f), 0f);
            }
            else
            {
                myNetWorkName.transform.localPosition = Vector3.up * Screen.height * 2f;
            }
        }

        if (!titanForm)
        {
            if (IN_GAME_MAIN_CAMERA.cameraTilt == 1 && (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine))
            {
                Quaternion quaternion3;
                var zero = Vector3.zero;
                var position = Vector3.zero;
                if (isLaunchLeft && bulletLeft != null && bulletLeft.GetComponent<Bullet>().isHooked())
                    zero = bulletLeft.transform.position;
                if (isLaunchRight && bulletRight != null && bulletRight.GetComponent<Bullet>().isHooked())
                    position = bulletRight.transform.position;
                var vector8 = Vector3.zero;
                if (zero.magnitude != 0f && position.magnitude == 0f)
                    vector8 = zero;
                else if (zero.magnitude == 0f && position.magnitude != 0f)
                    vector8 = position;
                else if (zero.magnitude != 0f && position.magnitude != 0f)
                    vector8 = (zero + position) * 0.5f;
                var from = Vector3.Project(vector8 - transform.position, onNormal: GameObject.Find("MainCamera").transform.up);
                var vector10 = Vector3.Project(vector8 - transform.position, onNormal: GameObject.Find("MainCamera").transform.right);
                if (vector8.magnitude > 0f)
                {
                    var to = from + vector10;
                    var num = Vector3.Angle(vector8 - transform.position, to: rigidbody.velocity) * 0.005f;
                    var vector14 = GameObject.Find("MainCamera").transform.right + vector10.normalized;
                    quaternion3 = Quaternion.Euler(x: GameObject.Find("MainCamera").transform.rotation.eulerAngles.x, y: GameObject.Find("MainCamera").transform.rotation.eulerAngles.y, vector14.magnitude >= 1f ? -Vector3.Angle(from: from, to: to) * num : Vector3.Angle(from: from, to: to) * num);
                }
                else
                {
                    quaternion3 = Quaternion.Euler(x: GameObject.Find("MainCamera").transform.rotation.eulerAngles.x, y: GameObject.Find("MainCamera").transform.rotation.eulerAngles.y, 0f);
                }

                GameObject.Find("MainCamera").transform.rotation = Quaternion.Lerp(from: GameObject.Find("MainCamera").transform.rotation, to: quaternion3, Time.deltaTime * 2f);
            }

            if (state == HERO_STATE.Grab && titanWhoGrabMe != null)
            {
                if (titanWhoGrabMe.GetComponent<TITAN>() != null)
                {
                    transform.position = titanWhoGrabMe.GetComponent<TITAN>().grabTF.transform.position;
                    transform.rotation = titanWhoGrabMe.GetComponent<TITAN>().grabTF.transform.rotation;
                }
                else if (titanWhoGrabMe.GetComponent<FEMALE_TITAN>() != null)
                {
                    transform.position = titanWhoGrabMe.GetComponent<FEMALE_TITAN>().grabTF.transform.position;
                    transform.rotation = titanWhoGrabMe.GetComponent<FEMALE_TITAN>().grabTF.transform.rotation;
                }
            }

            if (useGun)
            {
                if (!leftArmAim && !rightArmAim)
                {
                    if (!grounded)
                    {
                        handL.localRotation = Quaternion.Euler(90f, 0f, 0f);
                        handR.localRotation = Quaternion.Euler(-90f, 0f, 0f);
                    }
                }
                else
                {
                    var vector17 = gunTarget - transform.position;
                    var current = -Mathf.Atan2(y: vector17.z, x: vector17.x) * 57.29578f;
                    var num3 = -Mathf.DeltaAngle(current: current, transform.rotation.eulerAngles.y - 90f);
                    headMovement();
                    if (!isLeftHandHooked && leftArmAim && num3 < 40f && num3 > -90f)
                        leftArmAimTo(target: gunTarget);
                    if (!isRightHandHooked && rightArmAim && num3 > -40f && num3 < 90f)
                        rightArmAimTo(target: gunTarget);
                }

                if (isLeftHandHooked && bulletLeft != null)
                    leftArmAimTo(target: bulletLeft.transform.position);
                if (isRightHandHooked && bulletRight != null)
                    rightArmAimTo(target: bulletRight.transform.position);
            }

            setHookedPplDirection();
            bodyLean();
            if (!animation.IsPlaying("attack3_2") && !animation.IsPlaying("attack5") && !animation.IsPlaying("special_petra"))
                rigidbody.rotation = Quaternion.Lerp(from: gameObject.transform.rotation, to: targetRotation, Time.deltaTime * 6f);
        }
    }

    public void lateUpdate2()
    {
        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && myNetWorkName != null)
        {
            if (titanForm && eren_titan != null)
                myNetWorkName.transform.localPosition = Vector3.up * Screen.height * 2f;
            var start = new Vector3(x: baseTransform.position.x, baseTransform.position.y + 2f, z: baseTransform.position.z);
            var maincamera = this.maincamera;
            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask3 = mask2 | mask;
            if (Vector3.Angle(from: maincamera.transform.forward, start - maincamera.transform.position) > 90f || Physics.Linecast(start: start, end: maincamera.transform.position, layerMask: mask3))
            {
                myNetWorkName.transform.localPosition = Vector3.up * Screen.height * 2f;
            }
            else
            {
                Vector2 vector2 = this.maincamera.GetComponent<Camera>().WorldToScreenPoint(position: start);
                myNetWorkName.transform.localPosition = new Vector3((int)(vector2.x - Screen.width * 0.5f), (int)(vector2.y - Screen.height * 0.5f), 0f);
            }
        }

        if (!titanForm && !isCannon)
        {
            if (IN_GAME_MAIN_CAMERA.cameraTilt == 1 && (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine))
            {
                Quaternion quaternion2;
                var zero = Vector3.zero;
                var position = Vector3.zero;
                if (isLaunchLeft && bulletLeft != null && bulletLeft.GetComponent<Bullet>().isHooked())
                    zero = bulletLeft.transform.position;
                if (isLaunchRight && bulletRight != null && bulletRight.GetComponent<Bullet>().isHooked())
                    position = bulletRight.transform.position;
                var vector5 = Vector3.zero;
                if (zero.magnitude != 0f && position.magnitude == 0f)
                    vector5 = zero;
                else if (zero.magnitude == 0f && position.magnitude != 0f)
                    vector5 = position;
                else if (zero.magnitude != 0f && position.magnitude != 0f)
                    vector5 = (zero + position) * 0.5f;
                var from = Vector3.Project(vector5 - baseTransform.position, onNormal: maincamera.transform.up);
                var vector7 = Vector3.Project(vector5 - baseTransform.position, onNormal: maincamera.transform.right);
                if (vector5.magnitude > 0f)
                {
                    var to = from + vector7;
                    var num = Vector3.Angle(vector5 - baseTransform.position, to: baseRigidBody.velocity) * 0.005f;
                    var vector9 = maincamera.transform.right + vector7.normalized;
                    quaternion2 = Quaternion.Euler(x: maincamera.transform.rotation.eulerAngles.x, y: maincamera.transform.rotation.eulerAngles.y, vector9.magnitude >= 1f ? -Vector3.Angle(from: from, to: to) * num : Vector3.Angle(from: from, to: to) * num);
                }
                else
                {
                    quaternion2 = Quaternion.Euler(x: maincamera.transform.rotation.eulerAngles.x, y: maincamera.transform.rotation.eulerAngles.y, 0f);
                }

                maincamera.transform.rotation = Quaternion.Lerp(from: maincamera.transform.rotation, to: quaternion2, Time.deltaTime * 2f);
            }

            if (state == HERO_STATE.Grab && titanWhoGrabMe != null)
            {
                if (titanWhoGrabMe.GetComponent<TITAN>() != null)
                {
                    baseTransform.position = titanWhoGrabMe.GetComponent<TITAN>().grabTF.transform.position;
                    baseTransform.rotation = titanWhoGrabMe.GetComponent<TITAN>().grabTF.transform.rotation;
                }
                else if (titanWhoGrabMe.GetComponent<FEMALE_TITAN>() != null)
                {
                    baseTransform.position = titanWhoGrabMe.GetComponent<FEMALE_TITAN>().grabTF.transform.position;
                    baseTransform.rotation = titanWhoGrabMe.GetComponent<FEMALE_TITAN>().grabTF.transform.rotation;
                }
            }

            if (useGun)
            {
                if (leftArmAim || rightArmAim)
                {
                    var vector10 = gunTarget - baseTransform.position;
                    var current = -Mathf.Atan2(y: vector10.z, x: vector10.x) * 57.29578f;
                    var num3 = -Mathf.DeltaAngle(current: current, baseTransform.rotation.eulerAngles.y - 90f);
                    headMovement();
                    if (!isLeftHandHooked && leftArmAim && num3 < 40f && num3 > -90f)
                        leftArmAimTo(target: gunTarget);
                    if (!isRightHandHooked && rightArmAim && num3 > -40f && num3 < 90f)
                        rightArmAimTo(target: gunTarget);
                }
                else if (!grounded)
                {
                    handL.localRotation = Quaternion.Euler(90f, 0f, 0f);
                    handR.localRotation = Quaternion.Euler(-90f, 0f, 0f);
                }

                if (isLeftHandHooked && bulletLeft != null)
                    leftArmAimTo(target: bulletLeft.transform.position);
                if (isRightHandHooked && bulletRight != null)
                    rightArmAimTo(target: bulletRight.transform.position);
            }

            setHookedPplDirection();
            bodyLean();
        }
    }

    public void launch(Vector3 des, bool left = true, bool leviMode = false)
    {
        if (isMounted)
            unmounted();
        if (state != HERO_STATE.Attack)
            idle();
        var vector = des - transform.position;
        if (left)
            launchPointLeft = des;
        else
            launchPointRight = des;
        vector.Normalize();
        vector = vector * 20f;
        if (bulletLeft != null && bulletRight != null && bulletLeft.GetComponent<Bullet>().isHooked() && bulletRight.GetComponent<Bullet>().isHooked())
            vector = vector * 0.8f;
        if (!animation.IsPlaying("attack5") && !animation.IsPlaying("special_petra"))
            leviMode = false;
        else
            leviMode = true;
        if (!leviMode)
        {
            falseAttack();
            idle();
            if (useGun)
            {
                crossFade("AHSS_hook_forward_both", 0.1f);
            }
            else if (left && !isRightHandHooked)
            {
                crossFade("air_hook_l_just", 0.1f);
            }
            else if (!left && !isLeftHandHooked)
            {
                crossFade("air_hook_r_just", 0.1f);
            }
            else
            {
                crossFade("dash", 0.1f);
                animation["dash"].time = 0f;
            }
        }

        if (left)
            isLaunchLeft = true;
        if (!left)
            isLaunchRight = true;
        launchForce = vector;
        if (!leviMode)
        {
            if (vector.y < 30f)
                launchForce += Vector3.up * (30f - vector.y);
            if (des.y >= transform.position.y)
                launchForce += Vector3.up * (des.y - transform.position.y) * 10f;
            rigidbody.AddForce(force: launchForce);
        }

        facingDirection = Mathf.Atan2(y: launchForce.x, x: launchForce.z) * 57.29578f;
        var quaternion = Quaternion.Euler(0f, y: facingDirection, 0f);
        gameObject.transform.rotation = quaternion;
        rigidbody.rotation = quaternion;
        targetRotation = quaternion;
        if (left)
            launchElapsedTimeL = 0f;
        else
            launchElapsedTimeR = 0f;
        if (leviMode)
            launchElapsedTimeR = -100f;
        if (animation.IsPlaying("special_petra"))
        {
            launchElapsedTimeR = -100f;
            launchElapsedTimeL = -100f;
            if (bulletRight != null)
            {
                bulletRight.GetComponent<Bullet>().disable();
                releaseIfIHookSb();
            }

            if (bulletLeft != null)
            {
                bulletLeft.GetComponent<Bullet>().disable();
                releaseIfIHookSb();
            }
        }

        sparks.enableEmission = false;
    }

    private void launchLeftRope(RaycastHit hit, bool single, int mode = 0)
    {
        if (currentGas != 0f)
        {
            useGas();
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                bulletLeft = (GameObject)Instantiate(Resources.Load("hook"));
            else if (photonView.isMine)
                bulletLeft = PhotonNetwork.Instantiate("hook", position: transform.position, rotation: transform.rotation, 0);
            var obj2 = !useGun ? hookRefL1 : hookRefL2;
            var str = !useGun ? "hookRefL1" : "hookRefL2";
            bulletLeft.transform.position = obj2.transform.position;
            var component = bulletLeft.GetComponent<Bullet>();
            var num = !single ? hit.distance <= 50f ? hit.distance * 0.05f : hit.distance * 0.3f : 0f;
            var vector = hit.point - transform.right * num - bulletLeft.transform.position;
            vector.Normalize();
            if (mode == 1)
                component.launch(vector * 3f, v2: rigidbody.velocity, launcher_ref: str, true, hero: gameObject, true);
            else
                component.launch(vector * 3f, v2: rigidbody.velocity, launcher_ref: str, true, hero: gameObject);
            launchPointLeft = Vector3.zero;
        }
    }

    private void launchRightRope(RaycastHit hit, bool single, int mode = 0)
    {
        if (currentGas != 0f)
        {
            useGas();
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                bulletRight = (GameObject)Instantiate(Resources.Load("hook"));
            else if (photonView.isMine)
                bulletRight = PhotonNetwork.Instantiate("hook", position: transform.position, rotation: transform.rotation, 0);
            var obj2 = !useGun ? hookRefR1 : hookRefR2;
            var str = !useGun ? "hookRefR1" : "hookRefR2";
            bulletRight.transform.position = obj2.transform.position;
            var component = bulletRight.GetComponent<Bullet>();
            var num = !single ? hit.distance <= 50f ? hit.distance * 0.05f : hit.distance * 0.3f : 0f;
            var vector = hit.point + transform.right * num - bulletRight.transform.position;
            vector.Normalize();
            if (mode == 1)
                component.launch(vector * 5f, v2: rigidbody.velocity, launcher_ref: str, false, hero: gameObject, true);
            else
                component.launch(vector * 3f, v2: rigidbody.velocity, launcher_ref: str, false, hero: gameObject);
            launchPointRight = Vector3.zero;
        }
    }

    private void leftArmAimTo(Vector3 target)
    {
        var y = target.x - upperarmL.transform.position.x;
        var num2 = target.y - upperarmL.transform.position.y;
        var x = target.z - upperarmL.transform.position.z;
        var num4 = Mathf.Sqrt(y * y + x * x);
        handL.localRotation = Quaternion.Euler(90f, 0f, 0f);
        forearmL.localRotation = Quaternion.Euler(-90f, 0f, 0f);
        upperarmL.rotation = Quaternion.Euler(0f, 90f + Mathf.Atan2(y: y, x: x) * 57.29578f, -Mathf.Atan2(y: num2, x: num4) * 57.29578f);
    }

    public void loadskin()
    {
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
        {
            if ((int)FengGameManagerMKII.settings[0x5d] == 1)
                foreach (var renderer in GetComponentsInChildren<Renderer>())
                {
                    if (renderer.name.Contains("speed"))
                        renderer.enabled = false;
                }

            if ((int)FengGameManagerMKII.settings[0] == 1)
            {
                var index = 14;
                var num3 = 4;
                var num4 = 5;
                var num5 = 6;
                var num6 = 7;
                var num7 = 8;
                var num8 = 9;
                var num9 = 10;
                var num10 = 11;
                var num11 = 12;
                var num12 = 13;
                var num13 = 3;
                var num14 = 0x5e;
                if ((int)FengGameManagerMKII.settings[0x85] == 1)
                {
                    num13 = 0x86;
                    num3 = 0x87;
                    num4 = 0x88;
                    num5 = 0x89;
                    num6 = 0x8a;
                    num7 = 0x8b;
                    num8 = 140;
                    num9 = 0x8d;
                    num10 = 0x8e;
                    num11 = 0x8f;
                    num12 = 0x90;
                    index = 0x91;
                    num14 = 0x92;
                }
                else if ((int)FengGameManagerMKII.settings[0x85] == 2)
                {
                    num13 = 0x93;
                    num3 = 0x94;
                    num4 = 0x95;
                    num5 = 150;
                    num6 = 0x97;
                    num7 = 0x98;
                    num8 = 0x99;
                    num9 = 0x9a;
                    num10 = 0x9b;
                    num11 = 0x9c;
                    num12 = 0x9d;
                    index = 0x9e;
                    num14 = 0x9f;
                }

                var str = (string)FengGameManagerMKII.settings[index];
                var str2 = (string)FengGameManagerMKII.settings[num3];
                var str3 = (string)FengGameManagerMKII.settings[num4];
                var str4 = (string)FengGameManagerMKII.settings[num5];
                var str5 = (string)FengGameManagerMKII.settings[num6];
                var str6 = (string)FengGameManagerMKII.settings[num7];
                var str7 = (string)FengGameManagerMKII.settings[num8];
                var str8 = (string)FengGameManagerMKII.settings[num9];
                var str9 = (string)FengGameManagerMKII.settings[num10];
                var str10 = (string)FengGameManagerMKII.settings[num11];
                var str11 = (string)FengGameManagerMKII.settings[num12];
                var str12 = (string)FengGameManagerMKII.settings[num13];
                var str13 = (string)FengGameManagerMKII.settings[num14];
                var url = str12 + "," + str2 + "," + str3 + "," + str4 + "," + str5 + "," + str6 + "," + str7 + "," + str8 + "," + str9 + "," + str10 + "," + str11 + "," + str + "," + str13;
                if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                {
                    StartCoroutine(loadskinE(-1, url: url));
                }
                else
                {
                    var viewID = -1;
                    if (myHorse != null)
                        viewID = myHorse.GetPhotonView().viewID;
                    photonView.RPC("loadskinRPC", target: PhotonTargets.AllBuffered, viewID, url);
                }
            }
        }
    }

    public IEnumerator loadskinE(int horse, string url)
    {
        while (!hasspawn)
            yield return null;
        var mipmap = true;
        var iteratorVariable1 = false;
        if ((int)FengGameManagerMKII.settings[0x3f] == 1)
            mipmap = false;
        var iteratorVariable2 = url.Split(',');
        var iteratorVariable3 = false;
        if ((int)FengGameManagerMKII.settings[15] == 0)
            iteratorVariable3 = true;
        var iteratorVariable4 = false;
        if (LevelInfo.getInfo(name: FengGameManagerMKII.level).horse || RCSettings.horseMode == 1)
            iteratorVariable4 = true;
        var iteratorVariable5 = false;
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
            iteratorVariable5 = true;
        if (setup.part_hair_1 != null)
        {
            var renderer = setup.part_hair_1.renderer;
            if (iteratorVariable2[1].EndsWith(".jpg") || iteratorVariable2[1].EndsWith(".png") || iteratorVariable2[1].EndsWith(".jpeg"))
            {
                if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[1]))
                {
                    var link = new WWW(iteratorVariable2[1]);
                    yield return link;
                    var iteratorVariable8 = RCextensions.loadimage(link: link, mipmap: mipmap, 0x30d40);
                    link.Dispose();
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[1]))
                    {
                        iteratorVariable1 = true;
                        if (setup.myCostume.hairInfo.id >= 0)
                            renderer.material = CharacterMaterials.materials[key: setup.myCostume.hairInfo.texture];
                        renderer.material.mainTexture = iteratorVariable8;
                        FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[1], value: renderer.material);
                        renderer.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[1]];
                    }
                    else
                    {
                        renderer.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[1]];
                    }
                }
                else
                {
                    renderer.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[1]];
                }
            }
            else if (iteratorVariable2[1].ToLower() == "transparent")
            {
                renderer.enabled = false;
            }
        }

        if (setup.part_cape != null)
        {
            var iteratorVariable9 = setup.part_cape.renderer;
            if (iteratorVariable2[7].EndsWith(".jpg") || iteratorVariable2[7].EndsWith(".png") || iteratorVariable2[7].EndsWith(".jpeg"))
            {
                if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[7]))
                {
                    var iteratorVariable10 = new WWW(iteratorVariable2[7]);
                    yield return iteratorVariable10;
                    var iteratorVariable11 = RCextensions.loadimage(link: iteratorVariable10, mipmap: mipmap, 0x30d40);
                    iteratorVariable10.Dispose();
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[7]))
                    {
                        iteratorVariable1 = true;
                        iteratorVariable9.material.mainTexture = iteratorVariable11;
                        FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[7], value: iteratorVariable9.material);
                        iteratorVariable9.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[7]];
                    }
                    else
                    {
                        iteratorVariable9.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[7]];
                    }
                }
                else
                {
                    iteratorVariable9.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[7]];
                }
            }
            else if (iteratorVariable2[7].ToLower() == "transparent")
            {
                iteratorVariable9.enabled = false;
            }
        }

        if (setup.part_chest_3 != null)
        {
            var iteratorVariable12 = setup.part_chest_3.renderer;
            if (iteratorVariable2[6].EndsWith(".jpg") || iteratorVariable2[6].EndsWith(".png") || iteratorVariable2[6].EndsWith(".jpeg"))
            {
                if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[6]))
                {
                    var iteratorVariable13 = new WWW(iteratorVariable2[6]);
                    yield return iteratorVariable13;
                    var iteratorVariable14 = RCextensions.loadimage(link: iteratorVariable13, mipmap: mipmap, 0x7a120);
                    iteratorVariable13.Dispose();
                    if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[6]))
                    {
                        iteratorVariable1 = true;
                        iteratorVariable12.material.mainTexture = iteratorVariable14;
                        FengGameManagerMKII.linkHash[1].Add(iteratorVariable2[6], value: iteratorVariable12.material);
                        iteratorVariable12.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[6]];
                    }
                    else
                    {
                        iteratorVariable12.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[6]];
                    }
                }
                else
                {
                    iteratorVariable12.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[6]];
                }
            }
            else if (iteratorVariable2[6].ToLower() == "transparent")
            {
                iteratorVariable12.enabled = false;
            }
        }

        foreach (var iteratorVariable15 in GetComponentsInChildren<Renderer>())
        {
            if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[1]))
            {
                if (iteratorVariable2[1].EndsWith(".jpg") || iteratorVariable2[1].EndsWith(".png") || iteratorVariable2[1].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[1]))
                    {
                        var iteratorVariable16 = new WWW(iteratorVariable2[1]);
                        yield return iteratorVariable16;
                        var iteratorVariable17 = RCextensions.loadimage(link: iteratorVariable16, mipmap: mipmap, 0x30d40);
                        iteratorVariable16.Dispose();
                        if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[1]))
                        {
                            iteratorVariable1 = true;
                            if (setup.myCostume.hairInfo.id >= 0)
                                iteratorVariable15.material = CharacterMaterials.materials[key: setup.myCostume.hairInfo.texture];
                            iteratorVariable15.material.mainTexture = iteratorVariable17;
                            FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[1], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[1]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[1]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[1]];
                    }
                }
                else if (iteratorVariable2[1].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[2]))
            {
                if (iteratorVariable2[2].EndsWith(".jpg") || iteratorVariable2[2].EndsWith(".png") || iteratorVariable2[2].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[2]))
                    {
                        var iteratorVariable18 = new WWW(iteratorVariable2[2]);
                        yield return iteratorVariable18;
                        var iteratorVariable19 = RCextensions.loadimage(link: iteratorVariable18, mipmap: mipmap, 0x30d40);
                        iteratorVariable18.Dispose();
                        if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[2]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTextureScale = iteratorVariable15.material.mainTextureScale * 8f;
                            iteratorVariable15.material.mainTextureOffset = new Vector2(0f, 0f);
                            iteratorVariable15.material.mainTexture = iteratorVariable19;
                            FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[2], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[2]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[2]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[2]];
                    }
                }
                else if (iteratorVariable2[2].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[3]))
            {
                if (iteratorVariable2[3].EndsWith(".jpg") || iteratorVariable2[3].EndsWith(".png") || iteratorVariable2[3].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[3]))
                    {
                        var iteratorVariable20 = new WWW(iteratorVariable2[3]);
                        yield return iteratorVariable20;
                        var iteratorVariable21 = RCextensions.loadimage(link: iteratorVariable20, mipmap: mipmap, 0x30d40);
                        iteratorVariable20.Dispose();
                        if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[3]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTextureScale = iteratorVariable15.material.mainTextureScale * 8f;
                            iteratorVariable15.material.mainTextureOffset = new Vector2(0f, 0f);
                            iteratorVariable15.material.mainTexture = iteratorVariable21;
                            FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[3], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[3]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[3]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[3]];
                    }
                }
                else if (iteratorVariable2[3].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[4]))
            {
                if (iteratorVariable2[4].EndsWith(".jpg") || iteratorVariable2[4].EndsWith(".png") || iteratorVariable2[4].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[4]))
                    {
                        var iteratorVariable22 = new WWW(iteratorVariable2[4]);
                        yield return iteratorVariable22;
                        var iteratorVariable23 = RCextensions.loadimage(link: iteratorVariable22, mipmap: mipmap, 0x30d40);
                        iteratorVariable22.Dispose();
                        if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[4]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTextureScale = iteratorVariable15.material.mainTextureScale * 8f;
                            iteratorVariable15.material.mainTextureOffset = new Vector2(0f, 0f);
                            iteratorVariable15.material.mainTexture = iteratorVariable23;
                            FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[4], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[4]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[4]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[4]];
                    }
                }
                else if (iteratorVariable2[4].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[5]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[6]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[10]))
            {
                if (iteratorVariable2[5].EndsWith(".jpg") || iteratorVariable2[5].EndsWith(".png") || iteratorVariable2[5].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[5]))
                    {
                        var iteratorVariable24 = new WWW(iteratorVariable2[5]);
                        yield return iteratorVariable24;
                        var iteratorVariable25 = RCextensions.loadimage(link: iteratorVariable24, mipmap: mipmap, 0x30d40);
                        iteratorVariable24.Dispose();
                        if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[5]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTexture = iteratorVariable25;
                            FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[5], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[5]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[5]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[5]];
                    }
                }
                else if (iteratorVariable2[5].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[7]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[8]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[9]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[0x18]))
            {
                if (iteratorVariable2[6].EndsWith(".jpg") || iteratorVariable2[6].EndsWith(".png") || iteratorVariable2[6].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[6]))
                    {
                        var iteratorVariable26 = new WWW(iteratorVariable2[6]);
                        yield return iteratorVariable26;
                        var iteratorVariable27 = RCextensions.loadimage(link: iteratorVariable26, mipmap: mipmap, 0x7a120);
                        iteratorVariable26.Dispose();
                        if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[6]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTexture = iteratorVariable27;
                            FengGameManagerMKII.linkHash[1].Add(iteratorVariable2[6], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[6]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[6]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[6]];
                    }
                }
                else if (iteratorVariable2[6].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[11]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[12]))
            {
                if (iteratorVariable2[7].EndsWith(".jpg") || iteratorVariable2[7].EndsWith(".png") || iteratorVariable2[7].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[7]))
                    {
                        var iteratorVariable28 = new WWW(iteratorVariable2[7]);
                        yield return iteratorVariable28;
                        var iteratorVariable29 = RCextensions.loadimage(link: iteratorVariable28, mipmap: mipmap, 0x30d40);
                        iteratorVariable28.Dispose();
                        if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[7]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTexture = iteratorVariable29;
                            FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[7], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[7]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[7]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[7]];
                    }
                }
                else if (iteratorVariable2[7].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[15]) || (iteratorVariable15.name.Contains(FengGameManagerMKII.s[13]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[0x1a])) && !iteratorVariable15.name.Contains("_r"))
            {
                if (iteratorVariable2[8].EndsWith(".jpg") || iteratorVariable2[8].EndsWith(".png") || iteratorVariable2[8].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[8]))
                    {
                        var iteratorVariable30 = new WWW(iteratorVariable2[8]);
                        yield return iteratorVariable30;
                        var iteratorVariable31 = RCextensions.loadimage(link: iteratorVariable30, mipmap: mipmap, 0x7a120);
                        iteratorVariable30.Dispose();
                        if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[8]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTexture = iteratorVariable31;
                            FengGameManagerMKII.linkHash[1].Add(iteratorVariable2[8], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[8]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[8]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[8]];
                    }
                }
                else if (iteratorVariable2[8].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[0x11]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[0x10]) || iteratorVariable15.name.Contains(FengGameManagerMKII.s[0x1a]) && iteratorVariable15.name.Contains("_r"))
            {
                if (iteratorVariable2[9].EndsWith(".jpg") || iteratorVariable2[9].EndsWith(".png") || iteratorVariable2[9].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[9]))
                    {
                        var iteratorVariable32 = new WWW(iteratorVariable2[9]);
                        yield return iteratorVariable32;
                        var iteratorVariable33 = RCextensions.loadimage(link: iteratorVariable32, mipmap: mipmap, 0x7a120);
                        iteratorVariable32.Dispose();
                        if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[9]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTexture = iteratorVariable33;
                            FengGameManagerMKII.linkHash[1].Add(iteratorVariable2[9], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[9]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[9]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[9]];
                    }
                }
                else if (iteratorVariable2[9].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name == FengGameManagerMKII.s[0x12] && iteratorVariable3)
            {
                if (iteratorVariable2[10].EndsWith(".jpg") || iteratorVariable2[10].EndsWith(".png") || iteratorVariable2[10].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[10]))
                    {
                        var iteratorVariable34 = new WWW(iteratorVariable2[10]);
                        yield return iteratorVariable34;
                        var iteratorVariable35 = RCextensions.loadimage(link: iteratorVariable34, mipmap: mipmap, 0x30d40);
                        iteratorVariable34.Dispose();
                        if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[10]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTexture = iteratorVariable35;
                            FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[10], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[10]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[10]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[10]];
                    }
                }
                else if (iteratorVariable2[10].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
            else if (iteratorVariable15.name.Contains(FengGameManagerMKII.s[0x19]))
            {
                if (iteratorVariable2[11].EndsWith(".jpg") || iteratorVariable2[11].EndsWith(".png") || iteratorVariable2[11].EndsWith(".jpeg"))
                {
                    if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[11]))
                    {
                        var iteratorVariable36 = new WWW(iteratorVariable2[11]);
                        yield return iteratorVariable36;
                        var iteratorVariable37 = RCextensions.loadimage(link: iteratorVariable36, mipmap: mipmap, 0x30d40);
                        iteratorVariable36.Dispose();
                        if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[11]))
                        {
                            iteratorVariable1 = true;
                            iteratorVariable15.material.mainTexture = iteratorVariable37;
                            FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[11], value: iteratorVariable15.material);
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[11]];
                        }
                        else
                        {
                            iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[11]];
                        }
                    }
                    else
                    {
                        iteratorVariable15.material = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[11]];
                    }
                }
                else if (iteratorVariable2[11].ToLower() == "transparent")
                {
                    iteratorVariable15.enabled = false;
                }
            }
        }

        if (iteratorVariable4 && horse >= 0)
        {
            var gameObject = PhotonView.Find(viewID: horse).gameObject;
            if (gameObject != null)
                foreach (var iteratorVariable39 in gameObject.GetComponentsInChildren<Renderer>())
                {
                    if (iteratorVariable39.name.Contains(FengGameManagerMKII.s[0x13]))
                    {
                        if (iteratorVariable2[0].EndsWith(".jpg") || iteratorVariable2[0].EndsWith(".png") || iteratorVariable2[0].EndsWith(".jpeg"))
                        {
                            if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[0]))
                            {
                                var iteratorVariable40 = new WWW(iteratorVariable2[0]);
                                yield return iteratorVariable40;
                                var iteratorVariable41 = RCextensions.loadimage(link: iteratorVariable40, mipmap: mipmap, 0x7a120);
                                iteratorVariable40.Dispose();
                                if (!FengGameManagerMKII.linkHash[1].ContainsKey(iteratorVariable2[0]))
                                {
                                    iteratorVariable1 = true;
                                    iteratorVariable39.material.mainTexture = iteratorVariable41;
                                    FengGameManagerMKII.linkHash[1].Add(iteratorVariable2[0], value: iteratorVariable39.material);
                                    iteratorVariable39.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[0]];
                                }
                                else
                                {
                                    iteratorVariable39.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[0]];
                                }
                            }
                            else
                            {
                                iteratorVariable39.material = (Material)FengGameManagerMKII.linkHash[1][iteratorVariable2[0]];
                            }
                        }
                        else if (iteratorVariable2[0].ToLower() == "transparent")
                        {
                            iteratorVariable39.enabled = false;
                        }
                    }
                }
        }

        if (iteratorVariable5 && (iteratorVariable2[12].EndsWith(".jpg") || iteratorVariable2[12].EndsWith(".png") || iteratorVariable2[12].EndsWith(".jpeg")))
        {
            if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[12]))
            {
                var iteratorVariable42 = new WWW(iteratorVariable2[12]);
                yield return iteratorVariable42;
                var iteratorVariable43 = RCextensions.loadimage(link: iteratorVariable42, mipmap: mipmap, 0x30d40);
                iteratorVariable42.Dispose();
                if (!FengGameManagerMKII.linkHash[0].ContainsKey(iteratorVariable2[12]))
                {
                    iteratorVariable1 = true;
                    leftbladetrail.MyMaterial.mainTexture = iteratorVariable43;
                    rightbladetrail.MyMaterial.mainTexture = iteratorVariable43;
                    FengGameManagerMKII.linkHash[0].Add(iteratorVariable2[12], value: leftbladetrail.MyMaterial);
                    leftbladetrail.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                    rightbladetrail.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                    leftbladetrail2.MyMaterial = leftbladetrail.MyMaterial;
                    rightbladetrail2.MyMaterial = leftbladetrail.MyMaterial;
                }
                else
                {
                    leftbladetrail2.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                    rightbladetrail2.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                    leftbladetrail.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                    rightbladetrail.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                }
            }
            else
            {
                leftbladetrail2.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                rightbladetrail2.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                leftbladetrail.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
                rightbladetrail.MyMaterial = (Material)FengGameManagerMKII.linkHash[0][iteratorVariable2[12]];
            }
        }

        if (iteratorVariable1)
            FengGameManagerMKII.instance.unloadAssets();
    }

    [RPC]
    public void loadskinRPC(int horse, string url)
    {
        if ((int)FengGameManagerMKII.settings[0] == 1)
            StartCoroutine(loadskinE(horse: horse, url: url));
    }

    public void markDie()
    {
        hasDied = true;
        state = HERO_STATE.Die;
    }

    [RPC]
    public void moveToRPC(float posX, float posY, float posZ, PhotonMessageInfo info)
    {
        if (info.sender.isMasterClient)
            transform.position = new Vector3(x: posX, y: posY, z: posZ);
    }

    [RPC]
    private void net3DMGSMOKE(bool ifON)
    {
        if (smoke_3dmg != null)
            smoke_3dmg.enableEmission = ifON;
    }

    [RPC]
    private void netContinueAnimation()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                if (current != null && current.speed == 1f)
                    return;
                current.speed = 1f;
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }

        playAnimation(currentPlayingClipName());
    }

    [RPC]
    private void netCrossFade(string aniName, float time)
    {
        currentAnimation = aniName;
        if (animation != null)
            animation.CrossFade(animation: aniName, fadeLength: time);
    }

    [RPC]
    public void netDie(Vector3 v, bool isBite, int viewID = -1, string titanName = "", bool killByTitan = true, PhotonMessageInfo info = null)
    {
        if (photonView.isMine && info != null && IN_GAME_MAIN_CAMERA.gamemode != GAMEMODE.BOSS_FIGHT_CT)
        {
            if (FengGameManagerMKII.ignoreList.Contains(item: info.sender.ID))
            {
                photonView.RPC("backToHumanRPC", target: PhotonTargets.Others);
                return;
            }

            if (!info.sender.isLocal && !info.sender.isMasterClient)
            {
                if (info.sender.customProperties[key: PhotonPlayerProperty.name] == null || info.sender.customProperties[key: PhotonPlayerProperty.isTitan] == null)
                {
                    FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + "</color>");
                }
                else if (viewID < 0)
                {
                    if (titanName == "")
                        FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + " (possibly valid).</color>");
                    else
                        FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + "</color>");
                }
                else if (PhotonView.Find(viewID: viewID) == null)
                {
                    FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + "</color>");
                }
                else if (PhotonView.Find(viewID: viewID).owner.ID != info.sender.ID)
                {
                    FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + "</color>");
                }
            }
        }

        if (PhotonNetwork.isMasterClient)
        {
            onDeathEvent(viewID: viewID, isTitan: killByTitan);
            var iD = photonView.owner.ID;
            if (FengGameManagerMKII.heroHash.ContainsKey(key: iD))
                FengGameManagerMKII.heroHash.Remove(key: iD);
        }

        if (photonView.isMine)
        {
            var vector = Vector3.up * 5000f;
            if (myBomb != null)
                myBomb.destroyMe();
            if (myCannon != null)
                PhotonNetwork.Destroy(targetGo: myCannon);
            if (titanForm && eren_titan != null)
                eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
            if (skillCD != null)
                skillCD.transform.localPosition = vector;
        }

        if (bulletLeft != null)
            bulletLeft.GetComponent<Bullet>().removeMe();
        if (bulletRight != null)
            bulletRight.GetComponent<Bullet>().removeMe();
        meatDie.Play();
        if (!(useGun || IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && !photonView.isMine))
        {
            leftbladetrail.Deactivate();
            rightbladetrail.Deactivate();
            leftbladetrail2.Deactivate();
            rightbladetrail2.Deactivate();
        }

        falseAttack();
        breakApart2(v: v, isBite: isBite);
        if (photonView.isMine)
        {
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setSpectorMode(false);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            FengGameManagerMKII.instance.myRespawnTime = 0f;
        }

        hasDied = true;
        var transform = this.transform.Find("audio_die");
        if (transform != null)
        {
            transform.parent = null;
            transform.GetComponent<AudioSource>().Play();
        }

        gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
        if (photonView.isMine)
        {
            PhotonNetwork.RemoveRPCs(targetPhotonView: photonView);
            var propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.dead, true);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
            propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.deaths, RCextensions.returnIntFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.deaths]) + 1);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
            object[] parameters = { !(titanName == string.Empty) ? 1 : 0 };
            FengGameManagerMKII.instance.photonView.RPC("someOneIsDead", target: PhotonTargets.MasterClient, parameters: parameters);
            if (viewID != -1)
            {
                var view2 = PhotonView.Find(viewID: viewID);
                if (view2 != null)
                {
                    FengGameManagerMKII.instance.sendKillInfo(t1: killByTitan, "[FFC000][" + info.sender.ID + "][FFFFFF]" + RCextensions.returnStringFromObject(view2.owner.customProperties[key: PhotonPlayerProperty.name]), false,
                        RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]));
                    propertiesToSet = new Hashtable();
                    propertiesToSet.Add(key: PhotonPlayerProperty.kills, RCextensions.returnIntFromObject(view2.owner.customProperties[key: PhotonPlayerProperty.kills]) + 1);
                    view2.owner.SetCustomProperties(propertiesToSet: propertiesToSet);
                }
            }
            else
            {
                FengGameManagerMKII.instance.sendKillInfo(!(titanName == string.Empty), "[FFC000][" + info.sender.ID + "][FFFFFF]" + titanName, false, RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]));
            }
        }

        if (photonView.isMine)
            PhotonNetwork.Destroy(targetView: photonView);
    }

    [RPC]
    private void netDie2(int viewID = -1, string titanName = "", PhotonMessageInfo info = null)
    {
        GameObject obj2;
        if (photonView.isMine && info != null && IN_GAME_MAIN_CAMERA.gamemode != GAMEMODE.BOSS_FIGHT_CT)
        {
            if (FengGameManagerMKII.ignoreList.Contains(item: info.sender.ID))
            {
                photonView.RPC("backToHumanRPC", target: PhotonTargets.Others);
                return;
            }

            if (!info.sender.isLocal && !info.sender.isMasterClient)
            {
                if (info.sender.customProperties[key: PhotonPlayerProperty.name] == null || info.sender.customProperties[key: PhotonPlayerProperty.isTitan] == null)
                {
                    FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + "</color>");
                }
                else if (viewID < 0)
                {
                    if (titanName == "")
                        FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + " (possibly valid).</color>");
                    else if (RCSettings.bombMode == 0 && RCSettings.deadlyCannons == 0)
                        FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + "</color>");
                }
                else if (PhotonView.Find(viewID: viewID) == null)
                {
                    FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + "</color>");
                }
                else if (PhotonView.Find(viewID: viewID).owner.ID != info.sender.ID)
                {
                    FengGameManagerMKII.instance.chatRoom.addLINE("<color=#FFCC00>Unusual Kill from ID " + info.sender.ID + "</color>");
                }
            }
        }

        if (photonView.isMine)
        {
            var vector = Vector3.up * 5000f;
            if (myBomb != null)
                myBomb.destroyMe();
            if (myCannon != null)
                PhotonNetwork.Destroy(targetGo: myCannon);
            PhotonNetwork.RemoveRPCs(targetPhotonView: photonView);
            if (titanForm && eren_titan != null)
                eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
            if (skillCD != null)
                skillCD.transform.localPosition = vector;
        }

        meatDie.Play();
        if (bulletLeft != null)
            bulletLeft.GetComponent<Bullet>().removeMe();
        if (bulletRight != null)
            bulletRight.GetComponent<Bullet>().removeMe();
        var transform = this.transform.Find("audio_die");
        transform.parent = null;
        transform.GetComponent<AudioSource>().Play();
        if (photonView.isMine)
        {
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(null);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setSpectorMode(true);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            FengGameManagerMKII.instance.myRespawnTime = 0f;
        }

        falseAttack();
        hasDied = true;
        gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER && photonView.isMine)
        {
            PhotonNetwork.RemoveRPCs(targetPhotonView: photonView);
            var propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.dead, true);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
            propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.deaths, (int)PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.deaths] + 1);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
            if (viewID != -1)
            {
                var view2 = PhotonView.Find(viewID: viewID);
                if (view2 != null)
                {
                    FengGameManagerMKII.instance.sendKillInfo(true, "[FFC000][" + info.sender.ID + "][FFFFFF]" + RCextensions.returnStringFromObject(view2.owner.customProperties[key: PhotonPlayerProperty.name]), false,
                        RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]));
                    propertiesToSet = new Hashtable();
                    propertiesToSet.Add(key: PhotonPlayerProperty.kills, RCextensions.returnIntFromObject(view2.owner.customProperties[key: PhotonPlayerProperty.kills]) + 1);
                    view2.owner.SetCustomProperties(propertiesToSet: propertiesToSet);
                }
            }
            else
            {
                FengGameManagerMKII.instance.sendKillInfo(true, "[FFC000][" + info.sender.ID + "][FFFFFF]" + titanName, false, RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]));
            }

            object[] parameters = { !(titanName == string.Empty) ? 1 : 0 };
            FengGameManagerMKII.instance.photonView.RPC("someOneIsDead", target: PhotonTargets.MasterClient, parameters: parameters);
        }

        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && photonView.isMine)
            obj2 = PhotonNetwork.Instantiate("hitMeat2", position: this.transform.position, Quaternion.Euler(270f, 0f, 0f), 0);
        else
            obj2 = (GameObject)Instantiate(Resources.Load("hitMeat2"));
        obj2.transform.position = this.transform.position;
        if (photonView.isMine)
            PhotonNetwork.Destroy(targetView: photonView);
        if (PhotonNetwork.isMasterClient)
        {
            onDeathEvent(viewID: viewID, true);
            var iD = photonView.owner.ID;
            if (FengGameManagerMKII.heroHash.ContainsKey(key: iD))
                FengGameManagerMKII.heroHash.Remove(key: iD);
        }
    }

    public void netDieLocal(Vector3 v, bool isBite, int viewID = -1, string titanName = "", bool killByTitan = true)
    {
        if (photonView.isMine)
        {
            var vector = Vector3.up * 5000f;
            if (titanForm && eren_titan != null)
                eren_titan.GetComponent<TITAN_EREN>().lifeTime = 0.1f;
            if (myBomb != null)
                myBomb.destroyMe();
            if (myCannon != null)
                PhotonNetwork.Destroy(targetGo: myCannon);
            if (skillCD != null)
                skillCD.transform.localPosition = vector;
        }

        if (bulletLeft != null)
            bulletLeft.GetComponent<Bullet>().removeMe();
        if (bulletRight != null)
            bulletRight.GetComponent<Bullet>().removeMe();
        meatDie.Play();
        if (!(useGun || IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && !photonView.isMine))
        {
            leftbladetrail.Deactivate();
            rightbladetrail.Deactivate();
            leftbladetrail2.Deactivate();
            rightbladetrail2.Deactivate();
        }

        falseAttack();
        breakApart2(v: v, isBite: isBite);
        if (photonView.isMine)
        {
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().setSpectorMode(false);
            currentCamera.GetComponent<IN_GAME_MAIN_CAMERA>().gameOver = true;
            FengGameManagerMKII.instance.myRespawnTime = 0f;
        }

        hasDied = true;
        var transform = this.transform.Find("audio_die");
        transform.parent = null;
        transform.GetComponent<AudioSource>().Play();
        gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
        if (photonView.isMine)
        {
            PhotonNetwork.RemoveRPCs(targetPhotonView: photonView);
            var propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.dead, true);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
            propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.deaths, RCextensions.returnIntFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.deaths]) + 1);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
            object[] parameters = { !(titanName == string.Empty) ? 1 : 0 };
            FengGameManagerMKII.instance.photonView.RPC("someOneIsDead", target: PhotonTargets.MasterClient, parameters: parameters);
            if (viewID != -1)
            {
                var view = PhotonView.Find(viewID: viewID);
                if (view != null)
                {
                    FengGameManagerMKII.instance.sendKillInfo(t1: killByTitan, RCextensions.returnStringFromObject(view.owner.customProperties[key: PhotonPlayerProperty.name]), false, RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]));
                    propertiesToSet = new Hashtable();
                    propertiesToSet.Add(key: PhotonPlayerProperty.kills, RCextensions.returnIntFromObject(view.owner.customProperties[key: PhotonPlayerProperty.kills]) + 1);
                    view.owner.SetCustomProperties(propertiesToSet: propertiesToSet);
                }
            }
            else
            {
                FengGameManagerMKII.instance.sendKillInfo(!(titanName == string.Empty), killer: titanName, false, RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]));
            }
        }

        if (photonView.isMine)
            PhotonNetwork.Destroy(targetView: photonView);
        if (PhotonNetwork.isMasterClient)
        {
            onDeathEvent(viewID: viewID, isTitan: killByTitan);
            var iD = photonView.owner.ID;
            if (FengGameManagerMKII.heroHash.ContainsKey(key: iD))
                FengGameManagerMKII.heroHash.Remove(key: iD);
        }
    }

    [RPC]
    private void netGrabbed(int id, bool leftHand)
    {
        titanWhoGrabMeID = id;
        grabbed(titan: PhotonView.Find(viewID: id).gameObject, leftHand: leftHand);
    }

    [RPC]
    private void netlaughAttack()
    {
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("titan"))
        {
            if (Vector3.Distance(a: obj2.transform.position, b: transform.position) < 50f && Vector3.Angle(from: obj2.transform.forward, transform.position - obj2.transform.position) < 90f && obj2.GetComponent<TITAN>() != null)
                obj2.GetComponent<TITAN>().beLaughAttacked();
        }
    }

    [RPC]
    private void netPauseAnimation()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                current.speed = 0f;
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }
    }

    [RPC]
    private void netPlayAnimation(string aniName)
    {
        currentAnimation = aniName;
        if (animation != null)
            animation.Play(animation: aniName);
    }

    [RPC]
    private void netPlayAnimationAt(string aniName, float normalizedTime)
    {
        currentAnimation = aniName;
        if (animation != null)
        {
            animation.Play(animation: aniName);
            animation[name: aniName].normalizedTime = normalizedTime;
        }
    }

    [RPC]
    private void netSetIsGrabbedFalse()
    {
        state = HERO_STATE.Idle;
    }

    [RPC]
    private void netTauntAttack(float tauntTime, float distance = 100f)
    {
        foreach (var obj2 in GameObject.FindGameObjectsWithTag("titan"))
        {
            if (Vector3.Distance(a: obj2.transform.position, b: transform.position) < distance && obj2.GetComponent<TITAN>() != null)
                obj2.GetComponent<TITAN>().beTauntedBy(target: gameObject, tauntTime: tauntTime);
        }
    }

    [RPC]
    private void netUngrabbed()
    {
        ungrabbed();
        netPlayAnimation(aniName: standAnimation);
        falseAttack();
    }

    public void onDeathEvent(int viewID, bool isTitan)
    {
        RCEvent event2;
        string[] strArray;
        if (isTitan)
        {
            if (FengGameManagerMKII.RCEvents.ContainsKey("OnPlayerDieByTitan"))
            {
                event2 = (RCEvent)FengGameManagerMKII.RCEvents["OnPlayerDieByTitan"];
                strArray = (string[])FengGameManagerMKII.RCVariableNames["OnPlayerDieByTitan"];
                if (FengGameManagerMKII.playerVariables.ContainsKey(strArray[0]))
                    FengGameManagerMKII.playerVariables[strArray[0]] = photonView.owner;
                else
                    FengGameManagerMKII.playerVariables.Add(strArray[0], value: photonView.owner);
                if (FengGameManagerMKII.titanVariables.ContainsKey(strArray[1]))
                    FengGameManagerMKII.titanVariables[strArray[1]] = PhotonView.Find(viewID: viewID).gameObject.GetComponent<TITAN>();
                else
                    FengGameManagerMKII.titanVariables.Add(strArray[1], PhotonView.Find(viewID: viewID).gameObject.GetComponent<TITAN>());
                event2.checkEvent();
            }
        }
        else if (FengGameManagerMKII.RCEvents.ContainsKey("OnPlayerDieByPlayer"))
        {
            event2 = (RCEvent)FengGameManagerMKII.RCEvents["OnPlayerDieByPlayer"];
            strArray = (string[])FengGameManagerMKII.RCVariableNames["OnPlayerDieByPlayer"];
            if (FengGameManagerMKII.playerVariables.ContainsKey(strArray[0]))
                FengGameManagerMKII.playerVariables[strArray[0]] = photonView.owner;
            else
                FengGameManagerMKII.playerVariables.Add(strArray[0], value: photonView.owner);
            if (FengGameManagerMKII.playerVariables.ContainsKey(strArray[1]))
                FengGameManagerMKII.playerVariables[strArray[1]] = PhotonView.Find(viewID: viewID).owner;
            else
                FengGameManagerMKII.playerVariables.Add(strArray[1], value: PhotonView.Find(viewID: viewID).owner);
            event2.checkEvent();
        }
    }

    private void OnDestroy()
    {
        if (myNetWorkName != null)
            Destroy(obj: myNetWorkName);
        if (gunDummy != null)
            Destroy(obj: gunDummy);
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER)
            releaseIfIHookSb();
        if (GameObject.Find("MultiplayerManager") != null)
            GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().removeHero(this);
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER && photonView.isMine)
        {
            var vector = Vector3.up * 5000f;
            cross1.transform.localPosition = vector;
            cross2.transform.localPosition = vector;
            crossL1.transform.localPosition = vector;
            crossL2.transform.localPosition = vector;
            crossR1.transform.localPosition = vector;
            crossR2.transform.localPosition = vector;
            LabelDistance.transform.localPosition = vector;
        }

        if (setup.part_cape != null)
            ClothFactory.DisposeObject(cachedObject: setup.part_cape);
        if (setup.part_hair_1 != null)
            ClothFactory.DisposeObject(cachedObject: setup.part_hair_1);
        if (setup.part_hair_2 != null)
            ClothFactory.DisposeObject(cachedObject: setup.part_hair_2);
    }

    public void pauseAnimation()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                if (current != null)
                    current.speed = 0f;
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }

        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && photonView.isMine)
            photonView.RPC("netPauseAnimation", target: PhotonTargets.Others);
    }

    public void playAnimation(string aniName)
    {
        currentAnimation = aniName;
        animation.Play(animation: aniName);
        if (PhotonNetwork.connected && photonView.isMine)
        {
            object[] parameters = { aniName };
            photonView.RPC("netPlayAnimation", target: PhotonTargets.Others, parameters: parameters);
        }
    }

    private void playAnimationAt(string aniName, float normalizedTime)
    {
        currentAnimation = aniName;
        animation.Play(animation: aniName);
        animation[name: aniName].normalizedTime = normalizedTime;
        if (PhotonNetwork.connected && photonView.isMine)
        {
            object[] parameters = { aniName, normalizedTime };
            photonView.RPC("netPlayAnimationAt", target: PhotonTargets.Others, parameters: parameters);
        }
    }

    private void releaseIfIHookSb()
    {
        if (hookSomeOne && hookTarget != null)
        {
            hookTarget.GetPhotonView().RPC("badGuyReleaseMe", targetPlayer: hookTarget.GetPhotonView().owner);
            hookTarget = null;
            hookSomeOne = false;
        }
    }

    public IEnumerator reloadSky()
    {
        yield return new WaitForSeconds(0.5f);
        if (FengGameManagerMKII.skyMaterial != null && Camera.main.GetComponent<Skybox>().material != FengGameManagerMKII.skyMaterial)
            Camera.main.GetComponent<Skybox>().material = FengGameManagerMKII.skyMaterial;
    }

    public void resetAnimationSpeed()
    {
        var enumerator = animation.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = (AnimationState)enumerator.Current;
                if (current != null)
                    current.speed = 1f;
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }

        customAnimationSpeed();
    }

    [RPC]
    public void ReturnFromCannon(PhotonMessageInfo info)
    {
        if (info.sender == photonView.owner)
        {
            isCannon = false;
            gameObject.GetComponent<SmoothSyncMovement>().disabled = false;
        }
    }

    private void rightArmAimTo(Vector3 target)
    {
        var y = target.x - upperarmR.transform.position.x;
        var num2 = target.y - upperarmR.transform.position.y;
        var x = target.z - upperarmR.transform.position.z;
        var num4 = Mathf.Sqrt(y * y + x * x);
        handR.localRotation = Quaternion.Euler(-90f, 0f, 0f);
        forearmR.localRotation = Quaternion.Euler(90f, 0f, 0f);
        upperarmR.rotation = Quaternion.Euler(180f, 90f + Mathf.Atan2(y: y, x: x) * 57.29578f, Mathf.Atan2(y: num2, x: num4) * 57.29578f);
    }

    [RPC]
    private void RPCHookedByHuman(int hooker, Vector3 hookPosition)
    {
        hookBySomeOne = true;
        badGuy = PhotonView.Find(viewID: hooker).gameObject;
        if (Vector3.Distance(a: hookPosition, b: transform.position) < 15f)
        {
            launchForce = PhotonView.Find(viewID: hooker).gameObject.transform.position - transform.position;
            rigidbody.AddForce(-rigidbody.velocity * 0.9f, mode: ForceMode.VelocityChange);
            var num = Mathf.Pow(f: launchForce.magnitude, 0.1f);
            if (grounded)
                rigidbody.AddForce(Vector3.up * Mathf.Min(launchForce.magnitude * 0.2f, 10f), mode: ForceMode.Impulse);
            rigidbody.AddForce(launchForce * num * 0.1f, mode: ForceMode.Impulse);
            if (state != HERO_STATE.Grab)
            {
                dashTime = 1f;
                crossFade("dash", 0.05f);
                animation["dash"].time = 0.1f;
                state = HERO_STATE.AirDodge;
                falseAttack();
                facingDirection = Mathf.Atan2(y: launchForce.x, x: launchForce.z) * 57.29578f;
                var quaternion = Quaternion.Euler(0f, y: facingDirection, 0f);
                gameObject.transform.rotation = quaternion;
                rigidbody.rotation = quaternion;
                targetRotation = quaternion;
            }
        }
        else
        {
            hookBySomeOne = false;
            badGuy = null;
            PhotonView.Find(viewID: hooker).RPC("hookFail", targetPlayer: PhotonView.Find(viewID: hooker).owner);
        }
    }

    private void salute()
    {
        state = HERO_STATE.Salute;
        crossFade("salute", 0.1f);
    }

    private void setHookedPplDirection()
    {
        almostSingleHook = false;
        if (isRightHandHooked && isLeftHandHooked)
        {
            if (bulletLeft != null && bulletRight != null)
            {
                var normal = bulletLeft.transform.position - bulletRight.transform.position;
                if (normal.sqrMagnitude < 4f)
                {
                    var vector2 = (bulletLeft.transform.position + bulletRight.transform.position) * 0.5f - transform.position;
                    facingDirection = Mathf.Atan2(y: vector2.x, x: vector2.z) * 57.29578f;
                    if (useGun && state != HERO_STATE.Attack)
                    {
                        var current = -Mathf.Atan2(y: rigidbody.velocity.z, x: rigidbody.velocity.x) * 57.29578f;
                        var target = -Mathf.Atan2(y: vector2.z, x: vector2.x) * 57.29578f;
                        var num3 = -Mathf.DeltaAngle(current: current, target: target);
                        facingDirection += num3;
                    }

                    almostSingleHook = true;
                }
                else
                {
                    var to = transform.position - bulletLeft.transform.position;
                    var vector6 = transform.position - bulletRight.transform.position;
                    var vector7 = (bulletLeft.transform.position + bulletRight.transform.position) * 0.5f;
                    var from = transform.position - vector7;
                    if (Vector3.Angle(from: from, to: to) < 30f && Vector3.Angle(from: from, to: vector6) < 30f)
                    {
                        almostSingleHook = true;
                        var vector9 = vector7 - transform.position;
                        facingDirection = Mathf.Atan2(y: vector9.x, x: vector9.z) * 57.29578f;
                    }
                    else
                    {
                        almostSingleHook = false;
                        var forward = transform.forward;
                        Vector3.OrthoNormalize(normal: ref normal, tangent: ref forward);
                        facingDirection = Mathf.Atan2(y: forward.x, x: forward.z) * 57.29578f;
                        var num4 = Mathf.Atan2(y: to.x, x: to.z) * 57.29578f;
                        if (Mathf.DeltaAngle(current: num4, target: facingDirection) > 0f)
                            facingDirection += 180f;
                    }
                }
            }
        }
        else
        {
            almostSingleHook = true;
            var zero = Vector3.zero;
            if (isRightHandHooked && bulletRight != null)
            {
                zero = bulletRight.transform.position - transform.position;
            }
            else
            {
                if (!isLeftHandHooked || bulletLeft == null)
                    return;
                zero = bulletLeft.transform.position - transform.position;
            }

            facingDirection = Mathf.Atan2(y: zero.x, x: zero.z) * 57.29578f;
            if (state != HERO_STATE.Attack)
            {
                var num6 = -Mathf.Atan2(y: rigidbody.velocity.z, x: rigidbody.velocity.x) * 57.29578f;
                var num7 = -Mathf.Atan2(y: zero.z, x: zero.x) * 57.29578f;
                var num8 = -Mathf.DeltaAngle(current: num6, target: num7);
                if (useGun)
                {
                    facingDirection += num8;
                }
                else
                {
                    var num9 = 0f;
                    if (isLeftHandHooked && num8 < 0f || isRightHandHooked && num8 > 0f)
                        num9 = -0.1f;
                    else
                        num9 = 0.1f;
                    facingDirection += num8 * num9;
                }
            }
        }
    }

    [RPC]
    public void SetMyCannon(int viewID, PhotonMessageInfo info)
    {
        if (info.sender == photonView.owner)
        {
            var view = PhotonView.Find(viewID: viewID);
            if (view != null)
            {
                myCannon = view.gameObject;
                if (myCannon != null)
                {
                    myCannonBase = myCannon.transform;
                    myCannonPlayer = myCannonBase.Find("PlayerPoint");
                    isCannon = true;
                }
            }
        }
    }

    [RPC]
    public void SetMyPhotonCamera(float offset, PhotonMessageInfo info)
    {
        if (photonView.owner == info.sender)
        {
            CameraMultiplier = offset;
            GetComponent<SmoothSyncMovement>().PhotonCamera = true;
            isPhotonCamera = true;
        }
    }

    [RPC]
    private void setMyTeam(int val)
    {
        myTeam = val;
        checkBoxLeft.GetComponent<TriggerColliderWeapon>().myTeam = val;
        checkBoxRight.GetComponent<TriggerColliderWeapon>().myTeam = val;
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER && PhotonNetwork.isMasterClient)
        {
            object[] objArray;
            if (RCSettings.friendlyMode > 0)
            {
                if (val != 1)
                {
                    objArray = new object[] { 1 };
                    photonView.RPC("setMyTeam", target: PhotonTargets.AllBuffered, parameters: objArray);
                }
            }
            else if (RCSettings.pvpMode == 1)
            {
                var num = 0;
                if (photonView.owner.customProperties[key: PhotonPlayerProperty.RCteam] != null)
                    num = RCextensions.returnIntFromObject(photonView.owner.customProperties[key: PhotonPlayerProperty.RCteam]);
                if (val != num)
                {
                    objArray = new object[] { num };
                    photonView.RPC("setMyTeam", target: PhotonTargets.AllBuffered, parameters: objArray);
                }
            }
            else if (RCSettings.pvpMode == 2 && val != photonView.owner.ID)
            {
                objArray = new object[] { photonView.owner.ID };
                photonView.RPC("setMyTeam", target: PhotonTargets.AllBuffered, parameters: objArray);
            }
        }
    }

    public void setSkillHUDPosition()
    {
        skillCD = GameObject.Find("skill_cd_" + skillId);
        if (skillCD != null)
            skillCD.transform.localPosition = GameObject.Find("skill_cd_bottom").transform.localPosition;
        if (useGun)
            skillCD.transform.localPosition = Vector3.up * 5000f;
    }

    public void setSkillHUDPosition2()
    {
        skillCD = GameObject.Find("skill_cd_" + skillIDHUD);
        if (skillCD != null)
            skillCD.transform.localPosition = GameObject.Find("skill_cd_bottom").transform.localPosition;
        if (useGun && RCSettings.bombMode == 0)
            skillCD.transform.localPosition = Vector3.up * 5000f;
    }

    public void setStat()
    {
        skillCDLast = 1.5f;
        skillId = setup.myCostume.stat.skillId;
        if (skillId == "levi")
            skillCDLast = 3.5f;
        customAnimationSpeed();
        if (skillId == "armin")
            skillCDLast = 5f;
        if (skillId == "marco")
            skillCDLast = 10f;
        if (skillId == "jean")
            skillCDLast = 0.001f;
        if (skillId == "eren")
        {
            skillCDLast = 120f;
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER)
            {
                if (!LevelInfo.getInfo(name: FengGameManagerMKII.level).teamTitan && LevelInfo.getInfo(name: FengGameManagerMKII.level).type != GAMEMODE.RACING && LevelInfo.getInfo(name: FengGameManagerMKII.level).type != GAMEMODE.PVP_CAPTURE && LevelInfo.getInfo(name: FengGameManagerMKII.level).type != GAMEMODE.TROST)
                {
                    var num = 0;
                    foreach (var player in PhotonNetwork.playerList)
                    {
                        if ((int)player.customProperties[key: PhotonPlayerProperty.isTitan] == 1 && ((string)player.customProperties[key: PhotonPlayerProperty.character]).ToUpper() == "EREN")
                            num++;
                    }

                    if (num > 1)
                    {
                        skillId = "petra";
                        skillCDLast = 1f;
                    }
                }
                else
                {
                    skillId = "petra";
                    skillCDLast = 1f;
                }
            }
        }

        if (skillId == "sasha")
            skillCDLast = 20f;
        if (skillId == "petra")
            skillCDLast = 3.5f;
        skillCDDuration = skillCDLast;
        speed = setup.myCostume.stat.SPD / 10f;
        totalGas = currentGas = setup.myCostume.stat.GAS;
        totalBladeSta = currentBladeSta = setup.myCostume.stat.BLA;
        rigidbody.mass = 0.5f - (setup.myCostume.stat.ACL - 100) * 0.001f;
        GameObject.Find("skill_cd_bottom").transform.localPosition = new Vector3(0f, -Screen.height * 0.5f + 5f, 0f);
        skillCD = GameObject.Find("skill_cd_" + skillId);
        skillCD.transform.localPosition = GameObject.Find("skill_cd_bottom").transform.localPosition;
        GameObject.Find("GasUI").transform.localPosition = GameObject.Find("skill_cd_bottom").transform.localPosition;
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
        {
            GameObject.Find("bulletL").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL1").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR1").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL2").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR2").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL3").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR3").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL4").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR4").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL5").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR5").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL6").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR6").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL7").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR7").GetComponent<UISprite>().enabled = false;
        }

        if (setup.myCostume.uniform_type == UNIFORM_TYPE.CasualAHSS)
        {
            standAnimation = "AHSS_stand_gun";
            useGun = true;
            gunDummy = new GameObject();
            gunDummy.name = "gunDummy";
            gunDummy.transform.position = transform.position;
            gunDummy.transform.rotation = transform.rotation;
            myGroup = GROUP.A;
            setTeam(2);
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
            {
                GameObject.Find("bladeCL").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladeCR").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel1").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader1").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel2").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader2").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel3").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader3").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel4").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader4").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel5").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader5").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bulletL").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL1").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR1").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL2").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR2").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL3").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR3").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL4").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR4").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL5").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR5").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL6").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR6").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL7").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR7").GetComponent<UISprite>().enabled = true;
                skillCD.transform.localPosition = Vector3.up * 5000f;
            }
        }
        else if (setup.myCostume.sex == SEX.FEMALE)
        {
            standAnimation = "stand";
            setTeam(1);
        }
        else
        {
            standAnimation = "stand_levi";
            setTeam(1);
        }
    }

    public void setStat2()
    {
        skillCDLast = 1.5f;
        skillId = setup.myCostume.stat.skillId;
        if (skillId == "levi")
            skillCDLast = 3.5f;
        customAnimationSpeed();
        if (skillId == "armin")
            skillCDLast = 5f;
        if (skillId == "marco")
            skillCDLast = 10f;
        if (skillId == "jean")
            skillCDLast = 0.001f;
        if (skillId == "eren")
        {
            skillCDLast = 120f;
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER)
            {
                if (LevelInfo.getInfo(name: FengGameManagerMKII.level).teamTitan || LevelInfo.getInfo(name: FengGameManagerMKII.level).type == GAMEMODE.RACING || LevelInfo.getInfo(name: FengGameManagerMKII.level).type == GAMEMODE.PVP_CAPTURE || LevelInfo.getInfo(name: FengGameManagerMKII.level).type == GAMEMODE.TROST)
                {
                    skillId = "petra";
                    skillCDLast = 1f;
                }
                else
                {
                    var num = 0;
                    foreach (var player in PhotonNetwork.playerList)
                    {
                        if (RCextensions.returnIntFromObject(player.customProperties[key: PhotonPlayerProperty.isTitan]) == 1 && RCextensions.returnStringFromObject(player.customProperties[key: PhotonPlayerProperty.character]).ToUpper() == "EREN")
                            num++;
                    }

                    if (num > 1)
                    {
                        skillId = "petra";
                        skillCDLast = 1f;
                    }
                }
            }
        }

        if (skillId == "sasha")
            skillCDLast = 20f;
        if (skillId == "petra")
            skillCDLast = 3.5f;
        bombInit();
        speed = setup.myCostume.stat.SPD / 10f;
        totalGas = currentGas = setup.myCostume.stat.GAS;
        totalBladeSta = currentBladeSta = setup.myCostume.stat.BLA;
        baseRigidBody.mass = 0.5f - (setup.myCostume.stat.ACL - 100) * 0.001f;
        GameObject.Find("skill_cd_bottom").transform.localPosition = new Vector3(0f, -Screen.height * 0.5f + 5f, 0f);
        skillCD = GameObject.Find("skill_cd_" + skillIDHUD);
        skillCD.transform.localPosition = GameObject.Find("skill_cd_bottom").transform.localPosition;
        GameObject.Find("GasUI").transform.localPosition = GameObject.Find("skill_cd_bottom").transform.localPosition;
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
        {
            GameObject.Find("bulletL").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL1").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR1").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL2").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR2").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL3").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR3").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL4").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR4").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL5").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR5").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL6").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR6").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletL7").GetComponent<UISprite>().enabled = false;
            GameObject.Find("bulletR7").GetComponent<UISprite>().enabled = false;
        }

        if (setup.myCostume.uniform_type == UNIFORM_TYPE.CasualAHSS)
        {
            standAnimation = "AHSS_stand_gun";
            useGun = true;
            gunDummy = new GameObject();
            gunDummy.name = "gunDummy";
            gunDummy.transform.position = baseTransform.position;
            gunDummy.transform.rotation = baseTransform.rotation;
            myGroup = GROUP.A;
            setTeam2(2);
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
            {
                GameObject.Find("bladeCL").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladeCR").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel1").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader1").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel2").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader2").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel3").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader3").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel4").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader4").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bladel5").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader5").GetComponent<UISprite>().enabled = false;
                GameObject.Find("bulletL").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL1").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR1").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL2").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR2").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL3").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR3").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL4").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR4").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL5").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR5").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL6").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR6").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletL7").GetComponent<UISprite>().enabled = true;
                GameObject.Find("bulletR7").GetComponent<UISprite>().enabled = true;
                if (skillId != "bomb")
                    skillCD.transform.localPosition = Vector3.up * 5000f;
            }
        }
        else if (setup.myCostume.sex == SEX.FEMALE)
        {
            standAnimation = "stand";
            setTeam2(1);
        }
        else
        {
            standAnimation = "stand_levi";
            setTeam2(1);
        }
    }

    public void setTeam(int team)
    {
        setMyTeam(val: team);
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER && photonView.isMine)
        {
            object[] parameters = { team };
            photonView.RPC("setMyTeam", target: PhotonTargets.OthersBuffered, parameters: parameters);
            var propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.team, value: team);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
        }
    }

    public void setTeam2(int team)
    {
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER && photonView.isMine)
        {
            object[] parameters = { team };
            photonView.RPC("setMyTeam", target: PhotonTargets.AllBuffered, parameters: parameters);
            var propertiesToSet = new Hashtable();
            propertiesToSet.Add(key: PhotonPlayerProperty.team, value: team);
            PhotonNetwork.player.SetCustomProperties(propertiesToSet: propertiesToSet);
        }
        else
        {
            setMyTeam(val: team);
        }
    }

    public void shootFlare(int type)
    {
        var flag = false;
        if (type == 1 && flare1CD == 0f)
        {
            flare1CD = flareTotalCD;
            flag = true;
        }

        if (type == 2 && flare2CD == 0f)
        {
            flare2CD = flareTotalCD;
            flag = true;
        }

        if (type == 3 && flare3CD == 0f)
        {
            flare3CD = flareTotalCD;
            flag = true;
        }

        if (flag)
        {
            if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
            {
                var obj2 = (GameObject)Instantiate(Resources.Load("FX/flareBullet" + type), position: transform.position, rotation: transform.rotation);
                obj2.GetComponent<FlareMovement>().dontShowHint();
                Destroy(obj: obj2, 25f);
            }
            else
            {
                PhotonNetwork.Instantiate("FX/flareBullet" + type, position: transform.position, rotation: transform.rotation, 0).GetComponent<FlareMovement>().dontShowHint();
            }
        }
    }

    private void showAimUI()
    {
        Vector3 vector;
        if (Screen.showCursor)
        {
            var obj2 = GameObject.Find("cross1");
            var obj3 = GameObject.Find("cross2");
            var obj4 = GameObject.Find("crossL1");
            var obj5 = GameObject.Find("crossL2");
            var obj6 = GameObject.Find("crossR1");
            var obj7 = GameObject.Find("crossR2");
            var obj8 = GameObject.Find("LabelDistance");
            vector = Vector3.up * 10000f;
            obj7.transform.localPosition = vector;
            obj6.transform.localPosition = vector;
            obj5.transform.localPosition = vector;
            obj4.transform.localPosition = vector;
            obj8.transform.localPosition = vector;
            obj3.transform.localPosition = vector;
            obj2.transform.localPosition = vector;
        }
        else
        {
            RaycastHit hit;
            var ray = Camera.main.ScreenPointToRay(position: Input.mousePosition);
            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask3 = mask2 | mask;
            if (Physics.Raycast(ray: ray, hitInfo: out hit, 1E+07f, layerMask: mask3.value))
            {
                RaycastHit hit2;
                var obj9 = GameObject.Find("cross1");
                var obj10 = GameObject.Find("cross2");
                obj9.transform.localPosition = Input.mousePosition;
                var transform = obj9.transform;
                transform.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                obj10.transform.localPosition = obj9.transform.localPosition;
                vector = hit.point - this.transform.position;
                var magnitude = vector.magnitude;
                var obj11 = GameObject.Find("LabelDistance");
                var str = magnitude <= 1000f ? ((int)magnitude).ToString() : "???";
                obj11.GetComponent<UILabel>().text = str;
                if (magnitude > 120f)
                {
                    var transform2 = obj9.transform;
                    transform2.localPosition += Vector3.up * 10000f;
                    obj11.transform.localPosition = obj10.transform.localPosition;
                }
                else
                {
                    var transform3 = obj10.transform;
                    transform3.localPosition += Vector3.up * 10000f;
                    obj11.transform.localPosition = obj9.transform.localPosition;
                }

                var transform4 = obj11.transform;
                transform4.localPosition -= new Vector3(0f, 15f, 0f);
                var vector2 = new Vector3(0f, 0.4f, 0f);
                vector2 -= this.transform.right * 0.3f;
                var vector3 = new Vector3(0f, 0.4f, 0f);
                vector3 += this.transform.right * 0.3f;
                var num3 = hit.distance <= 50f ? hit.distance * 0.05f : hit.distance * 0.3f;
                var vector4 = hit.point - this.transform.right * num3 - (this.transform.position + vector2);
                var vector5 = hit.point + this.transform.right * num3 - (this.transform.position + vector3);
                vector4.Normalize();
                vector5.Normalize();
                vector4 = vector4 * 1000000f;
                vector5 = vector5 * 1000000f;
                if (Physics.Linecast(this.transform.position + vector2, this.transform.position + vector2 + vector4, hitInfo: out hit2, layerMask: mask3.value))
                {
                    var obj12 = GameObject.Find("crossL1");
                    obj12.transform.localPosition = currentCamera.WorldToScreenPoint(position: hit2.point);
                    var transform5 = obj12.transform;
                    transform5.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                    obj12.transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(obj12.transform.localPosition.y - (Input.mousePosition.y - Screen.height * 0.5f), obj12.transform.localPosition.x - (Input.mousePosition.x - Screen.width * 0.5f)) * 57.29578f + 180f);
                    var obj13 = GameObject.Find("crossL2");
                    obj13.transform.localPosition = obj12.transform.localPosition;
                    obj13.transform.localRotation = obj12.transform.localRotation;
                    if (hit2.distance > 120f)
                    {
                        var transform6 = obj12.transform;
                        transform6.localPosition += Vector3.up * 10000f;
                    }
                    else
                    {
                        var transform7 = obj13.transform;
                        transform7.localPosition += Vector3.up * 10000f;
                    }
                }

                if (Physics.Linecast(this.transform.position + vector3, this.transform.position + vector3 + vector5, hitInfo: out hit2, layerMask: mask3.value))
                {
                    var obj14 = GameObject.Find("crossR1");
                    obj14.transform.localPosition = currentCamera.WorldToScreenPoint(position: hit2.point);
                    var transform8 = obj14.transform;
                    transform8.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                    obj14.transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(obj14.transform.localPosition.y - (Input.mousePosition.y - Screen.height * 0.5f), obj14.transform.localPosition.x - (Input.mousePosition.x - Screen.width * 0.5f)) * 57.29578f);
                    var obj15 = GameObject.Find("crossR2");
                    obj15.transform.localPosition = obj14.transform.localPosition;
                    obj15.transform.localRotation = obj14.transform.localRotation;
                    if (hit2.distance > 120f)
                    {
                        var transform9 = obj14.transform;
                        transform9.localPosition += Vector3.up * 10000f;
                    }
                    else
                    {
                        var transform10 = obj15.transform;
                        transform10.localPosition += Vector3.up * 10000f;
                    }
                }
            }
        }
    }

    private void showAimUI2()
    {
        Vector3 vector;
        if (Screen.showCursor)
        {
            var obj2 = cross1;
            var obj3 = cross2;
            var obj4 = crossL1;
            var obj5 = crossL2;
            var obj6 = crossR1;
            var obj7 = crossR2;
            var labelDistance = LabelDistance;
            vector = Vector3.up * 10000f;
            obj7.transform.localPosition = vector;
            obj6.transform.localPosition = vector;
            obj5.transform.localPosition = vector;
            obj4.transform.localPosition = vector;
            labelDistance.transform.localPosition = vector;
            obj3.transform.localPosition = vector;
            obj2.transform.localPosition = vector;
        }
        else
        {
            RaycastHit hit;
            checkTitan();
            var ray = Camera.main.ScreenPointToRay(position: Input.mousePosition);
            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
            LayerMask mask3 = mask2 | mask;
            if (Physics.Raycast(ray: ray, hitInfo: out hit, 1E+07f, layerMask: mask3.value))
            {
                RaycastHit hit2;
                var obj9 = cross1;
                var obj10 = cross2;
                obj9.transform.localPosition = Input.mousePosition;
                var transform = obj9.transform;
                transform.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                obj10.transform.localPosition = obj9.transform.localPosition;
                vector = hit.point - baseTransform.position;
                var magnitude = vector.magnitude;
                var obj11 = LabelDistance;
                var str = magnitude <= 1000f ? ((int)magnitude).ToString() : "???";
                if ((int)FengGameManagerMKII.settings[0xbd] == 1)
                    str = str + "\n" + currentSpeed.ToString("F1") + " u/s";
                else if ((int)FengGameManagerMKII.settings[0xbd] == 2)
                    str = str + "\n" + (currentSpeed / 100f).ToString("F1") + "K";
                obj11.GetComponent<UILabel>().text = str;
                if (magnitude > 120f)
                {
                    var transform11 = obj9.transform;
                    transform11.localPosition += Vector3.up * 10000f;
                    obj11.transform.localPosition = obj10.transform.localPosition;
                }
                else
                {
                    var transform12 = obj10.transform;
                    transform12.localPosition += Vector3.up * 10000f;
                    obj11.transform.localPosition = obj9.transform.localPosition;
                }

                var transform13 = obj11.transform;
                transform13.localPosition -= new Vector3(0f, 15f, 0f);
                var vector2 = new Vector3(0f, 0.4f, 0f);
                vector2 -= baseTransform.right * 0.3f;
                var vector3 = new Vector3(0f, 0.4f, 0f);
                vector3 += baseTransform.right * 0.3f;
                var num4 = hit.distance <= 50f ? hit.distance * 0.05f : hit.distance * 0.3f;
                var vector4 = hit.point - baseTransform.right * num4 - (baseTransform.position + vector2);
                var vector5 = hit.point + baseTransform.right * num4 - (baseTransform.position + vector3);
                vector4.Normalize();
                vector5.Normalize();
                vector4 = vector4 * 1000000f;
                vector5 = vector5 * 1000000f;
                if (Physics.Linecast(baseTransform.position + vector2, baseTransform.position + vector2 + vector4, hitInfo: out hit2, layerMask: mask3.value))
                {
                    var obj12 = crossL1;
                    obj12.transform.localPosition = currentCamera.WorldToScreenPoint(position: hit2.point);
                    var transform14 = obj12.transform;
                    transform14.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                    obj12.transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(obj12.transform.localPosition.y - (Input.mousePosition.y - Screen.height * 0.5f), obj12.transform.localPosition.x - (Input.mousePosition.x - Screen.width * 0.5f)) * 57.29578f + 180f);
                    var obj13 = crossL2;
                    obj13.transform.localPosition = obj12.transform.localPosition;
                    obj13.transform.localRotation = obj12.transform.localRotation;
                    if (hit2.distance > 120f)
                    {
                        var transform15 = obj12.transform;
                        transform15.localPosition += Vector3.up * 10000f;
                    }
                    else
                    {
                        var transform16 = obj13.transform;
                        transform16.localPosition += Vector3.up * 10000f;
                    }
                }

                if (Physics.Linecast(baseTransform.position + vector3, baseTransform.position + vector3 + vector5, hitInfo: out hit2, layerMask: mask3.value))
                {
                    var obj14 = crossR1;
                    obj14.transform.localPosition = currentCamera.WorldToScreenPoint(position: hit2.point);
                    var transform17 = obj14.transform;
                    transform17.localPosition -= new Vector3(Screen.width * 0.5f, Screen.height * 0.5f, 0f);
                    obj14.transform.localRotation = Quaternion.Euler(0f, 0f, Mathf.Atan2(obj14.transform.localPosition.y - (Input.mousePosition.y - Screen.height * 0.5f), obj14.transform.localPosition.x - (Input.mousePosition.x - Screen.width * 0.5f)) * 57.29578f);
                    var obj15 = crossR2;
                    obj15.transform.localPosition = obj14.transform.localPosition;
                    obj15.transform.localRotation = obj14.transform.localRotation;
                    if (hit2.distance > 120f)
                    {
                        var transform18 = obj14.transform;
                        transform18.localPosition += Vector3.up * 10000f;
                    }
                    else
                    {
                        var transform19 = obj15.transform;
                        transform19.localPosition += Vector3.up * 10000f;
                    }
                }
            }
        }
    }

    private void showFlareCD()
    {
        if (GameObject.Find("UIflare1") != null)
        {
            GameObject.Find("UIflare1").GetComponent<UISprite>().fillAmount = (flareTotalCD - flare1CD) / flareTotalCD;
            GameObject.Find("UIflare2").GetComponent<UISprite>().fillAmount = (flareTotalCD - flare2CD) / flareTotalCD;
            GameObject.Find("UIflare3").GetComponent<UISprite>().fillAmount = (flareTotalCD - flare3CD) / flareTotalCD;
        }
    }

    private void showFlareCD2()
    {
        if (cachedSprites["UIflare1"] != null)
        {
            cachedSprites["UIflare1"].fillAmount = (flareTotalCD - flare1CD) / flareTotalCD;
            cachedSprites["UIflare2"].fillAmount = (flareTotalCD - flare2CD) / flareTotalCD;
            cachedSprites["UIflare3"].fillAmount = (flareTotalCD - flare3CD) / flareTotalCD;
        }
    }

    private void showGas()
    {
        var num = currentGas / totalGas;
        var num2 = currentBladeSta / totalBladeSta;
        GameObject.Find("gasL1").GetComponent<UISprite>().fillAmount = currentGas / totalGas;
        GameObject.Find("gasR1").GetComponent<UISprite>().fillAmount = currentGas / totalGas;
        if (!useGun)
        {
            GameObject.Find("bladeCL").GetComponent<UISprite>().fillAmount = currentBladeSta / totalBladeSta;
            GameObject.Find("bladeCR").GetComponent<UISprite>().fillAmount = currentBladeSta / totalBladeSta;
            if (num <= 0f)
            {
                GameObject.Find("gasL").GetComponent<UISprite>().color = Color.red;
                GameObject.Find("gasR").GetComponent<UISprite>().color = Color.red;
            }
            else if (num < 0.3f)
            {
                GameObject.Find("gasL").GetComponent<UISprite>().color = Color.yellow;
                GameObject.Find("gasR").GetComponent<UISprite>().color = Color.yellow;
            }
            else
            {
                GameObject.Find("gasL").GetComponent<UISprite>().color = Color.white;
                GameObject.Find("gasR").GetComponent<UISprite>().color = Color.white;
            }

            if (num2 <= 0f)
            {
                GameObject.Find("bladel1").GetComponent<UISprite>().color = Color.red;
                GameObject.Find("blader1").GetComponent<UISprite>().color = Color.red;
            }
            else if (num2 < 0.3f)
            {
                GameObject.Find("bladel1").GetComponent<UISprite>().color = Color.yellow;
                GameObject.Find("blader1").GetComponent<UISprite>().color = Color.yellow;
            }
            else
            {
                GameObject.Find("bladel1").GetComponent<UISprite>().color = Color.white;
                GameObject.Find("blader1").GetComponent<UISprite>().color = Color.white;
            }

            if (currentBladeNum <= 4)
            {
                GameObject.Find("bladel5").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader5").GetComponent<UISprite>().enabled = false;
            }
            else
            {
                GameObject.Find("bladel5").GetComponent<UISprite>().enabled = true;
                GameObject.Find("blader5").GetComponent<UISprite>().enabled = true;
            }

            if (currentBladeNum <= 3)
            {
                GameObject.Find("bladel4").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader4").GetComponent<UISprite>().enabled = false;
            }
            else
            {
                GameObject.Find("bladel4").GetComponent<UISprite>().enabled = true;
                GameObject.Find("blader4").GetComponent<UISprite>().enabled = true;
            }

            if (currentBladeNum <= 2)
            {
                GameObject.Find("bladel3").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader3").GetComponent<UISprite>().enabled = false;
            }
            else
            {
                GameObject.Find("bladel3").GetComponent<UISprite>().enabled = true;
                GameObject.Find("blader3").GetComponent<UISprite>().enabled = true;
            }

            if (currentBladeNum <= 1)
            {
                GameObject.Find("bladel2").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader2").GetComponent<UISprite>().enabled = false;
            }
            else
            {
                GameObject.Find("bladel2").GetComponent<UISprite>().enabled = true;
                GameObject.Find("blader2").GetComponent<UISprite>().enabled = true;
            }

            if (currentBladeNum <= 0)
            {
                GameObject.Find("bladel1").GetComponent<UISprite>().enabled = false;
                GameObject.Find("blader1").GetComponent<UISprite>().enabled = false;
            }
            else
            {
                GameObject.Find("bladel1").GetComponent<UISprite>().enabled = true;
                GameObject.Find("blader1").GetComponent<UISprite>().enabled = true;
            }
        }
        else
        {
            if (leftGunHasBullet)
                GameObject.Find("bulletL").GetComponent<UISprite>().enabled = true;
            else
                GameObject.Find("bulletL").GetComponent<UISprite>().enabled = false;
            if (rightGunHasBullet)
                GameObject.Find("bulletR").GetComponent<UISprite>().enabled = true;
            else
                GameObject.Find("bulletR").GetComponent<UISprite>().enabled = false;
        }
    }

    private void showGas2()
    {
        var num = currentGas / totalGas;
        var num2 = currentBladeSta / totalBladeSta;
        cachedSprites["gasL1"].fillAmount = currentGas / totalGas;
        cachedSprites["gasR1"].fillAmount = currentGas / totalGas;
        if (!useGun)
        {
            cachedSprites["bladeCL"].fillAmount = currentBladeSta / totalBladeSta;
            cachedSprites["bladeCR"].fillAmount = currentBladeSta / totalBladeSta;
            if (num <= 0f)
            {
                cachedSprites["gasL"].color = Color.red;
                cachedSprites["gasR"].color = Color.red;
            }
            else if (num < 0.3f)
            {
                cachedSprites["gasL"].color = Color.yellow;
                cachedSprites["gasR"].color = Color.yellow;
            }
            else
            {
                cachedSprites["gasL"].color = Color.white;
                cachedSprites["gasR"].color = Color.white;
            }

            if (num2 <= 0f)
            {
                cachedSprites["bladel1"].color = Color.red;
                cachedSprites["blader1"].color = Color.red;
            }
            else if (num2 < 0.3f)
            {
                cachedSprites["bladel1"].color = Color.yellow;
                cachedSprites["blader1"].color = Color.yellow;
            }
            else
            {
                cachedSprites["bladel1"].color = Color.white;
                cachedSprites["blader1"].color = Color.white;
            }

            if (currentBladeNum <= 4)
            {
                cachedSprites["bladel5"].enabled = false;
                cachedSprites["blader5"].enabled = false;
            }
            else
            {
                cachedSprites["bladel5"].enabled = true;
                cachedSprites["blader5"].enabled = true;
            }

            if (currentBladeNum <= 3)
            {
                cachedSprites["bladel4"].enabled = false;
                cachedSprites["blader4"].enabled = false;
            }
            else
            {
                cachedSprites["bladel4"].enabled = true;
                cachedSprites["blader4"].enabled = true;
            }

            if (currentBladeNum <= 2)
            {
                cachedSprites["bladel3"].enabled = false;
                cachedSprites["blader3"].enabled = false;
            }
            else
            {
                cachedSprites["bladel3"].enabled = true;
                cachedSprites["blader3"].enabled = true;
            }

            if (currentBladeNum <= 1)
            {
                cachedSprites["bladel2"].enabled = false;
                cachedSprites["blader2"].enabled = false;
            }
            else
            {
                cachedSprites["bladel2"].enabled = true;
                cachedSprites["blader2"].enabled = true;
            }

            if (currentBladeNum <= 0)
            {
                cachedSprites["bladel1"].enabled = false;
                cachedSprites["blader1"].enabled = false;
            }
            else
            {
                cachedSprites["bladel1"].enabled = true;
                cachedSprites["blader1"].enabled = true;
            }
        }
        else
        {
            if (leftGunHasBullet)
                cachedSprites["bulletL"].enabled = true;
            else
                cachedSprites["bulletL"].enabled = false;
            if (rightGunHasBullet)
                cachedSprites["bulletR"].enabled = true;
            else
                cachedSprites["bulletR"].enabled = false;
        }
    }

    [RPC]
    private void showHitDamage()
    {
        var target = GameObject.Find("LabelScore");
        if (target != null)
        {
            speed = Mathf.Max(10f, b: speed);
            target.GetComponent<UILabel>().text = speed.ToString();
            target.transform.localScale = Vector3.zero;
            speed = (int)(speed * 0.1f);
            speed = Mathf.Clamp(value: speed, 40f, 150f);
            iTween.Stop(target: target);
            object[] args = { "x", speed, "y", speed, "z", speed, "easetype", iTween.EaseType.easeOutElastic, "time", 1f };
            iTween.ScaleTo(target: target, iTween.Hash(args: args));
            object[] objArray2 = { "x", 0, "y", 0, "z", 0, "easetype", iTween.EaseType.easeInBounce, "time", 0.5f, "delay", 2f };
            iTween.ScaleTo(target: target, iTween.Hash(args: objArray2));
        }
    }

    private void showSkillCD()
    {
        if (skillCD != null)
            skillCD.GetComponent<UISprite>().fillAmount = (skillCDLast - skillCDDuration) / skillCDLast;
    }

    [RPC]
    public void SpawnCannonRPC(string settings, PhotonMessageInfo info)
    {
        if (info.sender.isMasterClient && photonView.isMine && myCannon == null)
        {
            if (myHorse != null && isMounted)
                getOffHorse();
            idle();
            if (bulletLeft != null)
                bulletLeft.GetComponent<Bullet>().removeMe();
            if (bulletRight != null)
                bulletRight.GetComponent<Bullet>().removeMe();
            if (smoke_3dmg.enableEmission && IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && photonView.isMine)
            {
                object[] parameters = { false };
                photonView.RPC("net3DMGSMOKE", target: PhotonTargets.Others, parameters: parameters);
            }

            smoke_3dmg.enableEmission = false;
            rigidbody.velocity = Vector3.zero;
            var strArray = settings.Split(',');
            if (strArray.Length > 15)
                myCannon = PhotonNetwork.Instantiate("RCAsset/" + strArray[1], new Vector3(Convert.ToSingle(strArray[12]), Convert.ToSingle(strArray[13]), Convert.ToSingle(strArray[14])),
                    new Quaternion(Convert.ToSingle(strArray[15]), Convert.ToSingle(strArray[0x10]), Convert.ToSingle(strArray[0x11]), Convert.ToSingle(strArray[0x12])), 0);
            else
                myCannon = PhotonNetwork.Instantiate("RCAsset/" + strArray[1], new Vector3(Convert.ToSingle(strArray[2]), Convert.ToSingle(strArray[3]), Convert.ToSingle(strArray[4])),
                    new Quaternion(Convert.ToSingle(strArray[5]), Convert.ToSingle(strArray[6]), Convert.ToSingle(strArray[7]), Convert.ToSingle(strArray[8])), 0);
            myCannonBase = myCannon.transform;
            myCannonPlayer = myCannon.transform.Find("PlayerPoint");
            isCannon = true;
            myCannon.GetComponent<Cannon>().myHero = this;
            myCannonRegion = null;
            Camera.main.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj: myCannon.transform.Find("Barrel").Find("FiringPoint").gameObject);
            Camera.main.fieldOfView = 55f;
            photonView.RPC("SetMyCannon", target: PhotonTargets.OthersBuffered, myCannon.GetPhotonView().viewID);
            skillCDLastCannon = skillCDLast;
            skillCDLast = 3.5f;
            skillCDDuration = 3.5f;
        }
    }

    private void Start()
    {
        FengGameManagerMKII.instance.addHero(this);
        if ((LevelInfo.getInfo(name: FengGameManagerMKII.level).horse || RCSettings.horseMode == 1) && IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER && photonView.isMine)
        {
            myHorse = PhotonNetwork.Instantiate("horse", baseTransform.position + Vector3.up * 5f, rotation: baseTransform.rotation, 0);
            myHorse.GetComponent<Horse>().myHero = gameObject;
            myHorse.GetComponent<TITAN_CONTROLLER>().isHorse = true;
        }

        sparks = baseTransform.Find("slideSparks").GetComponent<ParticleSystem>();
        smoke_3dmg = baseTransform.Find("3dmg_smoke").GetComponent<ParticleSystem>();
        baseTransform.localScale = new Vector3(x: myScale, y: myScale, z: myScale);
        facingDirection = baseTransform.rotation.eulerAngles.y;
        targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
        smoke_3dmg.enableEmission = false;
        sparks.enableEmission = false;
        speedFXPS = speedFX1.GetComponent<ParticleSystem>();
        speedFXPS.enableEmission = false;
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER)
        {
            if (PhotonNetwork.isMasterClient)
            {
                var iD = photonView.owner.ID;
                if (FengGameManagerMKII.heroHash.ContainsKey(key: iD))
                    FengGameManagerMKII.heroHash[key: iD] = this;
                else
                    FengGameManagerMKII.heroHash.Add(key: iD, this);
            }

            var obj2 = GameObject.Find("UI_IN_GAME");
            myNetWorkName = (GameObject)Instantiate(Resources.Load("UI/LabelNameOverHead"));
            myNetWorkName.name = "LabelNameOverHead";
            myNetWorkName.transform.parent = obj2.GetComponent<UIReferArray>().panels[0].transform;
            myNetWorkName.transform.localScale = new Vector3(14f, 14f, 14f);
            myNetWorkName.GetComponent<UILabel>().text = string.Empty;
            if (photonView.isMine)
            {
                if (Minimap.instance != null)
                    Minimap.instance.TrackGameObjectOnMinimap(objToTrack: gameObject, iconColor: Color.green, false, true);
                GetComponent<SmoothSyncMovement>().PhotonCamera = true;
                photonView.RPC("SetMyPhotonCamera", target: PhotonTargets.OthersBuffered, PlayerPrefs.GetFloat("cameraDistance") + 0.3f);
            }
            else
            {
                var flag2 = false;
                if (photonView.owner.customProperties[key: PhotonPlayerProperty.RCteam] != null)
                    switch (RCextensions.returnIntFromObject(photonView.owner.customProperties[key: PhotonPlayerProperty.RCteam]))
                    {
                        case 1:
                            flag2 = true;
                            if (Minimap.instance != null)
                                Minimap.instance.TrackGameObjectOnMinimap(objToTrack: gameObject, iconColor: Color.cyan, false, true);
                            break;
                        case 2:
                            flag2 = true;
                            if (Minimap.instance != null)
                                Minimap.instance.TrackGameObjectOnMinimap(objToTrack: gameObject, iconColor: Color.magenta, false, true);
                            break;
                    }

                if (RCextensions.returnIntFromObject(photonView.owner.customProperties[key: PhotonPlayerProperty.team]) == 2)
                {
                    myNetWorkName.GetComponent<UILabel>().text = "[FF0000]AHSS\n[FFFFFF]";
                    if (!flag2 && Minimap.instance != null)
                        Minimap.instance.TrackGameObjectOnMinimap(objToTrack: gameObject, iconColor: Color.red, false, true);
                }
                else if (!flag2 && Minimap.instance != null)
                {
                    Minimap.instance.TrackGameObjectOnMinimap(objToTrack: gameObject, iconColor: Color.blue, false, true);
                }
            }

            var str = RCextensions.returnStringFromObject(photonView.owner.customProperties[key: PhotonPlayerProperty.guildName]);
            if (str != string.Empty)
            {
                var component = myNetWorkName.GetComponent<UILabel>();
                var text = component.text;
                string[] strArray2 = { text, "[FFFF00]", str, "\n[FFFFFF]", RCextensions.returnStringFromObject(photonView.owner.customProperties[key: PhotonPlayerProperty.name]) };
                component.text = string.Concat(values: strArray2);
            }
            else
            {
                var label2 = myNetWorkName.GetComponent<UILabel>();
                label2.text = label2.text + RCextensions.returnStringFromObject(photonView.owner.customProperties[key: PhotonPlayerProperty.name]);
            }
        }
        else if (Minimap.instance != null)
        {
            Minimap.instance.TrackGameObjectOnMinimap(objToTrack: gameObject, iconColor: Color.green, false, true);
        }

        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE && !photonView.isMine)
        {
            gameObject.layer = LayerMask.NameToLayer("NetworkObject");
            if (IN_GAME_MAIN_CAMERA.dayLight == DayLight.Night)
            {
                var obj3 = (GameObject)Instantiate(Resources.Load("flashlight"));
                obj3.transform.parent = baseTransform;
                obj3.transform.position = baseTransform.position + Vector3.up;
                obj3.transform.rotation = Quaternion.Euler(353f, 0f, 0f);
            }

            setup.init();
            setup.myCostume = new HeroCostume();
            setup.myCostume = CostumeConeveter.PhotonDataToHeroCostume2(player: photonView.owner);
            setup.setCharacterComponent();
            Destroy(obj: checkBoxLeft);
            Destroy(obj: checkBoxRight);
            Destroy(obj: leftbladetrail);
            Destroy(obj: rightbladetrail);
            Destroy(obj: leftbladetrail2);
            Destroy(obj: rightbladetrail2);
            hasspawn = true;
        }
        else
        {
            currentCamera = GameObject.Find("MainCamera").GetComponent<Camera>();
            inputManager = GameObject.Find("InputManagerController").GetComponent<FengCustomInputs>();
            loadskin();
            hasspawn = true;
            StartCoroutine(reloadSky());
        }

        bombImmune = false;
        if (RCSettings.bombMode == 1)
        {
            bombImmune = true;
            StartCoroutine(stopImmunity());
        }
    }

    public IEnumerator stopImmunity()
    {
        yield return new WaitForSeconds(5f);
        bombImmune = false;
    }

    private void suicide() { }

    private void suicide2()
    {
        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE)
        {
            netDieLocal(rigidbody.velocity * 50f, false, -1, titanName: string.Empty);
            FengGameManagerMKII.instance.needChooseSide = true;
            FengGameManagerMKII.instance.justSuicide = true;
        }
    }

    private void throwBlades()
    {
        var transform = setup.part_blade_l.transform;
        var transform2 = setup.part_blade_r.transform;
        var obj2 = (GameObject)Instantiate(Resources.Load("Character_parts/character_blade_l"), position: transform.position, rotation: transform.rotation);
        var obj3 = (GameObject)Instantiate(Resources.Load("Character_parts/character_blade_r"), position: transform2.position, rotation: transform2.rotation);
        obj2.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        obj3.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
        var force = this.transform.forward + this.transform.up * 2f - this.transform.right;
        obj2.rigidbody.AddForce(force: force, mode: ForceMode.Impulse);
        var vector2 = this.transform.forward + this.transform.up * 2f + this.transform.right;
        obj3.rigidbody.AddForce(force: vector2, mode: ForceMode.Impulse);
        var torque = new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100));
        torque.Normalize();
        obj2.rigidbody.AddTorque(torque: torque);
        torque = new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100));
        torque.Normalize();
        obj3.rigidbody.AddTorque(torque: torque);
        setup.part_blade_l.SetActive(false);
        setup.part_blade_r.SetActive(false);
        currentBladeNum--;
        if (currentBladeNum == 0)
            currentBladeSta = 0f;
        if (state == HERO_STATE.Attack)
            falseAttack();
    }

    public void ungrabbed()
    {
        facingDirection = 0f;
        targetRotation = Quaternion.Euler(0f, 0f, 0f);
        transform.parent = null;
        GetComponent<CapsuleCollider>().isTrigger = false;
        state = HERO_STATE.Idle;
    }

    private void unmounted()
    {
        myHorse.GetComponent<Horse>().unmounted();
        isMounted = false;
    }

    public void update()
    {
        if (!IN_GAME_MAIN_CAMERA.isPausing)
        {
            if (invincible > 0f)
                invincible -= Time.deltaTime;
            if (!hasDied)
            {
                if (titanForm && eren_titan != null)
                {
                    transform.position = eren_titan.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck").position;
                    gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
                }

                if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
                {
                    if (state == HERO_STATE.Grab && !useGun)
                    {
                        if (skillId == "jean")
                        {
                            if (state != HERO_STATE.Attack && (inputManager.isInputDown[InputCode.attack0] || inputManager.isInputDown[InputCode.attack1]) && escapeTimes > 0 && !animation.IsPlaying("grabbed_jean"))
                            {
                                playAnimation("grabbed_jean");
                                animation["grabbed_jean"].time = 0f;
                                escapeTimes--;
                            }

                            if (animation.IsPlaying("grabbed_jean") && animation["grabbed_jean"].normalizedTime > 0.64f && titanWhoGrabMe.GetComponent<TITAN>() != null)
                            {
                                ungrabbed();
                                rigidbody.velocity = Vector3.up * 30f;
                                if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                                {
                                    titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                                }
                                else
                                {
                                    photonView.RPC("netSetIsGrabbedFalse", target: PhotonTargets.All);
                                    if (PhotonNetwork.isMasterClient)
                                        titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                                    else
                                        PhotonView.Find(viewID: titanWhoGrabMeID).RPC("grabbedTargetEscape", target: PhotonTargets.MasterClient);
                                }
                            }
                        }
                        else if (skillId == "eren")
                        {
                            showSkillCD();
                            if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE || IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE && !IN_GAME_MAIN_CAMERA.isPausing)
                            {
                                calcSkillCD();
                                calcFlareCD();
                            }

                            if (inputManager.isInputDown[InputCode.attack1])
                            {
                                var flag = false;
                                if (skillCDDuration <= 0f && !flag)
                                {
                                    skillCDDuration = skillCDLast;
                                    if (skillId == "eren" && titanWhoGrabMe.GetComponent<TITAN>() != null)
                                    {
                                        ungrabbed();
                                        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                                        {
                                            titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                                        }
                                        else
                                        {
                                            photonView.RPC("netSetIsGrabbedFalse", target: PhotonTargets.All);
                                            if (PhotonNetwork.isMasterClient)
                                                titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                                            else
                                                PhotonView.Find(viewID: titanWhoGrabMeID).photonView.RPC("grabbedTargetEscape", target: PhotonTargets.MasterClient);
                                        }

                                        erenTransform();
                                    }
                                }
                                else
                                {
                                    flag = true;
                                }
                            }
                        }
                    }
                    else if (!titanForm)
                    {
                        bufferUpdate();
                        if (!grounded && state != HERO_STATE.AirDodge)
                        {
                            checkDashDoubleTap();
                            if (dashD)
                            {
                                dashD = false;
                                dash(0f, -1f);
                                return;
                            }

                            if (dashU)
                            {
                                dashU = false;
                                dash(0f, 1f);
                                return;
                            }

                            if (dashL)
                            {
                                dashL = false;
                                dash(-1f, 0f);
                                return;
                            }

                            if (dashR)
                            {
                                dashR = false;
                                dash(1f, 0f);
                                return;
                            }
                        }

                        if (grounded && (state == HERO_STATE.Idle || state == HERO_STATE.Slide))
                        {
                            if (inputManager.isInputDown[InputCode.jump] && !animation.IsPlaying("jump") && !animation.IsPlaying("horse_geton"))
                            {
                                idle();
                                crossFade("jump", 0.1f);
                                sparks.enableEmission = false;
                            }

                            if (inputManager.isInputDown[InputCode.dodge] && !animation.IsPlaying("jump") && !animation.IsPlaying("horse_geton"))
                            {
                                dodge2();
                                return;
                            }
                        }

                        if (state == HERO_STATE.Idle)
                        {
                            if (inputManager.isInputDown[InputCode.flare1])
                                shootFlare(1);
                            if (inputManager.isInputDown[InputCode.flare2])
                                shootFlare(2);
                            if (inputManager.isInputDown[InputCode.flare3])
                                shootFlare(3);
                            if (inputManager.isInputDown[InputCode.restart])
                                suicide();
                            if (myHorse != null && isMounted && inputManager.isInputDown[InputCode.dodge])
                                getOffHorse();
                            if ((animation.IsPlaying(name: standAnimation) || !grounded) && inputManager.isInputDown[InputCode.reload])
                            {
                                changeBlade();
                                return;
                            }

                            if (animation.IsPlaying(name: standAnimation) && inputManager.isInputDown[InputCode.salute])
                            {
                                salute();
                                return;
                            }

                            if (!isMounted && (inputManager.isInputDown[InputCode.attack0] || inputManager.isInputDown[InputCode.attack1]) && !useGun)
                            {
                                var flag2 = false;
                                if (inputManager.isInputDown[InputCode.attack1])
                                {
                                    if (skillCDDuration <= 0f && !flag2)
                                    {
                                        skillCDDuration = skillCDLast;
                                        if (skillId == "eren")
                                        {
                                            erenTransform();
                                            return;
                                        }

                                        if (skillId == "marco")
                                        {
                                            if (IsGrounded())
                                            {
                                                attackAnimation = Random.Range(0, 2) != 0 ? "special_marco_1" : "special_marco_0";
                                                playAnimation(aniName: attackAnimation);
                                            }
                                            else
                                            {
                                                flag2 = true;
                                                skillCDDuration = 0f;
                                            }
                                        }
                                        else if (skillId == "armin")
                                        {
                                            if (IsGrounded())
                                            {
                                                attackAnimation = "special_armin";
                                                playAnimation("special_armin");
                                            }
                                            else
                                            {
                                                flag2 = true;
                                                skillCDDuration = 0f;
                                            }
                                        }
                                        else if (skillId == "sasha")
                                        {
                                            if (IsGrounded())
                                            {
                                                attackAnimation = "special_sasha";
                                                playAnimation("special_sasha");
                                                currentBuff = BUFF.SpeedUp;
                                                buffTime = 10f;
                                            }
                                            else
                                            {
                                                flag2 = true;
                                                skillCDDuration = 0f;
                                            }
                                        }
                                        else if (skillId == "mikasa")
                                        {
                                            attackAnimation = "attack3_1";
                                            playAnimation("attack3_1");
                                            rigidbody.velocity = Vector3.up * 10f;
                                        }
                                        else if (skillId == "levi")
                                        {
                                            RaycastHit hit;
                                            attackAnimation = "attack5";
                                            playAnimation("attack5");
                                            var rigidbody = this.rigidbody;
                                            rigidbody.velocity += Vector3.up * 5f;
                                            var ray = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
                                            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
                                            LayerMask mask3 = mask2 | mask;
                                            if (Physics.Raycast(ray: ray, hitInfo: out hit, 1E+07f, layerMask: mask3.value))
                                            {
                                                if (bulletRight != null)
                                                {
                                                    bulletRight.GetComponent<Bullet>().disable();
                                                    releaseIfIHookSb();
                                                }

                                                dashDirection = hit.point - transform.position;
                                                launchRightRope(hit: hit, true, 1);
                                                rope.Play();
                                            }

                                            facingDirection = Mathf.Atan2(y: dashDirection.x, x: dashDirection.z) * 57.29578f;
                                            targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                                            attackLoop = 3;
                                        }
                                        else if (skillId == "petra")
                                        {
                                            RaycastHit hit2;
                                            attackAnimation = "special_petra";
                                            playAnimation("special_petra");
                                            var rigidbody2 = rigidbody;
                                            rigidbody2.velocity += Vector3.up * 5f;
                                            var ray2 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                            LayerMask mask4 = 1 << LayerMask.NameToLayer("Ground");
                                            LayerMask mask5 = 1 << LayerMask.NameToLayer("EnemyBox");
                                            LayerMask mask6 = mask5 | mask4;
                                            if (Physics.Raycast(ray: ray2, hitInfo: out hit2, 1E+07f, layerMask: mask6.value))
                                            {
                                                if (bulletRight != null)
                                                {
                                                    bulletRight.GetComponent<Bullet>().disable();
                                                    releaseIfIHookSb();
                                                }

                                                if (bulletLeft != null)
                                                {
                                                    bulletLeft.GetComponent<Bullet>().disable();
                                                    releaseIfIHookSb();
                                                }

                                                dashDirection = hit2.point - transform.position;
                                                launchLeftRope(hit: hit2, true);
                                                launchRightRope(hit: hit2, true);
                                                rope.Play();
                                            }

                                            facingDirection = Mathf.Atan2(y: dashDirection.x, x: dashDirection.z) * 57.29578f;
                                            targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                                            attackLoop = 3;
                                        }
                                        else
                                        {
                                            if (needLean)
                                            {
                                                if (leanLeft)
                                                    attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                                                else
                                                    attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                                            }
                                            else
                                            {
                                                attackAnimation = "attack1";
                                            }

                                            playAnimation(aniName: attackAnimation);
                                        }
                                    }
                                    else
                                    {
                                        flag2 = true;
                                    }
                                }
                                else if (inputManager.isInputDown[InputCode.attack0])
                                {
                                    if (needLean)
                                    {
                                        if (inputManager.isInput[InputCode.left])
                                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                                        else if (inputManager.isInput[InputCode.right])
                                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                                        else if (leanLeft)
                                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                                        else
                                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                                    }
                                    else if (inputManager.isInput[InputCode.left])
                                    {
                                        attackAnimation = "attack2";
                                    }
                                    else if (inputManager.isInput[InputCode.right])
                                    {
                                        attackAnimation = "attack1";
                                    }
                                    else if (lastHook != null)
                                    {
                                        if (lastHook.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck") != null)
                                            attackAccordingToTarget(lastHook.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck"));
                                        else
                                            flag2 = true;
                                    }
                                    else if (bulletLeft != null && bulletLeft.transform.parent != null)
                                    {
                                        var a = bulletLeft.transform.parent.transform.root.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                                        if (a != null)
                                            attackAccordingToTarget(a: a);
                                        else
                                            attackAccordingToMouse();
                                    }
                                    else if (bulletRight != null && bulletRight.transform.parent != null)
                                    {
                                        var transform2 = bulletRight.transform.parent.transform.root.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                                        if (transform2 != null)
                                            attackAccordingToTarget(a: transform2);
                                        else
                                            attackAccordingToMouse();
                                    }
                                    else
                                    {
                                        var obj2 = findNearestTitan();
                                        if (obj2 != null)
                                        {
                                            var transform3 = obj2.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                                            if (transform3 != null)
                                                attackAccordingToTarget(a: transform3);
                                            else
                                                attackAccordingToMouse();
                                        }
                                        else
                                        {
                                            attackAccordingToMouse();
                                        }
                                    }
                                }

                                if (!flag2)
                                {
                                    checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                                    checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                                    if (grounded)
                                        rigidbody.AddForce(gameObject.transform.forward * 200f);
                                    playAnimation(aniName: attackAnimation);
                                    animation[name: attackAnimation].time = 0f;
                                    buttonAttackRelease = false;
                                    state = HERO_STATE.Attack;
                                    if (!grounded && !(attackAnimation == "attack3_1") && !(attackAnimation == "attack5") && !(attackAnimation == "special_petra"))
                                    {
                                        attackReleased = false;
                                    }
                                    else
                                    {
                                        attackReleased = true;
                                        buttonAttackRelease = true;
                                    }

                                    sparks.enableEmission = false;
                                }
                            }

                            if (useGun)
                            {
                                if (inputManager.isInput[InputCode.attack1])
                                {
                                    leftArmAim = true;
                                    rightArmAim = true;
                                }
                                else if (inputManager.isInput[InputCode.attack0])
                                {
                                    if (leftGunHasBullet)
                                    {
                                        leftArmAim = true;
                                        rightArmAim = false;
                                    }
                                    else
                                    {
                                        leftArmAim = false;
                                        if (rightGunHasBullet)
                                            rightArmAim = true;
                                        else
                                            rightArmAim = false;
                                    }
                                }
                                else
                                {
                                    leftArmAim = false;
                                    rightArmAim = false;
                                }

                                if (leftArmAim || rightArmAim)
                                {
                                    RaycastHit hit3;
                                    var ray3 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                    LayerMask mask7 = 1 << LayerMask.NameToLayer("Ground");
                                    LayerMask mask8 = 1 << LayerMask.NameToLayer("EnemyBox");
                                    LayerMask mask9 = mask8 | mask7;
                                    if (Physics.Raycast(ray: ray3, hitInfo: out hit3, 1E+07f, layerMask: mask9.value))
                                        gunTarget = hit3.point;
                                }

                                var flag3 = false;
                                var flag4 = false;
                                var flag5 = false;
                                if (inputManager.isInputUp[InputCode.attack1])
                                {
                                    if (leftGunHasBullet && rightGunHasBullet)
                                    {
                                        if (grounded)
                                            attackAnimation = "AHSS_shoot_both";
                                        else
                                            attackAnimation = "AHSS_shoot_both_air";
                                        flag3 = true;
                                    }
                                    else if (!leftGunHasBullet && !rightGunHasBullet)
                                    {
                                        flag4 = true;
                                    }
                                    else
                                    {
                                        flag5 = true;
                                    }
                                }

                                if (flag5 || inputManager.isInputUp[InputCode.attack0])
                                {
                                    if (grounded)
                                    {
                                        if (leftGunHasBullet && rightGunHasBullet)
                                        {
                                            if (isLeftHandHooked)
                                                attackAnimation = "AHSS_shoot_r";
                                            else
                                                attackAnimation = "AHSS_shoot_l";
                                        }
                                        else if (leftGunHasBullet)
                                        {
                                            attackAnimation = "AHSS_shoot_l";
                                        }
                                        else if (rightGunHasBullet)
                                        {
                                            attackAnimation = "AHSS_shoot_r";
                                        }
                                    }
                                    else if (leftGunHasBullet && rightGunHasBullet)
                                    {
                                        if (isLeftHandHooked)
                                            attackAnimation = "AHSS_shoot_r_air";
                                        else
                                            attackAnimation = "AHSS_shoot_l_air";
                                    }
                                    else if (leftGunHasBullet)
                                    {
                                        attackAnimation = "AHSS_shoot_l_air";
                                    }
                                    else if (rightGunHasBullet)
                                    {
                                        attackAnimation = "AHSS_shoot_r_air";
                                    }

                                    if (!leftGunHasBullet && !rightGunHasBullet)
                                        flag4 = true;
                                    else
                                        flag3 = true;
                                }

                                if (flag3)
                                {
                                    state = HERO_STATE.Attack;
                                    crossFade(aniName: attackAnimation, 0.05f);
                                    gunDummy.transform.position = transform.position;
                                    gunDummy.transform.rotation = transform.rotation;
                                    gunDummy.transform.LookAt(worldPosition: gunTarget);
                                    attackReleased = false;
                                    facingDirection = gunDummy.transform.rotation.eulerAngles.y;
                                    targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                                }
                                else if (flag4 && (grounded || LevelInfo.getInfo(name: FengGameManagerMKII.level).type != GAMEMODE.PVP_AHSS))
                                {
                                    changeBlade();
                                }
                            }
                        }
                        else if (state == HERO_STATE.Attack)
                        {
                            if (!useGun)
                            {
                                if (!inputManager.isInput[InputCode.attack0])
                                    buttonAttackRelease = true;
                                if (!attackReleased)
                                {
                                    if (buttonAttackRelease)
                                    {
                                        continueAnimation();
                                        attackReleased = true;
                                    }
                                    else if (animation[name: attackAnimation].normalizedTime >= 0.32f)
                                    {
                                        pauseAnimation();
                                    }
                                }

                                if (attackAnimation == "attack3_1" && currentBladeSta > 0f)
                                {
                                    if (animation[name: attackAnimation].normalizedTime >= 0.8f)
                                    {
                                        if (!checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                                        {
                                            checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = true;
                                            leftbladetrail2.Activate();
                                            rightbladetrail2.Activate();
                                            if (QualitySettings.GetQualityLevel() >= 2)
                                            {
                                                leftbladetrail.Activate();
                                                rightbladetrail.Activate();
                                            }

                                            rigidbody.velocity = -Vector3.up * 30f;
                                        }

                                        if (!checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me)
                                        {
                                            checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = true;
                                            slash.Play();
                                        }
                                    }
                                    else if (checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                                    {
                                        checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                                        checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                                        checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                                        checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                                        leftbladetrail.StopSmoothly(0.1f);
                                        rightbladetrail.StopSmoothly(0.1f);
                                        leftbladetrail2.StopSmoothly(0.1f);
                                        rightbladetrail2.StopSmoothly(0.1f);
                                    }
                                }
                                else
                                {
                                    float num;
                                    float num2;
                                    if (currentBladeSta == 0f)
                                    {
                                        num = -1f;
                                        num2 = -1f;
                                    }
                                    else if (attackAnimation == "attack5")
                                    {
                                        num2 = 0.35f;
                                        num = 0.5f;
                                    }
                                    else if (attackAnimation == "special_petra")
                                    {
                                        num2 = 0.35f;
                                        num = 0.48f;
                                    }
                                    else if (attackAnimation == "special_armin")
                                    {
                                        num2 = 0.25f;
                                        num = 0.35f;
                                    }
                                    else if (attackAnimation == "attack4")
                                    {
                                        num2 = 0.6f;
                                        num = 0.9f;
                                    }
                                    else if (attackAnimation == "special_sasha")
                                    {
                                        num = -1f;
                                        num2 = -1f;
                                    }
                                    else
                                    {
                                        num2 = 0.5f;
                                        num = 0.85f;
                                    }

                                    if (animation[name: attackAnimation].normalizedTime > num2 && animation[name: attackAnimation].normalizedTime < num)
                                    {
                                        if (!checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                                        {
                                            checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = true;
                                            slash.Play();
                                            leftbladetrail2.Activate();
                                            rightbladetrail2.Activate();
                                            if (QualitySettings.GetQualityLevel() >= 2)
                                            {
                                                leftbladetrail.Activate();
                                                rightbladetrail.Activate();
                                            }
                                        }

                                        if (!checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me)
                                            checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = true;
                                    }
                                    else if (checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                                    {
                                        checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                                        checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                                        checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                                        checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                                        leftbladetrail2.StopSmoothly(0.1f);
                                        rightbladetrail2.StopSmoothly(0.1f);
                                        if (QualitySettings.GetQualityLevel() >= 2)
                                        {
                                            leftbladetrail.StopSmoothly(0.1f);
                                            rightbladetrail.StopSmoothly(0.1f);
                                        }
                                    }

                                    if (attackLoop > 0 && animation[name: attackAnimation].normalizedTime > num)
                                    {
                                        attackLoop--;
                                        playAnimationAt(aniName: attackAnimation, normalizedTime: num2);
                                    }
                                }

                                if (animation[name: attackAnimation].normalizedTime >= 1f)
                                {
                                    if (attackAnimation != "special_marco_0" && attackAnimation != "special_marco_1")
                                    {
                                        if (attackAnimation == "special_armin")
                                        {
                                            if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE)
                                            {
                                                if (!PhotonNetwork.isMasterClient)
                                                    photonView.RPC("netlaughAttack", target: PhotonTargets.MasterClient);
                                                else
                                                    netlaughAttack();
                                            }
                                            else
                                            {
                                                foreach (var obj3 in GameObject.FindGameObjectsWithTag("titan"))
                                                {
                                                    if (Vector3.Distance(a: obj3.transform.position, b: transform.position) < 50f && Vector3.Angle(from: obj3.transform.forward, transform.position - obj3.transform.position) < 90f && obj3.GetComponent<TITAN>() != null)
                                                        obj3.GetComponent<TITAN>().beLaughAttacked();
                                                }
                                            }

                                            falseAttack();
                                            idle();
                                        }
                                        else if (attackAnimation == "attack3_1")
                                        {
                                            var rigidbody3 = rigidbody;
                                            rigidbody3.velocity -= Vector3.up * Time.deltaTime * 30f;
                                        }
                                        else
                                        {
                                            falseAttack();
                                            idle();
                                        }
                                    }
                                    else
                                    {
                                        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE)
                                        {
                                            if (!PhotonNetwork.isMasterClient)
                                            {
                                                object[] parameters = { 5f, 100f };
                                                photonView.RPC("netTauntAttack", target: PhotonTargets.MasterClient, parameters: parameters);
                                            }
                                            else
                                            {
                                                netTauntAttack(5f);
                                            }
                                        }
                                        else
                                        {
                                            netTauntAttack(5f);
                                        }

                                        falseAttack();
                                        idle();
                                    }
                                }

                                if (animation.IsPlaying("attack3_2") && animation["attack3_2"].normalizedTime >= 1f)
                                {
                                    falseAttack();
                                    idle();
                                }
                            }
                            else
                            {
                                transform.rotation = Quaternion.Lerp(from: transform.rotation, to: gunDummy.transform.rotation, Time.deltaTime * 30f);
                                print(message: attackAnimation);
                                if (!attackReleased && animation[name: attackAnimation].normalizedTime > 0.167f)
                                {
                                    GameObject obj4;
                                    attackReleased = true;
                                    var flag6 = false;
                                    if (attackAnimation != "AHSS_shoot_both" && attackAnimation != "AHSS_shoot_both_air")
                                    {
                                        if (!(attackAnimation == "AHSS_shoot_l") && !(attackAnimation == "AHSS_shoot_l_air"))
                                            rightGunHasBullet = false;
                                        else
                                            leftGunHasBullet = false;
                                        rigidbody.AddForce(-transform.forward * 600f, mode: ForceMode.Acceleration);
                                    }
                                    else
                                    {
                                        flag6 = true;
                                        leftGunHasBullet = false;
                                        rightGunHasBullet = false;
                                        rigidbody.AddForce(-transform.forward * 1000f, mode: ForceMode.Acceleration);
                                    }

                                    rigidbody.AddForce(Vector3.up * 200f, mode: ForceMode.Acceleration);
                                    var prefabName = "FX/shotGun";
                                    if (flag6)
                                        prefabName = "FX/shotGun 1";
                                    if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER && photonView.isMine)
                                    {
                                        obj4 = PhotonNetwork.Instantiate(prefabName: prefabName, transform.position + transform.up * 0.8f - transform.right * 0.1f, rotation: transform.rotation, 0);
                                        if (obj4.GetComponent<EnemyfxIDcontainer>() != null)
                                            obj4.GetComponent<EnemyfxIDcontainer>().myOwnerViewID = photonView.viewID;
                                    }
                                    else
                                    {
                                        obj4 = (GameObject)Instantiate(Resources.Load(path: prefabName), transform.position + transform.up * 0.8f - transform.right * 0.1f, rotation: transform.rotation);
                                    }
                                }

                                if (animation[name: attackAnimation].normalizedTime >= 1f)
                                {
                                    falseAttack();
                                    idle();
                                }

                                if (!animation.IsPlaying(name: attackAnimation))
                                {
                                    falseAttack();
                                    idle();
                                }
                            }
                        }
                        else if (state == HERO_STATE.ChangeBlade)
                        {
                            if (useGun)
                            {
                                if (animation[name: reloadAnimation].normalizedTime > 0.22f)
                                {
                                    if (!leftGunHasBullet && setup.part_blade_l.activeSelf)
                                    {
                                        setup.part_blade_l.SetActive(false);
                                        var transform = setup.part_blade_l.transform;
                                        var obj5 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_l"), position: transform.position, rotation: transform.rotation);
                                        obj5.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
                                        var force = -this.transform.forward * 10f + this.transform.up * 5f - this.transform.right;
                                        obj5.rigidbody.AddForce(force: force, mode: ForceMode.Impulse);
                                        var torque = new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100));
                                        obj5.rigidbody.AddTorque(torque: torque, mode: ForceMode.Acceleration);
                                    }

                                    if (!rightGunHasBullet && setup.part_blade_r.activeSelf)
                                    {
                                        setup.part_blade_r.SetActive(false);
                                        var transform5 = setup.part_blade_r.transform;
                                        var obj6 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_r"), position: transform5.position, rotation: transform5.rotation);
                                        obj6.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
                                        var vector4 = -transform.forward * 10f + transform.up * 5f + transform.right;
                                        obj6.rigidbody.AddForce(force: vector4, mode: ForceMode.Impulse);
                                        var vector5 = new Vector3(Random.Range(-300, 300), Random.Range(-300, 300), Random.Range(-300, 300));
                                        obj6.rigidbody.AddTorque(torque: vector5, mode: ForceMode.Acceleration);
                                    }
                                }

                                if (animation[name: reloadAnimation].normalizedTime > 0.62f && !throwedBlades)
                                {
                                    throwedBlades = true;
                                    if (leftBulletLeft > 0 && !leftGunHasBullet)
                                    {
                                        leftBulletLeft--;
                                        setup.part_blade_l.SetActive(true);
                                        leftGunHasBullet = true;
                                    }

                                    if (rightBulletLeft > 0 && !rightGunHasBullet)
                                    {
                                        setup.part_blade_r.SetActive(true);
                                        rightBulletLeft--;
                                        rightGunHasBullet = true;
                                    }

                                    updateRightMagUI();
                                    updateLeftMagUI();
                                }

                                if (animation[name: reloadAnimation].normalizedTime > 1f)
                                    idle();
                            }
                            else
                            {
                                if (!grounded)
                                {
                                    if (animation[name: reloadAnimation].normalizedTime >= 0.2f && !throwedBlades)
                                    {
                                        throwedBlades = true;
                                        if (setup.part_blade_l.activeSelf)
                                            throwBlades();
                                    }

                                    if (animation[name: reloadAnimation].normalizedTime >= 0.56f && currentBladeNum > 0)
                                    {
                                        setup.part_blade_l.SetActive(true);
                                        setup.part_blade_r.SetActive(true);
                                        currentBladeSta = totalBladeSta;
                                    }
                                }
                                else
                                {
                                    if (animation[name: reloadAnimation].normalizedTime >= 0.13f && !throwedBlades)
                                    {
                                        throwedBlades = true;
                                        if (setup.part_blade_l.activeSelf)
                                            throwBlades();
                                    }

                                    if (animation[name: reloadAnimation].normalizedTime >= 0.37f && currentBladeNum > 0)
                                    {
                                        setup.part_blade_l.SetActive(true);
                                        setup.part_blade_r.SetActive(true);
                                        currentBladeSta = totalBladeSta;
                                    }
                                }

                                if (animation[name: reloadAnimation].normalizedTime >= 1f)
                                    idle();
                            }
                        }
                        else if (state == HERO_STATE.Salute)
                        {
                            if (animation["salute"].normalizedTime >= 1f)
                                idle();
                        }
                        else if (state == HERO_STATE.GroundDodge)
                        {
                            if (animation.IsPlaying("dodge"))
                            {
                                if (!grounded && animation["dodge"].normalizedTime > 0.6f)
                                    idle();
                                if (animation["dodge"].normalizedTime >= 1f)
                                    idle();
                            }
                        }
                        else if (state == HERO_STATE.Land)
                        {
                            if (animation.IsPlaying("dash_land") && animation["dash_land"].normalizedTime >= 1f)
                                idle();
                        }
                        else if (state == HERO_STATE.FillGas)
                        {
                            if (animation.IsPlaying("supply") && animation["supply"].normalizedTime >= 1f)
                            {
                                currentBladeSta = totalBladeSta;
                                currentBladeNum = totalBladeNum;
                                currentGas = totalGas;
                                if (!useGun)
                                {
                                    setup.part_blade_l.SetActive(true);
                                    setup.part_blade_r.SetActive(true);
                                }
                                else
                                {
                                    leftBulletLeft = rightBulletLeft = bulletMAX;
                                    rightGunHasBullet = true;
                                    leftGunHasBullet = true;
                                    setup.part_blade_l.SetActive(true);
                                    setup.part_blade_r.SetActive(true);
                                    updateRightMagUI();
                                    updateLeftMagUI();
                                }

                                idle();
                            }
                        }
                        else if (state == HERO_STATE.Slide)
                        {
                            if (!grounded)
                                idle();
                        }
                        else if (state == HERO_STATE.AirDodge)
                        {
                            if (dashTime > 0f)
                            {
                                dashTime -= Time.deltaTime;
                                if (currentSpeed > originVM)
                                    rigidbody.AddForce(-rigidbody.velocity * Time.deltaTime * 1.7f, mode: ForceMode.VelocityChange);
                            }
                            else
                            {
                                dashTime = 0f;
                                idle();
                            }
                        }

                        if (inputManager.isInput[InputCode.leftRope] && !animation.IsPlaying("attack3_1") && !animation.IsPlaying("attack5") && !animation.IsPlaying("special_petra") && state != HERO_STATE.ChangeBlade && state != HERO_STATE.Grab)
                        {
                            if (bulletLeft != null)
                            {
                                QHold = true;
                            }
                            else
                            {
                                RaycastHit hit4;
                                var ray4 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                LayerMask mask10 = 1 << LayerMask.NameToLayer("Ground");
                                LayerMask mask11 = 1 << LayerMask.NameToLayer("EnemyBox");
                                LayerMask mask12 = mask11 | mask10;
                                if (Physics.Raycast(ray: ray4, hitInfo: out hit4, 10000f, layerMask: mask12.value))
                                {
                                    launchLeftRope(hit: hit4, true);
                                    rope.Play();
                                }
                            }
                        }
                        else
                        {
                            QHold = false;
                        }

                        if (inputManager.isInput[InputCode.rightRope] && (!animation.IsPlaying("attack3_1") && !animation.IsPlaying("attack5") && !animation.IsPlaying("special_petra") || state == HERO_STATE.Idle))
                        {
                            if (bulletRight != null)
                            {
                                EHold = true;
                            }
                            else
                            {
                                RaycastHit hit5;
                                var ray5 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                LayerMask mask13 = 1 << LayerMask.NameToLayer("Ground");
                                LayerMask mask14 = 1 << LayerMask.NameToLayer("EnemyBox");
                                LayerMask mask15 = mask14 | mask13;
                                if (Physics.Raycast(ray: ray5, hitInfo: out hit5, 10000f, layerMask: mask15.value))
                                {
                                    launchRightRope(hit: hit5, true);
                                    rope.Play();
                                }
                            }
                        }
                        else
                        {
                            EHold = false;
                        }

                        if (inputManager.isInput[InputCode.bothRope] && (!animation.IsPlaying("attack3_1") && !animation.IsPlaying("attack5") && !animation.IsPlaying("special_petra") || state == HERO_STATE.Idle))
                        {
                            QHold = true;
                            EHold = true;
                            if (bulletLeft == null && bulletRight == null)
                            {
                                RaycastHit hit6;
                                var ray6 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                LayerMask mask16 = 1 << LayerMask.NameToLayer("Ground");
                                LayerMask mask17 = 1 << LayerMask.NameToLayer("EnemyBox");
                                LayerMask mask18 = mask17 | mask16;
                                if (Physics.Raycast(ray: ray6, hitInfo: out hit6, 1000000f, layerMask: mask18.value))
                                {
                                    launchLeftRope(hit: hit6, false);
                                    launchRightRope(hit: hit6, false);
                                    rope.Play();
                                }
                            }
                        }

                        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE || IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE && !IN_GAME_MAIN_CAMERA.isPausing)
                        {
                            calcSkillCD();
                            calcFlareCD();
                        }

                        if (!useGun)
                        {
                            if (leftbladetrail.gameObject.GetActive())
                            {
                                leftbladetrail.update();
                                rightbladetrail.update();
                            }

                            if (leftbladetrail2.gameObject.GetActive())
                            {
                                leftbladetrail2.update();
                                rightbladetrail2.update();
                            }

                            if (leftbladetrail.gameObject.GetActive())
                            {
                                leftbladetrail.lateUpdate();
                                rightbladetrail.lateUpdate();
                            }

                            if (leftbladetrail2.gameObject.GetActive())
                            {
                                leftbladetrail2.lateUpdate();
                                rightbladetrail2.lateUpdate();
                            }
                        }

                        if (!IN_GAME_MAIN_CAMERA.isPausing)
                        {
                            showSkillCD();
                            showFlareCD();
                            showGas();
                            showAimUI();
                        }
                    }
                }
            }
        }
    }

    public void update2()
    {
        if (!IN_GAME_MAIN_CAMERA.isPausing)
        {
            if (invincible > 0f)
                invincible -= Time.deltaTime;
            if (!hasDied)
            {
                if (titanForm && eren_titan != null)
                {
                    baseTransform.position = eren_titan.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck").position;
                    gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
                }
                else if (isCannon && myCannon != null)
                {
                    updateCannon();
                    gameObject.GetComponent<SmoothSyncMovement>().disabled = true;
                }

                if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
                {
                    if (myCannonRegion != null)
                    {
                        FengGameManagerMKII.instance.ShowHUDInfoCenter("Press 'Cannon Mount' key to use Cannon.");
                        if (FengGameManagerMKII.inputRC.isInputCannonDown(code: InputCodeRC.cannonMount))
                            myCannonRegion.photonView.RPC("RequestControlRPC", target: PhotonTargets.MasterClient, photonView.viewID);
                    }

                    if (state == HERO_STATE.Grab && !useGun)
                    {
                        if (skillId == "jean")
                        {
                            if (state != HERO_STATE.Attack && (inputManager.isInputDown[InputCode.attack0] || inputManager.isInputDown[InputCode.attack1]) && escapeTimes > 0 && !baseAnimation.IsPlaying("grabbed_jean"))
                            {
                                playAnimation("grabbed_jean");
                                baseAnimation["grabbed_jean"].time = 0f;
                                escapeTimes--;
                            }

                            if (baseAnimation.IsPlaying("grabbed_jean") && baseAnimation["grabbed_jean"].normalizedTime > 0.64f && titanWhoGrabMe.GetComponent<TITAN>() != null)
                            {
                                ungrabbed();
                                baseRigidBody.velocity = Vector3.up * 30f;
                                if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                                {
                                    titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                                }
                                else
                                {
                                    photonView.RPC("netSetIsGrabbedFalse", target: PhotonTargets.All);
                                    if (PhotonNetwork.isMasterClient)
                                        titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                                    else
                                        PhotonView.Find(viewID: titanWhoGrabMeID).RPC("grabbedTargetEscape", target: PhotonTargets.MasterClient);
                                }
                            }
                        }
                        else if (skillId == "eren")
                        {
                            showSkillCD();
                            if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE || IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE && !IN_GAME_MAIN_CAMERA.isPausing)
                            {
                                calcSkillCD();
                                calcFlareCD();
                            }

                            if (inputManager.isInputDown[InputCode.attack1])
                            {
                                var flag2 = false;
                                if (skillCDDuration > 0f || flag2)
                                {
                                    flag2 = true;
                                }
                                else
                                {
                                    skillCDDuration = skillCDLast;
                                    if (skillId == "eren" && titanWhoGrabMe.GetComponent<TITAN>() != null)
                                    {
                                        ungrabbed();
                                        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
                                        {
                                            titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                                        }
                                        else
                                        {
                                            photonView.RPC("netSetIsGrabbedFalse", target: PhotonTargets.All);
                                            if (PhotonNetwork.isMasterClient)
                                                titanWhoGrabMe.GetComponent<TITAN>().grabbedTargetEscape();
                                            else
                                                PhotonView.Find(viewID: titanWhoGrabMeID).photonView.RPC("grabbedTargetEscape", target: PhotonTargets.MasterClient);
                                        }

                                        erenTransform();
                                    }
                                }
                            }
                        }
                    }
                    else if (!titanForm && !isCannon)
                    {
                        bool ReflectorVariable2;
                        bool ReflectorVariable1;
                        bool ReflectorVariable0;
                        bufferUpdate();
                        updateExt();
                        if (!grounded && state != HERO_STATE.AirDodge)
                        {
                            if ((int)FengGameManagerMKII.settings[0xb5] == 1)
                                checkDashRebind();
                            else
                                checkDashDoubleTap();
                            if (dashD)
                            {
                                dashD = false;
                                dash(0f, -1f);
                                return;
                            }

                            if (dashU)
                            {
                                dashU = false;
                                dash(0f, 1f);
                                return;
                            }

                            if (dashL)
                            {
                                dashL = false;
                                dash(-1f, 0f);
                                return;
                            }

                            if (dashR)
                            {
                                dashR = false;
                                dash(1f, 0f);
                                return;
                            }
                        }

                        if (grounded && (state == HERO_STATE.Idle || state == HERO_STATE.Slide))
                        {
                            if (!(!inputManager.isInputDown[InputCode.jump] || baseAnimation.IsPlaying("jump") || baseAnimation.IsPlaying("horse_geton")))
                            {
                                idle();
                                crossFade("jump", 0.1f);
                                sparks.enableEmission = false;
                            }

                            if (!(!FengGameManagerMKII.inputRC.isInputHorseDown(code: InputCodeRC.horseMount) || baseAnimation.IsPlaying("jump") || baseAnimation.IsPlaying("horse_geton")) && myHorse != null && !isMounted && Vector3.Distance(a: myHorse.transform.position, b: transform.position) < 15f)
                                getOnHorse();
                            if (!(!inputManager.isInputDown[InputCode.dodge] || baseAnimation.IsPlaying("jump") || baseAnimation.IsPlaying("horse_geton")))
                            {
                                dodge2();
                                return;
                            }
                        }

                        if (state == HERO_STATE.Idle)
                        {
                            if (inputManager.isInputDown[InputCode.flare1])
                                shootFlare(1);
                            if (inputManager.isInputDown[InputCode.flare2])
                                shootFlare(2);
                            if (inputManager.isInputDown[InputCode.flare3])
                                shootFlare(3);
                            if (inputManager.isInputDown[InputCode.restart])
                                suicide2();
                            if (myHorse != null && isMounted && FengGameManagerMKII.inputRC.isInputHorseDown(code: InputCodeRC.horseMount))
                                getOffHorse();
                            if ((animation.IsPlaying(name: standAnimation) || !grounded) && inputManager.isInputDown[InputCode.reload] && (!useGun || RCSettings.ahssReload != 1 || grounded))
                            {
                                changeBlade();
                                return;
                            }

                            if (baseAnimation.IsPlaying(name: standAnimation) && inputManager.isInputDown[InputCode.salute])
                            {
                                salute();
                                return;
                            }

                            if (!isMounted && (inputManager.isInputDown[InputCode.attack0] || inputManager.isInputDown[InputCode.attack1]) && !useGun)
                            {
                                var flag3 = false;
                                if (inputManager.isInputDown[InputCode.attack1])
                                {
                                    if (skillCDDuration > 0f || flag3)
                                    {
                                        flag3 = true;
                                    }
                                    else
                                    {
                                        skillCDDuration = skillCDLast;
                                        if (skillId == "eren")
                                        {
                                            erenTransform();
                                            return;
                                        }

                                        if (skillId == "marco")
                                        {
                                            if (IsGrounded())
                                            {
                                                attackAnimation = Random.Range(0, 2) != 0 ? "special_marco_1" : "special_marco_0";
                                                playAnimation(aniName: attackAnimation);
                                            }
                                            else
                                            {
                                                flag3 = true;
                                                skillCDDuration = 0f;
                                            }
                                        }
                                        else if (skillId == "armin")
                                        {
                                            if (IsGrounded())
                                            {
                                                attackAnimation = "special_armin";
                                                playAnimation("special_armin");
                                            }
                                            else
                                            {
                                                flag3 = true;
                                                skillCDDuration = 0f;
                                            }
                                        }
                                        else if (skillId == "sasha")
                                        {
                                            if (IsGrounded())
                                            {
                                                attackAnimation = "special_sasha";
                                                playAnimation("special_sasha");
                                                currentBuff = BUFF.SpeedUp;
                                                buffTime = 10f;
                                            }
                                            else
                                            {
                                                flag3 = true;
                                                skillCDDuration = 0f;
                                            }
                                        }
                                        else if (skillId == "mikasa")
                                        {
                                            attackAnimation = "attack3_1";
                                            playAnimation("attack3_1");
                                            baseRigidBody.velocity = Vector3.up * 10f;
                                        }
                                        else if (skillId == "levi")
                                        {
                                            RaycastHit hit;
                                            attackAnimation = "attack5";
                                            playAnimation("attack5");
                                            baseRigidBody.velocity += Vector3.up * 5f;
                                            var ray = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                            LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
                                            LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
                                            LayerMask mask3 = mask2 | mask;
                                            if (Physics.Raycast(ray: ray, hitInfo: out hit, 1E+07f, layerMask: mask3.value))
                                            {
                                                if (bulletRight != null)
                                                {
                                                    bulletRight.GetComponent<Bullet>().disable();
                                                    releaseIfIHookSb();
                                                }

                                                dashDirection = hit.point - baseTransform.position;
                                                launchRightRope(hit: hit, true, 1);
                                                rope.Play();
                                            }

                                            facingDirection = Mathf.Atan2(y: dashDirection.x, x: dashDirection.z) * 57.29578f;
                                            targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                                            attackLoop = 3;
                                        }
                                        else if (skillId == "petra")
                                        {
                                            RaycastHit hit2;
                                            attackAnimation = "special_petra";
                                            playAnimation("special_petra");
                                            baseRigidBody.velocity += Vector3.up * 5f;
                                            var ray2 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                            LayerMask mask4 = 1 << LayerMask.NameToLayer("Ground");
                                            LayerMask mask5 = 1 << LayerMask.NameToLayer("EnemyBox");
                                            LayerMask mask6 = mask5 | mask4;
                                            if (Physics.Raycast(ray: ray2, hitInfo: out hit2, 1E+07f, layerMask: mask6.value))
                                            {
                                                if (bulletRight != null)
                                                {
                                                    bulletRight.GetComponent<Bullet>().disable();
                                                    releaseIfIHookSb();
                                                }

                                                if (bulletLeft != null)
                                                {
                                                    bulletLeft.GetComponent<Bullet>().disable();
                                                    releaseIfIHookSb();
                                                }

                                                dashDirection = hit2.point - baseTransform.position;
                                                launchLeftRope(hit: hit2, true);
                                                launchRightRope(hit: hit2, true);
                                                rope.Play();
                                            }

                                            facingDirection = Mathf.Atan2(y: dashDirection.x, x: dashDirection.z) * 57.29578f;
                                            targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                                            attackLoop = 3;
                                        }
                                        else
                                        {
                                            if (needLean)
                                            {
                                                if (leanLeft)
                                                    attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                                                else
                                                    attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                                            }
                                            else
                                            {
                                                attackAnimation = "attack1";
                                            }

                                            playAnimation(aniName: attackAnimation);
                                        }
                                    }
                                }
                                else if (inputManager.isInputDown[InputCode.attack0])
                                {
                                    if (needLean)
                                    {
                                        if (inputManager.isInput[InputCode.left])
                                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                                        else if (inputManager.isInput[InputCode.right])
                                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                                        else if (leanLeft)
                                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_l1" : "attack1_hook_l2";
                                        else
                                            attackAnimation = Random.Range(0, 100) >= 50 ? "attack1_hook_r1" : "attack1_hook_r2";
                                    }
                                    else if (inputManager.isInput[InputCode.left])
                                    {
                                        attackAnimation = "attack2";
                                    }
                                    else if (inputManager.isInput[InputCode.right])
                                    {
                                        attackAnimation = "attack1";
                                    }
                                    else if (lastHook != null)
                                    {
                                        if (lastHook.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck") != null)
                                            attackAccordingToTarget(lastHook.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck"));
                                        else
                                            flag3 = true;
                                    }
                                    else if (bulletLeft != null && bulletLeft.transform.parent != null)
                                    {
                                        var a = bulletLeft.transform.parent.transform.root.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                                        if (a != null)
                                            attackAccordingToTarget(a: a);
                                        else
                                            attackAccordingToMouse();
                                    }
                                    else if (bulletRight != null && bulletRight.transform.parent != null)
                                    {
                                        var transform2 = bulletRight.transform.parent.transform.root.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                                        if (transform2 != null)
                                            attackAccordingToTarget(a: transform2);
                                        else
                                            attackAccordingToMouse();
                                    }
                                    else
                                    {
                                        var obj2 = findNearestTitan();
                                        if (obj2 != null)
                                        {
                                            var transform3 = obj2.transform.Find("Amarture/Core/Controller_Body/hip/spine/chest/neck");
                                            if (transform3 != null)
                                                attackAccordingToTarget(a: transform3);
                                            else
                                                attackAccordingToMouse();
                                        }
                                        else
                                        {
                                            attackAccordingToMouse();
                                        }
                                    }
                                }

                                if (!flag3)
                                {
                                    checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                                    checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                                    if (grounded)
                                        baseRigidBody.AddForce(gameObject.transform.forward * 200f);
                                    playAnimation(aniName: attackAnimation);
                                    baseAnimation[name: attackAnimation].time = 0f;
                                    buttonAttackRelease = false;
                                    state = HERO_STATE.Attack;
                                    if (grounded || attackAnimation == "attack3_1" || attackAnimation == "attack5" || attackAnimation == "special_petra")
                                    {
                                        attackReleased = true;
                                        buttonAttackRelease = true;
                                    }
                                    else
                                    {
                                        attackReleased = false;
                                    }

                                    sparks.enableEmission = false;
                                }
                            }

                            if (useGun)
                            {
                                if (inputManager.isInput[InputCode.attack1])
                                {
                                    leftArmAim = true;
                                    rightArmAim = true;
                                }
                                else if (inputManager.isInput[InputCode.attack0])
                                {
                                    if (leftGunHasBullet)
                                    {
                                        leftArmAim = true;
                                        rightArmAim = false;
                                    }
                                    else
                                    {
                                        leftArmAim = false;
                                        if (rightGunHasBullet)
                                            rightArmAim = true;
                                        else
                                            rightArmAim = false;
                                    }
                                }
                                else
                                {
                                    leftArmAim = false;
                                    rightArmAim = false;
                                }

                                if (leftArmAim || rightArmAim)
                                {
                                    RaycastHit hit3;
                                    var ray3 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                    LayerMask mask7 = 1 << LayerMask.NameToLayer("Ground");
                                    LayerMask mask8 = 1 << LayerMask.NameToLayer("EnemyBox");
                                    LayerMask mask9 = mask8 | mask7;
                                    if (Physics.Raycast(ray: ray3, hitInfo: out hit3, 1E+07f, layerMask: mask9.value))
                                        gunTarget = hit3.point;
                                }

                                var flag4 = false;
                                var flag5 = false;
                                var flag6 = false;
                                if (inputManager.isInputUp[InputCode.attack1] && skillId != "bomb")
                                {
                                    if (leftGunHasBullet && rightGunHasBullet)
                                    {
                                        if (grounded)
                                            attackAnimation = "AHSS_shoot_both";
                                        else
                                            attackAnimation = "AHSS_shoot_both_air";
                                        flag4 = true;
                                    }
                                    else if (!(leftGunHasBullet || rightGunHasBullet))
                                    {
                                        flag5 = true;
                                    }
                                    else
                                    {
                                        flag6 = true;
                                    }
                                }

                                if (flag6 || inputManager.isInputUp[InputCode.attack0])
                                {
                                    if (grounded)
                                    {
                                        if (leftGunHasBullet && rightGunHasBullet)
                                        {
                                            if (isLeftHandHooked)
                                                attackAnimation = "AHSS_shoot_r";
                                            else
                                                attackAnimation = "AHSS_shoot_l";
                                        }
                                        else if (leftGunHasBullet)
                                        {
                                            attackAnimation = "AHSS_shoot_l";
                                        }
                                        else if (rightGunHasBullet)
                                        {
                                            attackAnimation = "AHSS_shoot_r";
                                        }
                                    }
                                    else if (leftGunHasBullet && rightGunHasBullet)
                                    {
                                        if (isLeftHandHooked)
                                            attackAnimation = "AHSS_shoot_r_air";
                                        else
                                            attackAnimation = "AHSS_shoot_l_air";
                                    }
                                    else if (leftGunHasBullet)
                                    {
                                        attackAnimation = "AHSS_shoot_l_air";
                                    }
                                    else if (rightGunHasBullet)
                                    {
                                        attackAnimation = "AHSS_shoot_r_air";
                                    }

                                    if (leftGunHasBullet || rightGunHasBullet)
                                        flag4 = true;
                                    else
                                        flag5 = true;
                                }

                                if (flag4)
                                {
                                    state = HERO_STATE.Attack;
                                    crossFade(aniName: attackAnimation, 0.05f);
                                    gunDummy.transform.position = baseTransform.position;
                                    gunDummy.transform.rotation = baseTransform.rotation;
                                    gunDummy.transform.LookAt(worldPosition: gunTarget);
                                    attackReleased = false;
                                    facingDirection = gunDummy.transform.rotation.eulerAngles.y;
                                    targetRotation = Quaternion.Euler(0f, y: facingDirection, 0f);
                                }
                                else if (flag5 && (grounded || LevelInfo.getInfo(name: FengGameManagerMKII.level).type != GAMEMODE.PVP_AHSS && RCSettings.ahssReload == 0))
                                {
                                    changeBlade();
                                }
                            }
                        }
                        else if (state == HERO_STATE.Attack)
                        {
                            if (!useGun)
                            {
                                if (!inputManager.isInput[InputCode.attack0])
                                    buttonAttackRelease = true;
                                if (!attackReleased)
                                {
                                    if (buttonAttackRelease)
                                    {
                                        continueAnimation();
                                        attackReleased = true;
                                    }
                                    else if (baseAnimation[name: attackAnimation].normalizedTime >= 0.32f)
                                    {
                                        pauseAnimation();
                                    }
                                }

                                if (attackAnimation == "attack3_1" && currentBladeSta > 0f)
                                {
                                    if (baseAnimation[name: attackAnimation].normalizedTime >= 0.8f)
                                    {
                                        if (!checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                                        {
                                            checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = true;
                                            if ((int)FengGameManagerMKII.settings[0x5c] == 0)
                                            {
                                                leftbladetrail2.Activate();
                                                rightbladetrail2.Activate();
                                                leftbladetrail.Activate();
                                                rightbladetrail.Activate();
                                            }

                                            baseRigidBody.velocity = -Vector3.up * 30f;
                                        }

                                        if (!checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me)
                                        {
                                            checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = true;
                                            slash.Play();
                                        }
                                    }
                                    else if (checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                                    {
                                        checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                                        checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                                        checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                                        checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                                        leftbladetrail.StopSmoothly(0.1f);
                                        rightbladetrail.StopSmoothly(0.1f);
                                        leftbladetrail2.StopSmoothly(0.1f);
                                        rightbladetrail2.StopSmoothly(0.1f);
                                    }
                                }
                                else
                                {
                                    float num;
                                    float num2;
                                    if (currentBladeSta == 0f)
                                    {
                                        num = -1f;
                                        num2 = -1f;
                                    }
                                    else if (attackAnimation == "attack5")
                                    {
                                        num2 = 0.35f;
                                        num = 0.5f;
                                    }
                                    else if (attackAnimation == "special_petra")
                                    {
                                        num2 = 0.35f;
                                        num = 0.48f;
                                    }
                                    else if (attackAnimation == "special_armin")
                                    {
                                        num2 = 0.25f;
                                        num = 0.35f;
                                    }
                                    else if (attackAnimation == "attack4")
                                    {
                                        num2 = 0.6f;
                                        num = 0.9f;
                                    }
                                    else if (attackAnimation == "special_sasha")
                                    {
                                        num = -1f;
                                        num2 = -1f;
                                    }
                                    else
                                    {
                                        num2 = 0.5f;
                                        num = 0.85f;
                                    }

                                    if (baseAnimation[name: attackAnimation].normalizedTime > num2 && baseAnimation[name: attackAnimation].normalizedTime < num)
                                    {
                                        if (!checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                                        {
                                            checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = true;
                                            slash.Play();
                                            if ((int)FengGameManagerMKII.settings[0x5c] == 0)
                                            {
                                                leftbladetrail2.Activate();
                                                rightbladetrail2.Activate();
                                                leftbladetrail.Activate();
                                                rightbladetrail.Activate();
                                            }
                                        }

                                        if (!checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me)
                                            checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = true;
                                    }
                                    else if (checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me)
                                    {
                                        checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                                        checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                                        checkBoxLeft.GetComponent<TriggerColliderWeapon>().clearHits();
                                        checkBoxRight.GetComponent<TriggerColliderWeapon>().clearHits();
                                        leftbladetrail2.StopSmoothly(0.1f);
                                        rightbladetrail2.StopSmoothly(0.1f);
                                        leftbladetrail.StopSmoothly(0.1f);
                                        rightbladetrail.StopSmoothly(0.1f);
                                    }

                                    if (attackLoop > 0 && baseAnimation[name: attackAnimation].normalizedTime > num)
                                    {
                                        attackLoop--;
                                        playAnimationAt(aniName: attackAnimation, normalizedTime: num2);
                                    }
                                }

                                if (baseAnimation[name: attackAnimation].normalizedTime >= 1f)
                                {
                                    if (attackAnimation == "special_marco_0" || attackAnimation == "special_marco_1")
                                    {
                                        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE)
                                        {
                                            if (!PhotonNetwork.isMasterClient)
                                            {
                                                object[] parameters = { 5f, 100f };
                                                photonView.RPC("netTauntAttack", target: PhotonTargets.MasterClient, parameters: parameters);
                                            }
                                            else
                                            {
                                                netTauntAttack(5f);
                                            }
                                        }
                                        else
                                        {
                                            netTauntAttack(5f);
                                        }

                                        falseAttack();
                                        idle();
                                    }
                                    else if (attackAnimation == "special_armin")
                                    {
                                        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE)
                                        {
                                            if (!PhotonNetwork.isMasterClient)
                                                photonView.RPC("netlaughAttack", target: PhotonTargets.MasterClient);
                                            else
                                                netlaughAttack();
                                        }
                                        else
                                        {
                                            foreach (var obj3 in GameObject.FindGameObjectsWithTag("titan"))
                                            {
                                                if (Vector3.Distance(a: obj3.transform.position, b: baseTransform.position) < 50f && Vector3.Angle(from: obj3.transform.forward, baseTransform.position - obj3.transform.position) < 90f && obj3.GetComponent<TITAN>() != null)
                                                    obj3.GetComponent<TITAN>().beLaughAttacked();
                                            }
                                        }

                                        falseAttack();
                                        idle();
                                    }
                                    else if (attackAnimation == "attack3_1")
                                    {
                                        baseRigidBody.velocity -= Vector3.up * Time.deltaTime * 30f;
                                    }
                                    else
                                    {
                                        falseAttack();
                                        idle();
                                    }
                                }

                                if (baseAnimation.IsPlaying("attack3_2") && baseAnimation["attack3_2"].normalizedTime >= 1f)
                                {
                                    falseAttack();
                                    idle();
                                }
                            }
                            else
                            {
                                baseTransform.rotation = Quaternion.Lerp(from: baseTransform.rotation, to: gunDummy.transform.rotation, Time.deltaTime * 30f);
                                if (!attackReleased && baseAnimation[name: attackAnimation].normalizedTime > 0.167f)
                                {
                                    GameObject obj4;
                                    attackReleased = true;
                                    var flag7 = false;
                                    if (attackAnimation == "AHSS_shoot_both" || attackAnimation == "AHSS_shoot_both_air")
                                    {
                                        flag7 = true;
                                        leftGunHasBullet = false;
                                        rightGunHasBullet = false;
                                        baseRigidBody.AddForce(-baseTransform.forward * 1000f, mode: ForceMode.Acceleration);
                                    }
                                    else
                                    {
                                        if (attackAnimation == "AHSS_shoot_l" || attackAnimation == "AHSS_shoot_l_air")
                                            leftGunHasBullet = false;
                                        else
                                            rightGunHasBullet = false;
                                        baseRigidBody.AddForce(-baseTransform.forward * 600f, mode: ForceMode.Acceleration);
                                    }

                                    baseRigidBody.AddForce(Vector3.up * 200f, mode: ForceMode.Acceleration);
                                    var prefabName = "FX/shotGun";
                                    if (flag7)
                                        prefabName = "FX/shotGun 1";
                                    if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.MULTIPLAYER && photonView.isMine)
                                    {
                                        obj4 = PhotonNetwork.Instantiate(prefabName: prefabName, baseTransform.position + baseTransform.up * 0.8f - baseTransform.right * 0.1f, rotation: baseTransform.rotation, 0);
                                        if (obj4.GetComponent<EnemyfxIDcontainer>() != null)
                                            obj4.GetComponent<EnemyfxIDcontainer>().myOwnerViewID = photonView.viewID;
                                    }
                                    else
                                    {
                                        obj4 = (GameObject)Instantiate(Resources.Load(path: prefabName), baseTransform.position + baseTransform.up * 0.8f - baseTransform.right * 0.1f, rotation: baseTransform.rotation);
                                    }
                                }

                                if (baseAnimation[name: attackAnimation].normalizedTime >= 1f)
                                {
                                    falseAttack();
                                    idle();
                                }

                                if (!baseAnimation.IsPlaying(name: attackAnimation))
                                {
                                    falseAttack();
                                    idle();
                                }
                            }
                        }
                        else if (state == HERO_STATE.ChangeBlade)
                        {
                            if (useGun)
                            {
                                if (baseAnimation[name: reloadAnimation].normalizedTime > 0.22f)
                                {
                                    if (!(leftGunHasBullet || !setup.part_blade_l.activeSelf))
                                    {
                                        setup.part_blade_l.SetActive(false);
                                        var transform = setup.part_blade_l.transform;
                                        var obj5 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_l"), position: transform.position, rotation: transform.rotation);
                                        obj5.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
                                        var force = -baseTransform.forward * 10f + baseTransform.up * 5f - baseTransform.right;
                                        obj5.rigidbody.AddForce(force: force, mode: ForceMode.Impulse);
                                        var torque = new Vector3(Random.Range(-100, 100), Random.Range(-100, 100), Random.Range(-100, 100));
                                        obj5.rigidbody.AddTorque(torque: torque, mode: ForceMode.Acceleration);
                                    }

                                    if (!(rightGunHasBullet || !setup.part_blade_r.activeSelf))
                                    {
                                        setup.part_blade_r.SetActive(false);
                                        var transform5 = setup.part_blade_r.transform;
                                        var obj6 = (GameObject)Instantiate(Resources.Load("Character_parts/character_gun_r"), position: transform5.position, rotation: transform5.rotation);
                                        obj6.renderer.material = CharacterMaterials.materials[key: setup.myCostume._3dmg_texture];
                                        var vector3 = -baseTransform.forward * 10f + baseTransform.up * 5f + baseTransform.right;
                                        obj6.rigidbody.AddForce(force: vector3, mode: ForceMode.Impulse);
                                        var vector4 = new Vector3(Random.Range(-300, 300), Random.Range(-300, 300), Random.Range(-300, 300));
                                        obj6.rigidbody.AddTorque(torque: vector4, mode: ForceMode.Acceleration);
                                    }
                                }

                                if (baseAnimation[name: reloadAnimation].normalizedTime > 0.62f && !throwedBlades)
                                {
                                    throwedBlades = true;
                                    if (!(leftBulletLeft <= 0 || leftGunHasBullet))
                                    {
                                        leftBulletLeft--;
                                        setup.part_blade_l.SetActive(true);
                                        leftGunHasBullet = true;
                                    }

                                    if (!(rightBulletLeft <= 0 || rightGunHasBullet))
                                    {
                                        setup.part_blade_r.SetActive(true);
                                        rightBulletLeft--;
                                        rightGunHasBullet = true;
                                    }

                                    updateRightMagUI();
                                    updateLeftMagUI();
                                }

                                if (baseAnimation[name: reloadAnimation].normalizedTime > 1f)
                                    idle();
                            }
                            else
                            {
                                if (!grounded)
                                {
                                    if (!(animation[name: reloadAnimation].normalizedTime < 0.2f || throwedBlades))
                                    {
                                        throwedBlades = true;
                                        if (setup.part_blade_l.activeSelf)
                                            throwBlades();
                                    }

                                    if (animation[name: reloadAnimation].normalizedTime >= 0.56f && currentBladeNum > 0)
                                    {
                                        setup.part_blade_l.SetActive(true);
                                        setup.part_blade_r.SetActive(true);
                                        currentBladeSta = totalBladeSta;
                                    }
                                }
                                else
                                {
                                    if (!(baseAnimation[name: reloadAnimation].normalizedTime < 0.13f || throwedBlades))
                                    {
                                        throwedBlades = true;
                                        if (setup.part_blade_l.activeSelf)
                                            throwBlades();
                                    }

                                    if (baseAnimation[name: reloadAnimation].normalizedTime >= 0.37f && currentBladeNum > 0)
                                    {
                                        setup.part_blade_l.SetActive(true);
                                        setup.part_blade_r.SetActive(true);
                                        currentBladeSta = totalBladeSta;
                                    }
                                }

                                if (baseAnimation[name: reloadAnimation].normalizedTime >= 1f)
                                    idle();
                            }
                        }
                        else if (state == HERO_STATE.Salute)
                        {
                            if (baseAnimation["salute"].normalizedTime >= 1f)
                                idle();
                        }
                        else if (state == HERO_STATE.GroundDodge)
                        {
                            if (baseAnimation.IsPlaying("dodge"))
                            {
                                if (!(grounded || baseAnimation["dodge"].normalizedTime <= 0.6f))
                                    idle();
                                if (baseAnimation["dodge"].normalizedTime >= 1f)
                                    idle();
                            }
                        }
                        else if (state == HERO_STATE.Land)
                        {
                            if (baseAnimation.IsPlaying("dash_land") && baseAnimation["dash_land"].normalizedTime >= 1f)
                                idle();
                        }
                        else if (state == HERO_STATE.FillGas)
                        {
                            if (baseAnimation.IsPlaying("supply") && baseAnimation["supply"].normalizedTime >= 1f)
                            {
                                currentBladeSta = totalBladeSta;
                                currentBladeNum = totalBladeNum;
                                currentGas = totalGas;
                                if (!useGun)
                                {
                                    setup.part_blade_l.SetActive(true);
                                    setup.part_blade_r.SetActive(true);
                                }
                                else
                                {
                                    leftBulletLeft = rightBulletLeft = bulletMAX;
                                    rightGunHasBullet = true;
                                    leftGunHasBullet = true;
                                    setup.part_blade_l.SetActive(true);
                                    setup.part_blade_r.SetActive(true);
                                    updateRightMagUI();
                                    updateLeftMagUI();
                                }

                                idle();
                            }
                        }
                        else if (state == HERO_STATE.Slide)
                        {
                            if (!grounded)
                                idle();
                        }
                        else if (state == HERO_STATE.AirDodge)
                        {
                            if (dashTime > 0f)
                            {
                                dashTime -= Time.deltaTime;
                                if (currentSpeed > originVM)
                                    baseRigidBody.AddForce(-baseRigidBody.velocity * Time.deltaTime * 1.7f, mode: ForceMode.VelocityChange);
                            }
                            else
                            {
                                dashTime = 0f;
                                idle();
                            }
                        }

                        if (inputManager.isInput[InputCode.leftRope])
                            ReflectorVariable0 = true;
                        else
                            ReflectorVariable0 = false;
                        if (!(ReflectorVariable0 ? baseAnimation.IsPlaying("attack3_1") || baseAnimation.IsPlaying("attack5") || baseAnimation.IsPlaying("special_petra") || state == HERO_STATE.Grab ? state != HERO_STATE.Idle : false : true))
                        {
                            if (bulletLeft != null)
                            {
                                QHold = true;
                            }
                            else
                            {
                                RaycastHit hit4;
                                var ray4 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                LayerMask mask10 = 1 << LayerMask.NameToLayer("Ground");
                                LayerMask mask11 = 1 << LayerMask.NameToLayer("EnemyBox");
                                LayerMask mask12 = mask11 | mask10;
                                if (Physics.Raycast(ray: ray4, hitInfo: out hit4, 10000f, layerMask: mask12.value))
                                {
                                    launchLeftRope(hit: hit4, true);
                                    rope.Play();
                                }
                            }
                        }
                        else
                        {
                            QHold = false;
                        }

                        if (inputManager.isInput[InputCode.rightRope])
                            ReflectorVariable1 = true;
                        else
                            ReflectorVariable1 = false;
                        if (!(ReflectorVariable1 ? baseAnimation.IsPlaying("attack3_1") || baseAnimation.IsPlaying("attack5") || baseAnimation.IsPlaying("special_petra") || state == HERO_STATE.Grab ? state != HERO_STATE.Idle : false : true))
                        {
                            if (bulletRight != null)
                            {
                                EHold = true;
                            }
                            else
                            {
                                RaycastHit hit5;
                                var ray5 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                LayerMask mask13 = 1 << LayerMask.NameToLayer("Ground");
                                LayerMask mask14 = 1 << LayerMask.NameToLayer("EnemyBox");
                                LayerMask mask15 = mask14 | mask13;
                                if (Physics.Raycast(ray: ray5, hitInfo: out hit5, 10000f, layerMask: mask15.value))
                                {
                                    launchRightRope(hit: hit5, true);
                                    rope.Play();
                                }
                            }
                        }
                        else
                        {
                            EHold = false;
                        }

                        if (inputManager.isInput[InputCode.bothRope])
                            ReflectorVariable2 = true;
                        else
                            ReflectorVariable2 = false;
                        if (!(ReflectorVariable2 ? baseAnimation.IsPlaying("attack3_1") || baseAnimation.IsPlaying("attack5") || baseAnimation.IsPlaying("special_petra") || state == HERO_STATE.Grab ? state != HERO_STATE.Idle : false : true))
                        {
                            QHold = true;
                            EHold = true;
                            if (bulletLeft == null && bulletRight == null)
                            {
                                RaycastHit hit6;
                                var ray6 = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                                LayerMask mask16 = 1 << LayerMask.NameToLayer("Ground");
                                LayerMask mask17 = 1 << LayerMask.NameToLayer("EnemyBox");
                                LayerMask mask18 = mask17 | mask16;
                                if (Physics.Raycast(ray: ray6, hitInfo: out hit6, 1000000f, layerMask: mask18.value))
                                {
                                    launchLeftRope(hit: hit6, false);
                                    launchRightRope(hit: hit6, false);
                                    rope.Play();
                                }
                            }
                        }

                        if (IN_GAME_MAIN_CAMERA.gametype != GAMETYPE.SINGLE || IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE && !IN_GAME_MAIN_CAMERA.isPausing)
                        {
                            calcSkillCD();
                            calcFlareCD();
                        }

                        if (!useGun)
                        {
                            if (leftbladetrail.gameObject.GetActive())
                            {
                                leftbladetrail.update();
                                rightbladetrail.update();
                            }

                            if (leftbladetrail2.gameObject.GetActive())
                            {
                                leftbladetrail2.update();
                                rightbladetrail2.update();
                            }

                            if (leftbladetrail.gameObject.GetActive())
                            {
                                leftbladetrail.lateUpdate();
                                rightbladetrail.lateUpdate();
                            }

                            if (leftbladetrail2.gameObject.GetActive())
                            {
                                leftbladetrail2.lateUpdate();
                                rightbladetrail2.lateUpdate();
                            }
                        }

                        if (!IN_GAME_MAIN_CAMERA.isPausing)
                        {
                            showSkillCD();
                            showFlareCD2();
                            showGas2();
                            showAimUI2();
                        }
                    }
                    else if (isCannon && !IN_GAME_MAIN_CAMERA.isPausing)
                    {
                        showAimUI2();
                        calcSkillCD();
                        showSkillCD();
                    }
                }
            }
        }
    }

    public void updateCannon()
    {
        baseTransform.position = myCannonPlayer.position;
        baseTransform.rotation = myCannonBase.rotation;
    }

    public void updateExt()
    {
        if (skillId == "bomb")
        {
            if (inputManager.isInputDown[InputCode.attack1] && skillCDDuration <= 0f)
            {
                if (!(myBomb == null || myBomb.disabled))
                    myBomb.Explode(radius: bombRadius);
                detonate = false;
                skillCDDuration = bombCD;
                var hitInfo = new RaycastHit();
                var ray = Camera.main.ScreenPointToRay(position: Input.mousePosition);
                LayerMask mask = 1 << LayerMask.NameToLayer("Ground");
                LayerMask mask2 = 1 << LayerMask.NameToLayer("EnemyBox");
                LayerMask mask3 = mask2 | mask;
                currentV = baseTransform.position;
                targetV = currentV + Vector3.forward * 200f;
                if (Physics.Raycast(ray: ray, hitInfo: out hitInfo, 1000000f, layerMask: mask3.value))
                    targetV = hitInfo.point;
                var vector = Vector3.Normalize(targetV - currentV);
                var obj2 = PhotonNetwork.Instantiate("RCAsset/BombMain", currentV + vector * 4f, new Quaternion(0f, 0f, 0f, 1f), 0);
                obj2.rigidbody.velocity = vector * bombSpeed;
                myBomb = obj2.GetComponent<Bomb>();
                bombTime = 0f;
            }
            else if (myBomb != null && !myBomb.disabled)
            {
                bombTime += Time.deltaTime;
                var flag2 = false;
                if (inputManager.isInputUp[InputCode.attack1])
                {
                    detonate = true;
                }
                else if (inputManager.isInputDown[InputCode.attack1] && detonate)
                {
                    detonate = false;
                    flag2 = true;
                }

                if (bombTime >= bombTimeMax)
                    flag2 = true;
                if (flag2)
                {
                    myBomb.Explode(radius: bombRadius);
                    detonate = false;
                }
            }
        }
    }

    private void updateLeftMagUI()
    {
        for (var i = 1; i <= bulletMAX; i++)
            GameObject.Find("bulletL" + i).GetComponent<UISprite>().enabled = false;
        for (var j = 1; j <= leftBulletLeft; j++)
            GameObject.Find("bulletL" + j).GetComponent<UISprite>().enabled = true;
    }

    private void updateRightMagUI()
    {
        for (var i = 1; i <= bulletMAX; i++)
            GameObject.Find("bulletR" + i).GetComponent<UISprite>().enabled = false;
        for (var j = 1; j <= rightBulletLeft; j++)
            GameObject.Find("bulletR" + j).GetComponent<UISprite>().enabled = true;
    }

    public void useBlade(int amount = 0)
    {
        if (amount == 0)
            amount = 1;
        amount *= 2;
        if (currentBladeSta > 0f)
        {
            currentBladeSta -= amount;
            if (currentBladeSta <= 0f)
            {
                if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE || photonView.isMine)
                {
                    leftbladetrail.Deactivate();
                    rightbladetrail.Deactivate();
                    leftbladetrail2.Deactivate();
                    rightbladetrail2.Deactivate();
                    checkBoxLeft.GetComponent<TriggerColliderWeapon>().active_me = false;
                    checkBoxRight.GetComponent<TriggerColliderWeapon>().active_me = false;
                }

                currentBladeSta = 0f;
                throwBlades();
            }
        }
    }

    private void useGas(float amount = 0)
    {
        if (amount == 0f)
            amount = useGasSpeed;
        if (currentGas > 0f)
        {
            currentGas -= amount;
            if (currentGas < 0f)
                currentGas = 0f;
        }
    }

    [RPC]
    private void whoIsMyErenTitan(int id)
    {
        eren_titan = PhotonView.Find(viewID: id).gameObject;
        titanForm = true;
    }
}