using System;
using System.Collections.Generic;
using ExitGames.Client.Photon;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;

public class InRoomChat : MonoBehaviour
{
    public static readonly string ChatRPC = "Chat";
    public static Rect GuiRect = new Rect(0f, 100f, 300f, 470f);
    public static Rect GuiRect2 = new Rect(30f, 575f, 300f, 25f);
    public static List<string> messages = new List<string>();
    private readonly bool AlignBottom = true;
    private string inputLine = string.Empty;
    public bool IsVisible = true;
    private Vector2 scrollPos = Vector2.zero;

    public void addLINE(string newLine)
    {
        messages.Add(item: newLine);
    }

    public void AddLine(string newLine)
    {
        messages.Add(item: newLine);
    }

    public void OnGUI()
    {
        int num4;
        if (!IsVisible || PhotonNetwork.connectionStatesDetailed != PeerStates.Joined)
            return;
        if (Event.current.type == EventType.KeyDown)
        {
            if (Event.current.keyCode != KeyCode.Tab && Event.current.character != '\t' || IN_GAME_MAIN_CAMERA.isPausing || FengGameManagerMKII.inputRC.humanKeys[InputCodeRC.chat] == KeyCode.Tab)
                goto Label_00E1;
            Event.current.Use();
            goto Label_013D;
        }

        if (Event.current.type == EventType.KeyUp && Event.current.keyCode != KeyCode.None && Event.current.keyCode == FengGameManagerMKII.inputRC.humanKeys[InputCodeRC.chat] && GUI.GetNameOfFocusedControl() != "ChatInput")
        {
            inputLine = string.Empty;
            GUI.FocusControl("ChatInput");
            goto Label_013D;
        }

        Label_00E1:
        if (Event.current.type == EventType.KeyDown && (Event.current.keyCode == KeyCode.KeypadEnter || Event.current.keyCode == KeyCode.Return))
        {
            if (!string.IsNullOrEmpty(value: inputLine))
            {
                string str2;
                if (inputLine == "\t")
                {
                    inputLine = string.Empty;
                    GUI.FocusControl(name: string.Empty);
                    return;
                }

                if (FengGameManagerMKII.RCEvents.ContainsKey("OnChatInput"))
                {
                    var key = (string)FengGameManagerMKII.RCVariableNames["OnChatInput"];
                    if (FengGameManagerMKII.stringVariables.ContainsKey(key: key))
                        FengGameManagerMKII.stringVariables[key: key] = inputLine;
                    else
                        FengGameManagerMKII.stringVariables.Add(key: key, value: inputLine);
                    ((RCEvent)FengGameManagerMKII.RCEvents["OnChatInput"]).checkEvent();
                }

                if (!inputLine.StartsWith("/"))
                {
                    str2 = RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]).hexColor();
                    if (str2 == string.Empty)
                    {
                        str2 = RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]);
                        if (PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.RCteam] != null)
                        {
                            if (RCextensions.returnIntFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.RCteam]) == 1)
                                str2 = "<color=#00FFFF>" + str2 + "</color>";
                            else if (RCextensions.returnIntFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.RCteam]) == 2)
                                str2 = "<color=#FF00FF>" + str2 + "</color>";
                        }
                    }

                    object[] parameters = { inputLine, str2 };
                    FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: parameters);
                }
                else if (inputLine == "/cloth")
                {
                    addLINE(ClothFactory.GetDebugInfo());
                }
                else if (inputLine.StartsWith("/aso"))
                {
                    if (PhotonNetwork.isMasterClient)
                        switch (inputLine.Substring(5))
                        {
                            case "kdr":
                                if (RCSettings.asoPreservekdr == 0)
                                {
                                    RCSettings.asoPreservekdr = 1;
                                    addLINE("<color=#FFCC00>KDRs will be preserved from disconnects.</color>");
                                }
                                else
                                {
                                    RCSettings.asoPreservekdr = 0;
                                    addLINE("<color=#FFCC00>KDRs will not be preserved from disconnects.</color>");
                                }

                                break;
                            case "racing":
                                if (RCSettings.racingStatic == 0)
                                {
                                    RCSettings.racingStatic = 1;
                                    addLINE("<color=#FFCC00>Racing will not end on finish.</color>");
                                }
                                else
                                {
                                    RCSettings.racingStatic = 0;
                                    addLINE("<color=#FFCC00>Racing will end on finish.</color>");
                                }

                                break;
                        }
                }
                else
                {
                    object[] objArray3;
                    if (inputLine == "/pause")
                    {
                        if (PhotonNetwork.isMasterClient)
                        {
                            FengGameManagerMKII.instance.photonView.RPC("pauseRPC", target: PhotonTargets.All, true);
                            objArray3 = new object[] { "<color=#FFCC00>MasterClient has paused the game.</color>", "" };
                            FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray3);
                        }
                        else
                        {
                            addLINE("<color=#FFCC00>error: not master client</color>");
                        }
                    }
                    else if (inputLine == "/unpause")
                    {
                        if (PhotonNetwork.isMasterClient)
                        {
                            FengGameManagerMKII.instance.photonView.RPC("pauseRPC", target: PhotonTargets.All, false);
                            objArray3 = new object[] { "<color=#FFCC00>MasterClient has unpaused the game.</color>", "" };
                            FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray3);
                        }
                        else
                        {
                            addLINE("<color=#FFCC00>error: not master client</color>");
                        }
                    }
                    else if (inputLine == "/checklevel")
                    {
                        foreach (var player in PhotonNetwork.playerList)
                            addLINE(RCextensions.returnStringFromObject(player.customProperties[key: PhotonPlayerProperty.currentLevel]));
                    }
                    else if (inputLine == "/isrc")
                    {
                        if (FengGameManagerMKII.masterRC)
                            addLINE("is RC");
                        else
                            addLINE("not RC");
                    }
                    else if (inputLine == "/ignorelist")
                    {
                        foreach (var num2 in FengGameManagerMKII.ignoreList)
                            addLINE(num2.ToString());
                    }
                    else if (inputLine.StartsWith("/room"))
                    {
                        if (PhotonNetwork.isMasterClient)
                        {
                            if (inputLine.Substring(6).StartsWith("max"))
                            {
                                var num3 = Convert.ToInt32(inputLine.Substring(10));
                                FengGameManagerMKII.instance.maxPlayers = num3;
                                PhotonNetwork.room.maxPlayers = num3;
                                objArray3 = new object[] { "<color=#FFCC00>Max players changed to " + inputLine.Substring(10) + "!</color>", "" };
                                FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray3);
                            }
                            else if (inputLine.Substring(6).StartsWith("time"))
                            {
                                FengGameManagerMKII.instance.addTime(Convert.ToSingle(inputLine.Substring(11)));
                                objArray3 = new object[] { "<color=#FFCC00>" + inputLine.Substring(11) + " seconds added to the clock.</color>", "" };
                                FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray3);
                            }
                        }
                        else
                        {
                            addLINE("<color=#FFCC00>error: not master client</color>");
                        }
                    }
                    else if (inputLine.StartsWith("/resetkd"))
                    {
                        Hashtable hashtable;
                        if (inputLine == "/resetkdall")
                        {
                            if (PhotonNetwork.isMasterClient)
                            {
                                foreach (var player in PhotonNetwork.playerList)
                                {
                                    hashtable = new Hashtable();
                                    hashtable.Add(key: PhotonPlayerProperty.kills, 0);
                                    hashtable.Add(key: PhotonPlayerProperty.deaths, 0);
                                    hashtable.Add(key: PhotonPlayerProperty.max_dmg, 0);
                                    hashtable.Add(key: PhotonPlayerProperty.total_dmg, 0);
                                    player.SetCustomProperties(propertiesToSet: hashtable);
                                }

                                objArray3 = new object[] { "<color=#FFCC00>All stats have been reset.</color>", "" };
                                FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray3);
                            }
                            else
                            {
                                addLINE("<color=#FFCC00>error: not master client</color>");
                            }
                        }
                        else
                        {
                            hashtable = new Hashtable();
                            hashtable.Add(key: PhotonPlayerProperty.kills, 0);
                            hashtable.Add(key: PhotonPlayerProperty.deaths, 0);
                            hashtable.Add(key: PhotonPlayerProperty.max_dmg, 0);
                            hashtable.Add(key: PhotonPlayerProperty.total_dmg, 0);
                            PhotonNetwork.player.SetCustomProperties(propertiesToSet: hashtable);
                            addLINE("<color=#FFCC00>Your stats have been reset. </color>");
                        }
                    }
                    else if (inputLine.StartsWith("/pm"))
                    {
                        var strArray = inputLine.Split(' ');
                        var targetPlayer = PhotonPlayer.Find(Convert.ToInt32(strArray[1]));
                        str2 = RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]).hexColor();
                        if (str2 == string.Empty)
                        {
                            str2 = RCextensions.returnStringFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.name]);
                            if (PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.RCteam] != null)
                            {
                                if (RCextensions.returnIntFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.RCteam]) == 1)
                                    str2 = "<color=#00FFFF>" + str2 + "</color>";
                                else if (RCextensions.returnIntFromObject(PhotonNetwork.player.customProperties[key: PhotonPlayerProperty.RCteam]) == 2)
                                    str2 = "<color=#FF00FF>" + str2 + "</color>";
                            }
                        }

                        var str3 = RCextensions.returnStringFromObject(targetPlayer.customProperties[key: PhotonPlayerProperty.name]).hexColor();
                        if (str3 == string.Empty)
                        {
                            str3 = RCextensions.returnStringFromObject(targetPlayer.customProperties[key: PhotonPlayerProperty.name]);
                            if (targetPlayer.customProperties[key: PhotonPlayerProperty.RCteam] != null)
                            {
                                if (RCextensions.returnIntFromObject(targetPlayer.customProperties[key: PhotonPlayerProperty.RCteam]) == 1)
                                    str3 = "<color=#00FFFF>" + str3 + "</color>";
                                else if (RCextensions.returnIntFromObject(targetPlayer.customProperties[key: PhotonPlayerProperty.RCteam]) == 2)
                                    str3 = "<color=#FF00FF>" + str3 + "</color>";
                            }
                        }

                        var str4 = string.Empty;
                        for (num4 = 2; num4 < strArray.Length; num4++)
                            str4 = str4 + strArray[num4] + " ";
                        FengGameManagerMKII.instance.photonView.RPC("ChatPM", targetPlayer: targetPlayer, str2, str4);
                        addLINE("<color=#FFC000>TO [" + targetPlayer.ID + "]</color> " + str3 + ":" + str4);
                    }
                    else if (inputLine.StartsWith("/team"))
                    {
                        if (RCSettings.teamMode == 1)
                        {
                            if (inputLine.Substring(6) == "1" || inputLine.Substring(6) == "cyan")
                            {
                                FengGameManagerMKII.instance.photonView.RPC("setTeamRPC", targetPlayer: PhotonNetwork.player, 1);
                                addLINE("<color=#00FFFF>You have joined team cyan.</color>");
                                foreach (var obj2 in GameObject.FindGameObjectsWithTag("Player"))
                                {
                                    if (obj2.GetPhotonView().isMine)
                                    {
                                        obj2.GetComponent<HERO>().markDie();
                                        obj2.GetComponent<HERO>().photonView.RPC("netDie2", target: PhotonTargets.All, -1, "Team Switch");
                                    }
                                }
                            }
                            else if (inputLine.Substring(6) == "2" || inputLine.Substring(6) == "magenta")
                            {
                                FengGameManagerMKII.instance.photonView.RPC("setTeamRPC", targetPlayer: PhotonNetwork.player, 2);
                                addLINE("<color=#FF00FF>You have joined team magenta.</color>");
                                foreach (var obj3 in GameObject.FindGameObjectsWithTag("Player"))
                                {
                                    if (obj3.GetPhotonView().isMine)
                                    {
                                        obj3.GetComponent<HERO>().markDie();
                                        obj3.GetComponent<HERO>().photonView.RPC("netDie2", target: PhotonTargets.All, -1, "Team Switch");
                                    }
                                }
                            }
                            else if (inputLine.Substring(6) == "0" || inputLine.Substring(6) == "individual")
                            {
                                FengGameManagerMKII.instance.photonView.RPC("setTeamRPC", targetPlayer: PhotonNetwork.player, 0);
                                addLINE("<color=#00FF00>You have joined individuals.</color>");
                                foreach (var obj4 in GameObject.FindGameObjectsWithTag("Player"))
                                {
                                    if (obj4.GetPhotonView().isMine)
                                    {
                                        obj4.GetComponent<HERO>().markDie();
                                        obj4.GetComponent<HERO>().photonView.RPC("netDie2", target: PhotonTargets.All, -1, "Team Switch");
                                    }
                                }
                            }
                            else
                            {
                                addLINE("<color=#FFCC00>error: invalid team code. Accepted values are 0,1, and 2.</color>");
                            }
                        }
                        else
                        {
                            addLINE("<color=#FFCC00>error: teams are locked or disabled. </color>");
                        }
                    }
                    else if (inputLine == "/restart")
                    {
                        if (PhotonNetwork.isMasterClient)
                        {
                            objArray3 = new object[] { "<color=#FFCC00>MasterClient has restarted the game!</color>", "" };
                            FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray3);
                            FengGameManagerMKII.instance.restartRC();
                        }
                        else
                        {
                            addLINE("<color=#FFCC00>error: not master client</color>");
                        }
                    }
                    else if (inputLine.StartsWith("/specmode"))
                    {
                        if ((int)FengGameManagerMKII.settings[0xf5] == 0)
                        {
                            FengGameManagerMKII.settings[0xf5] = 1;
                            FengGameManagerMKII.instance.EnterSpecMode(true);
                            addLINE("<color=#FFCC00>You have entered spectator mode.</color>");
                        }
                        else
                        {
                            FengGameManagerMKII.settings[0xf5] = 0;
                            FengGameManagerMKII.instance.EnterSpecMode(false);
                            addLINE("<color=#FFCC00>You have exited spectator mode.</color>");
                        }
                    }
                    else if (inputLine.StartsWith("/fov"))
                    {
                        var num6 = Convert.ToInt32(inputLine.Substring(5));
                        Camera.main.fieldOfView = num6;
                        addLINE("<color=#FFCC00>Field of vision set to " + num6 + ".</color>");
                    }
                    else if (inputLine == "/colliders")
                    {
                        var num7 = 0;
                        foreach (TITAN titan in FengGameManagerMKII.instance.getTitans())
                        {
                            if (titan.myTitanTrigger.isCollide)
                                num7++;
                        }

                        FengGameManagerMKII.instance.chatRoom.addLINE(num7.ToString());
                    }
                    else
                    {
                        int num8;
                        if (inputLine.StartsWith("/spectate"))
                        {
                            num8 = Convert.ToInt32(inputLine.Substring(10));
                            foreach (var obj5 in GameObject.FindGameObjectsWithTag("Player"))
                            {
                                if (obj5.GetPhotonView().owner.ID == num8)
                                {
                                    Camera.main.GetComponent<IN_GAME_MAIN_CAMERA>().setMainObject(obj: obj5);
                                    Camera.main.GetComponent<IN_GAME_MAIN_CAMERA>().setSpectorMode(false);
                                }
                            }
                        }
                        else if (!inputLine.StartsWith("/kill"))
                        {
                            object[] objArray5;
                            if (inputLine.StartsWith("/revive"))
                            {
                                if (PhotonNetwork.isMasterClient)
                                {
                                    if (inputLine == "/reviveall")
                                    {
                                        objArray5 = new object[] { "<color=#FFCC00>All players have been revived.</color>", string.Empty };
                                        FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray5);
                                        foreach (var player in PhotonNetwork.playerList)
                                        {
                                            if (player.customProperties[key: PhotonPlayerProperty.dead] != null && RCextensions.returnBoolFromObject(player.customProperties[key: PhotonPlayerProperty.dead]) && RCextensions.returnIntFromObject(player.customProperties[key: PhotonPlayerProperty.isTitan]) != 2)
                                                FengGameManagerMKII.instance.photonView.RPC("respawnHeroInNewRound", targetPlayer: player);
                                        }
                                    }
                                    else
                                    {
                                        num8 = Convert.ToInt32(inputLine.Substring(8));
                                        foreach (var player in PhotonNetwork.playerList)
                                        {
                                            if (player.ID == num8)
                                            {
                                                addLINE("<color=#FFCC00>Player " + num8 + " has been revived.</color>");
                                                if (player.customProperties[key: PhotonPlayerProperty.dead] != null && RCextensions.returnBoolFromObject(player.customProperties[key: PhotonPlayerProperty.dead]) && RCextensions.returnIntFromObject(player.customProperties[key: PhotonPlayerProperty.isTitan]) != 2)
                                                {
                                                    objArray5 = new object[] { "<color=#FFCC00>You have been revived by the master client.</color>", string.Empty };
                                                    FengGameManagerMKII.instance.photonView.RPC("Chat", targetPlayer: player, parameters: objArray5);
                                                    FengGameManagerMKII.instance.photonView.RPC("respawnHeroInNewRound", targetPlayer: player);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    addLINE("<color=#FFCC00>error: not master client</color>");
                                }
                            }
                            else if (inputLine.StartsWith("/unban"))
                            {
                                if (FengGameManagerMKII.OnPrivateServer)
                                {
                                    FengGameManagerMKII.ServerRequestUnban(inputLine.Substring(7));
                                }
                                else if (PhotonNetwork.isMasterClient)
                                {
                                    var num9 = Convert.ToInt32(inputLine.Substring(7));
                                    if (FengGameManagerMKII.banHash.ContainsKey(key: num9))
                                    {
                                        objArray5 = new object[] { "<color=#FFCC00>" + (string)FengGameManagerMKII.banHash[key: num9] + " has been unbanned from the server. </color>", string.Empty };
                                        FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray5);
                                        FengGameManagerMKII.banHash.Remove(key: num9);
                                    }
                                    else
                                    {
                                        addLINE("error: no such player");
                                    }
                                }
                                else
                                {
                                    addLINE("<color=#FFCC00>error: not master client</color>");
                                }
                            }
                            else if (inputLine.StartsWith("/rules"))
                            {
                                addLINE("<color=#FFCC00>Currently activated gamemodes:</color>");
                                if (RCSettings.bombMode > 0)
                                    addLINE("<color=#FFCC00>Bomb mode is on.</color>");
                                if (RCSettings.teamMode > 0)
                                {
                                    if (RCSettings.teamMode == 1)
                                        addLINE("<color=#FFCC00>Team mode is on (no sort).</color>");
                                    else if (RCSettings.teamMode == 2)
                                        addLINE("<color=#FFCC00>Team mode is on (sort by size).</color>");
                                    else if (RCSettings.teamMode == 3)
                                        addLINE("<color=#FFCC00>Team mode is on (sort by skill).</color>");
                                }

                                if (RCSettings.pointMode > 0)
                                    addLINE("<color=#FFCC00>Point mode is on (" + Convert.ToString(value: RCSettings.pointMode) + ").</color>");
                                if (RCSettings.disableRock > 0)
                                    addLINE("<color=#FFCC00>Punk Rock-Throwing is disabled.</color>");
                                if (RCSettings.spawnMode > 0)
                                    addLINE("<color=#FFCC00>Custom spawn rate is on (" + RCSettings.nRate.ToString("F2") + "% Normal, " + RCSettings.aRate.ToString("F2") + "% Abnormal, " + RCSettings.jRate.ToString("F2") + "% Jumper, " + RCSettings.cRate.ToString("F2") + "% Crawler, " + RCSettings.pRate.ToString("F2")
                                            + "% Punk </color>");
                                if (RCSettings.explodeMode > 0)
                                    addLINE("<color=#FFCC00>Titan explode mode is on (" + Convert.ToString(value: RCSettings.explodeMode) + ").</color>");
                                if (RCSettings.healthMode > 0)
                                    addLINE("<color=#FFCC00>Titan health mode is on (" + Convert.ToString(value: RCSettings.healthLower) + "-" + Convert.ToString(value: RCSettings.healthUpper) + ").</color>");
                                if (RCSettings.infectionMode > 0)
                                    addLINE("<color=#FFCC00>Infection mode is on (" + Convert.ToString(value: RCSettings.infectionMode) + ").</color>");
                                if (RCSettings.damageMode > 0)
                                    addLINE("<color=#FFCC00>Minimum nape damage is on (" + Convert.ToString(value: RCSettings.damageMode) + ").</color>");
                                if (RCSettings.moreTitans > 0)
                                    addLINE("<color=#FFCC00>Custom titan # is on (" + Convert.ToString(value: RCSettings.moreTitans) + ").</color>");
                                if (RCSettings.sizeMode > 0)
                                    addLINE("<color=#FFCC00>Custom titan size is on (" + RCSettings.sizeLower.ToString("F2") + "," + RCSettings.sizeUpper.ToString("F2") + ").</color>");
                                if (RCSettings.banEren > 0)
                                    addLINE("<color=#FFCC00>Anti-Eren is on. Using Titan eren will get you kicked.</color>");
                                if (RCSettings.waveModeOn == 1)
                                    addLINE("<color=#FFCC00>Custom wave mode is on (" + Convert.ToString(value: RCSettings.waveModeNum) + ").</color>");
                                if (RCSettings.friendlyMode > 0)
                                    addLINE("<color=#FFCC00>Friendly-Fire disabled. PVP is prohibited.</color>");
                                if (RCSettings.pvpMode > 0)
                                {
                                    if (RCSettings.pvpMode == 1)
                                        addLINE("<color=#FFCC00>AHSS/Blade PVP is on (team-based).</color>");
                                    else if (RCSettings.pvpMode == 2)
                                        addLINE("<color=#FFCC00>AHSS/Blade PVP is on (FFA).</color>");
                                }

                                if (RCSettings.maxWave > 0)
                                    addLINE("<color=#FFCC00>Max Wave set to " + RCSettings.maxWave + "</color>");
                                if (RCSettings.horseMode > 0)
                                    addLINE("<color=#FFCC00>Horses are enabled.</color>");
                                if (RCSettings.ahssReload > 0)
                                    addLINE("<color=#FFCC00>AHSS Air-Reload disabled.</color>");
                                if (RCSettings.punkWaves > 0)
                                    addLINE("<color=#FFCC00>Punk override every 5 waves enabled.</color>");
                                if (RCSettings.endlessMode > 0)
                                    addLINE("<color=#FFCC00>Endless Respawn is enabled (" + RCSettings.endlessMode + " seconds).</color>");
                                if (RCSettings.globalDisableMinimap > 0)
                                    addLINE("<color=#FFCC00>Minimaps are disabled.</color>");
                                if (RCSettings.motd != string.Empty)
                                    addLINE("<color=#FFCC00>MOTD:" + RCSettings.motd + "</color>");
                                if (RCSettings.deadlyCannons > 0)
                                    addLINE("<color=#FFCC00>Cannons will kill humans.</color>");
                            }
                            else
                            {
                                object[] objArray6;
                                bool flag2;
                                object[] objArray7;
                                if (inputLine.StartsWith("/kick"))
                                {
                                    num8 = Convert.ToInt32(inputLine.Substring(6));
                                    if (num8 == PhotonNetwork.player.ID)
                                    {
                                        addLINE("error:can't kick yourself.");
                                    }
                                    else if (!(FengGameManagerMKII.OnPrivateServer || PhotonNetwork.isMasterClient))
                                    {
                                        objArray6 = new object[] { "/kick #" + Convert.ToString(value: num8), LoginFengKAI.player.name };
                                        FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray6);
                                    }
                                    else
                                    {
                                        flag2 = false;
                                        foreach (var player3 in PhotonNetwork.playerList)
                                        {
                                            if (num8 == player3.ID)
                                            {
                                                flag2 = true;
                                                if (FengGameManagerMKII.OnPrivateServer)
                                                {
                                                    FengGameManagerMKII.instance.kickPlayerRC(player: player3, false, "");
                                                }
                                                else if (PhotonNetwork.isMasterClient)
                                                {
                                                    FengGameManagerMKII.instance.kickPlayerRC(player: player3, false, "");
                                                    objArray7 = new object[] { "<color=#FFCC00>" + RCextensions.returnStringFromObject(player3.customProperties[key: PhotonPlayerProperty.name]) + " has been kicked from the server!</color>", string.Empty };
                                                    FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray7);
                                                }
                                            }
                                        }

                                        if (!flag2)
                                            addLINE("error:no such player.");
                                    }
                                }
                                else if (inputLine.StartsWith("/ban"))
                                {
                                    if (inputLine == "/banlist")
                                    {
                                        addLINE("<color=#FFCC00>List of banned players:</color>");
                                        foreach (int num10 in FengGameManagerMKII.banHash.Keys)
                                            addLINE("<color=#FFCC00>" + Convert.ToString(value: num10) + ":" + (string)FengGameManagerMKII.banHash[key: num10] + "</color>");
                                    }
                                    else
                                    {
                                        num8 = Convert.ToInt32(inputLine.Substring(5));
                                        if (num8 == PhotonNetwork.player.ID)
                                        {
                                            addLINE("error:can't kick yourself.");
                                        }
                                        else if (!(FengGameManagerMKII.OnPrivateServer || PhotonNetwork.isMasterClient))
                                        {
                                            objArray6 = new object[] { "/kick #" + Convert.ToString(value: num8), LoginFengKAI.player.name };
                                            FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray6);
                                        }
                                        else
                                        {
                                            flag2 = false;
                                            foreach (var player3 in PhotonNetwork.playerList)
                                            {
                                                if (num8 == player3.ID)
                                                {
                                                    flag2 = true;
                                                    if (FengGameManagerMKII.OnPrivateServer)
                                                    {
                                                        FengGameManagerMKII.instance.kickPlayerRC(player: player3, true, "");
                                                    }
                                                    else if (PhotonNetwork.isMasterClient)
                                                    {
                                                        FengGameManagerMKII.instance.kickPlayerRC(player: player3, true, "");
                                                        objArray7 = new object[] { "<color=#FFCC00>" + RCextensions.returnStringFromObject(player3.customProperties[key: PhotonPlayerProperty.name]) + " has been banned from the server!</color>", string.Empty };
                                                        FengGameManagerMKII.instance.photonView.RPC("Chat", target: PhotonTargets.All, parameters: objArray7);
                                                    }
                                                }
                                            }

                                            if (!flag2)
                                                addLINE("error:no such player.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                inputLine = string.Empty;
                GUI.FocusControl(name: string.Empty);
                return;
            }

            inputLine = "\t";
            GUI.FocusControl("ChatInput");
        }

        Label_013D:
        GUI.SetNextControlName(name: string.Empty);
        GUILayout.BeginArea(screenRect: GuiRect);
        GUILayout.FlexibleSpace();
        var text = string.Empty;
        if (messages.Count < 15)
            for (num4 = 0; num4 < messages.Count; num4++)
                text = text + messages[index: num4] + "\n";
        else
            for (var i = messages.Count - 15; i < messages.Count; i++)
                text = text + messages[index: i] + "\n";
        GUILayout.Label(text: text);
        GUILayout.EndArea();
        GUILayout.BeginArea(screenRect: GuiRect2);
        GUILayout.BeginHorizontal();
        GUI.SetNextControlName("ChatInput");
        inputLine = GUILayout.TextField(text: inputLine);
        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    public void setPosition()
    {
        if (AlignBottom)
        {
            GuiRect = new Rect(0f, Screen.height - 500, 300f, 470f);
            GuiRect2 = new Rect(30f, Screen.height - 300 + 0x113, 300f, 25f);
        }
    }

    public void Start()
    {
        setPosition();
    }
}