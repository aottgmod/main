using System.Collections;
using UnityEngine;
using MonoBehaviour = Photon.MonoBehaviour;

public class PVPcheckPoint : MonoBehaviour
{
    public static ArrayList chkPts;
    private bool annie;
    public GameObject[] chkPtNextArr;
    public GameObject[] chkPtPreviousArr;
    private readonly float getPtsInterval = 20f;
    private float getPtsTimer;
    public bool hasAnnie;
    private float hitTestR = 15f;
    public GameObject humanCyc;
    public float humanPt;
    public float humanPtMax = 40f;
    public int id;
    public bool isBase;
    public int normalTitanRate = 70;
    private bool playerOn;
    public float size = 1f;
    private float spawnTitanTimer;
    public CheckPointState state;
    private GameObject supply;
    private readonly float syncInterval = 0.6f; //todo: smooth me out
    private float syncTimer;
    public GameObject titanCyc;
    public float titanInterval = 30f;
    private bool titanOn;
    public float titanPt;
    public float titanPtMax = 40f;

    public GameObject chkPtNext
    {
        get
        {
            if (chkPtNextArr.Length <= 0)
                return null;
            return chkPtNextArr[Random.Range(0, max: chkPtNextArr.Length)];
        }
    }

    public GameObject chkPtPrevious
    {
        get
        {
            if (chkPtPreviousArr.Length <= 0)
                return null;
            return chkPtPreviousArr[Random.Range(0, max: chkPtPreviousArr.Length)];
        }
    }

    [RPC]
    private void changeHumanPt(float pt)
    {
        humanPt = pt;
    }

    [RPC]
    private void changeState(int num)
    {
        if (num == 0)
            state = CheckPointState.Non;
        if (num == 1)
            state = CheckPointState.Human;
        if (num == 2)
            state = CheckPointState.Titan;
    }

    [RPC]
    private void changeTitanPt(float pt)
    {
        titanPt = pt;
    }

    private void checkIfBeingCapture()
    {
        int num;
        playerOn = false;
        titanOn = false;
        var objArray = GameObject.FindGameObjectsWithTag("Player");
        var objArray2 = GameObject.FindGameObjectsWithTag("titan");
        for (num = 0; num < objArray.Length; num++)
        {
            if (Vector3.Distance(a: objArray[num].transform.position, b: transform.position) < hitTestR)
            {
                playerOn = true;
                if (state == CheckPointState.Human && objArray[num].GetPhotonView().isMine)
                {
                    if (GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().checkpoint != gameObject)
                    {
                        GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().checkpoint = gameObject;
                        GameObject.Find("Chatroom").GetComponent<InRoomChat>().addLINE("<color=#A8FF24>Respawn point changed to point" + id + "</color>");
                    }

                    break;
                }
            }
        }

        for (num = 0; num < objArray2.Length; num++)
        {
            if (Vector3.Distance(a: objArray2[num].transform.position, b: transform.position) < hitTestR + 5f && (objArray2[num].GetComponent<TITAN>() == null || !objArray2[num].GetComponent<TITAN>().hasDie))
            {
                titanOn = true;
                if (state == CheckPointState.Titan && objArray2[num].GetPhotonView().isMine && objArray2[num].GetComponent<TITAN>() != null && objArray2[num].GetComponent<TITAN>().nonAI)
                {
                    if (GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().checkpoint != gameObject)
                    {
                        GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().checkpoint = gameObject;
                        GameObject.Find("Chatroom").GetComponent<InRoomChat>().addLINE("<color=#A8FF24>Respawn point changed to point" + id + "</color>");
                    }

                    break;
                }
            }
        }
    }

    private bool checkIfHumanWins()
    {
        for (var i = 0; i < chkPts.Count; i++)
        {
            if ((chkPts[index: i] as PVPcheckPoint).state != CheckPointState.Human)
                return false;
        }

        return true;
    }

    private bool checkIfTitanWins()
    {
        for (var i = 0; i < chkPts.Count; i++)
        {
            if ((chkPts[index: i] as PVPcheckPoint).state != CheckPointState.Titan)
                return false;
        }

        return true;
    }

    private float getHeight(Vector3 pt)
    {
        RaycastHit hit;
        LayerMask mask2 = 1 << LayerMask.NameToLayer("Ground");
        if (Physics.Raycast(origin: pt, direction: -Vector3.up, hitInfo: out hit, 1000f, layerMask: mask2.value))
            return hit.point.y;
        return 0f;
    }

    public string getStateString()
    {
        if (state == CheckPointState.Human)
            return "[" + ColorSet.color_human + "]H[-]";
        if (state == CheckPointState.Titan)
            return "[" + ColorSet.color_titan_player + "]T[-]";
        return "[" + ColorSet.color_D + "]_[-]";
    }

    private void humanGetsPoint()
    {
        if (humanPt >= humanPtMax)
        {
            humanPt = humanPtMax;
            titanPt = 0f;
            syncPts();
            state = CheckPointState.Human;
            object[] parameters = { 1 };
            photonView.RPC("changeState", target: PhotonTargets.All, parameters: parameters);
            if (LevelInfo.getInfo(name: FengGameManagerMKII.level).mapName != "The City I")
                supply = PhotonNetwork.Instantiate("aot_supply", transform.position - Vector3.up * (transform.position.y - getHeight(pt: transform.position)), rotation: transform.rotation, 0);
            var component = GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>();
            component.PVPhumanScore += 2;
            GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().checkPVPpts();
            if (checkIfHumanWins())
                GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().gameWin2();
        }
        else
        {
            humanPt += Time.deltaTime;
        }
    }

    private void humanLosePoint()
    {
        if (humanPt > 0f)
        {
            humanPt -= Time.deltaTime * 3f;
            if (humanPt <= 0f)
            {
                humanPt = 0f;
                syncPts();
                if (state != CheckPointState.Titan)
                {
                    state = CheckPointState.Non;
                    object[] parameters = { 0 };
                    photonView.RPC("changeState", target: PhotonTargets.Others, parameters: parameters);
                }
            }
        }
    }

    private void newTitan()
    {
        var obj2 = GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().spawnTitan(rate: normalTitanRate, transform.position - Vector3.up * (transform.position.y - getHeight(pt: transform.position)), rotation: transform.rotation);
        if (LevelInfo.getInfo(name: FengGameManagerMKII.level).mapName == "The City I")
            obj2.GetComponent<TITAN>().chaseDistance = 120f;
        else
            obj2.GetComponent<TITAN>().chaseDistance = 200f;
        obj2.GetComponent<TITAN>().PVPfromCheckPt = this;
    }

    private void Start()
    {
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
        {
            Destroy(obj: gameObject);
        }
        else if (IN_GAME_MAIN_CAMERA.gamemode != GAMEMODE.PVP_CAPTURE)
        {
            if (photonView.isMine)
                Destroy(obj: gameObject);
            Destroy(obj: gameObject);
        }
        else
        {
            chkPts.Add(this);
            IComparer comparer = new IComparerPVPchkPtID();
            chkPts.Sort(comparer: comparer);
            if (humanPt == humanPtMax)
            {
                state = CheckPointState.Human;
                if (photonView.isMine && LevelInfo.getInfo(name: FengGameManagerMKII.level).mapName != "The City I")
                    supply = PhotonNetwork.Instantiate("aot_supply", transform.position - Vector3.up * (transform.position.y - getHeight(pt: transform.position)), rotation: transform.rotation, 0);
            }
            else if (photonView.isMine && !hasAnnie)
            {
                if (Random.Range(0, 100) < 50)
                {
                    var num = Random.Range(1, 2);
                    for (var i = 0; i < num; i++)
                        newTitan();
                }

                if (isBase)
                    newTitan();
            }

            if (titanPt == titanPtMax)
                state = CheckPointState.Titan;
            hitTestR = 15f * size;
            transform.localScale = new Vector3(x: size, y: size, z: size);
        }
    }

    private void syncPts()
    {
        object[] parameters = { titanPt };
        photonView.RPC("changeTitanPt", target: PhotonTargets.Others, parameters: parameters);
        object[] objArray2 = { humanPt };
        photonView.RPC("changeHumanPt", target: PhotonTargets.Others, parameters: objArray2);
    }

    private void titanGetsPoint()
    {
        if (titanPt >= titanPtMax)
        {
            titanPt = titanPtMax;
            humanPt = 0f;
            syncPts();
            if (state == CheckPointState.Human && supply != null)
                PhotonNetwork.Destroy(targetGo: supply);
            state = CheckPointState.Titan;
            object[] parameters = { 2 };
            photonView.RPC("changeState", target: PhotonTargets.All, parameters: parameters);
            var component = GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>();
            component.PVPtitanScore += 2;
            GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().checkPVPpts();
            if (checkIfTitanWins())
                GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().gameLose2();
            if (hasAnnie)
            {
                if (!annie)
                {
                    annie = true;
                    PhotonNetwork.Instantiate("FEMALE_TITAN", transform.position - Vector3.up * (transform.position.y - getHeight(pt: transform.position)), rotation: transform.rotation, 0);
                }
                else
                {
                    newTitan();
                }
            }
            else
            {
                newTitan();
            }
        }
        else
        {
            titanPt += Time.deltaTime;
        }
    }

    private void titanLosePoint()
    {
        if (titanPt > 0f)
        {
            titanPt -= Time.deltaTime * 3f;
            if (titanPt <= 0f)
            {
                titanPt = 0f;
                syncPts();
                if (state != CheckPointState.Human)
                {
                    state = CheckPointState.Non;
                    object[] parameters = { 0 };
                    photonView.RPC("changeState", target: PhotonTargets.All, parameters: parameters);
                }
            }
        }
    }

    private void Update()
    {
        var x = humanPt / humanPtMax;
        var num2 = titanPt / titanPtMax;
        if (!photonView.isMine)
        {
            x = humanPt / humanPtMax;
            num2 = titanPt / titanPtMax;
            humanCyc.transform.localScale = new Vector3(x: x, y: x, 1f);
            titanCyc.transform.localScale = new Vector3(x: num2, y: num2, 1f);
            syncTimer += Time.deltaTime;
            if (syncTimer > syncInterval)
            {
                syncTimer = 0f;
                checkIfBeingCapture();
            }
        }
        else
        {
            if (state == CheckPointState.Non)
            {
                if (playerOn && !titanOn)
                {
                    humanGetsPoint();
                    titanLosePoint();
                }
                else if (titanOn && !playerOn)
                {
                    titanGetsPoint();
                    humanLosePoint();
                }
                else
                {
                    humanLosePoint();
                    titanLosePoint();
                }
            }
            else if (state == CheckPointState.Human)
            {
                if (titanOn && !playerOn)
                    titanGetsPoint();
                else
                    titanLosePoint();
                getPtsTimer += Time.deltaTime;
                if (getPtsTimer > getPtsInterval)
                {
                    getPtsTimer = 0f;
                    if (!isBase)
                    {
                        var component = GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>();
                        component.PVPhumanScore++;
                    }

                    GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().checkPVPpts();
                }
            }
            else if (state == CheckPointState.Titan)
            {
                if (playerOn && !titanOn)
                    humanGetsPoint();
                else
                    humanLosePoint();
                getPtsTimer += Time.deltaTime;
                if (getPtsTimer > getPtsInterval)
                {
                    getPtsTimer = 0f;
                    if (!isBase)
                    {
                        var local2 = GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>();
                        local2.PVPtitanScore++;
                    }

                    GameObject.Find("MultiplayerManager").GetComponent<FengGameManagerMKII>().checkPVPpts();
                }

                spawnTitanTimer += Time.deltaTime;
                if (spawnTitanTimer > titanInterval)
                {
                    spawnTitanTimer = 0f;
                    if (LevelInfo.getInfo(name: FengGameManagerMKII.level).mapName == "The City I")
                    {
                        if (GameObject.FindGameObjectsWithTag("titan").Length < 12)
                            newTitan();
                    }
                    else if (GameObject.FindGameObjectsWithTag("titan").Length < 20)
                    {
                        newTitan();
                    }
                }
            }

            syncTimer += Time.deltaTime;
            if (syncTimer > syncInterval)
            {
                syncTimer = 0f;
                checkIfBeingCapture();
                syncPts();
            }

            x = humanPt / humanPtMax;
            num2 = titanPt / titanPtMax;
            humanCyc.transform.localScale = new Vector3(x: x, y: x, 1f);
            titanCyc.transform.localScale = new Vector3(x: num2, y: num2, 1f);
        }
    }
}