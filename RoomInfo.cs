using ExitGames.Client.Photon;

public class RoomInfo
{
    protected bool autoCleanUpField = PhotonNetwork.autoCleanUpPlayerObjects;
    protected byte maxPlayersField;
    protected string nameField;
    protected bool openField = true;
    protected bool visibleField = true;

    protected internal RoomInfo(string roomName, Hashtable properties)
    {
        CacheProperties(propertiesToCache: properties);
        nameField = roomName;
    }

    public Hashtable customProperties { get; } = new Hashtable();

    public bool isLocalClientInside { get; set; }

    public byte maxPlayers => maxPlayersField;

    public string name => nameField;

    public bool open => openField;

    public int playerCount { get; private set; }

    public bool removedFromList { get; internal set; }

    public bool visible => visibleField;

    protected internal void CacheProperties(Hashtable propertiesToCache)
    {
        if (propertiesToCache != null && propertiesToCache.Count != 0 && !customProperties.Equals(obj: propertiesToCache))
        {
            if (propertiesToCache.ContainsKey((byte)0xfb))
            {
                removedFromList = (bool)propertiesToCache[(byte)0xfb];
                if (removedFromList)
                    return;
            }

            if (propertiesToCache.ContainsKey((byte)0xff))
                maxPlayersField = (byte)propertiesToCache[(byte)0xff];
            if (propertiesToCache.ContainsKey((byte)0xfd))
                openField = (bool)propertiesToCache[(byte)0xfd];
            if (propertiesToCache.ContainsKey((byte)0xfe))
                visibleField = (bool)propertiesToCache[(byte)0xfe];
            if (propertiesToCache.ContainsKey((byte)0xfc))
                playerCount = (byte)propertiesToCache[(byte)0xfc];
            if (propertiesToCache.ContainsKey((byte)0xf9))
                autoCleanUpField = (bool)propertiesToCache[(byte)0xf9];
            customProperties.MergeStringKeys(addHash: propertiesToCache);
        }
    }

    public override bool Equals(object p)
    {
        var room = p as Room;
        return room != null && nameField.Equals(value: room.nameField);
    }

    public override int GetHashCode()
    {
        return nameField.GetHashCode();
    }

    public override string ToString()
    {
        object[] args = { nameField, !visibleField ? "hidden" : "visible", !openField ? "closed" : "open", maxPlayersField, playerCount };
        return string.Format("Room: '{0}' {1},{2} {4}/{3} players.", args: args);
    }

    public string ToStringFull()
    {
        object[] args = { nameField, !visibleField ? "hidden" : "visible", !openField ? "closed" : "open", maxPlayersField, playerCount, customProperties.ToStringFull() };
        return string.Format("Room: '{0}' {1},{2} {4}/{3} players.\ncustomProps: {5}", args: args);
    }
}