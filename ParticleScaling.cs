using UnityEngine;

public class ParticleScaling : MonoBehaviour
{
    public void OnWillRenderObject()
    {
        GetComponent<ParticleSystem>().renderer.material.SetVector("_Center", vector: transform.position);
        GetComponent<ParticleSystem>().renderer.material.SetVector("_Scaling", vector: transform.lossyScale);
        GetComponent<ParticleSystem>().renderer.material.SetMatrix("_Camera", matrix: Camera.current.worldToCameraMatrix);
        GetComponent<ParticleSystem>().renderer.material.SetMatrix("_CameraInv", matrix: Camera.current.worldToCameraMatrix.inverse);
    }
}