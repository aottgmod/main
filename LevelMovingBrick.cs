using UnityEngine;

public class LevelMovingBrick : MonoBehaviour
{
    private Vector3 pointA;
    private Vector3 pointB;
    public GameObject pointGOA;
    public GameObject pointGOB;
    public float speed = 10f;
    public bool towardsA = true;

    private void Start()
    {
        pointA = pointGOA.transform.position;
        pointB = pointGOB.transform.position;
        Destroy(obj: pointGOA);
        Destroy(obj: pointGOB);
    }

    private void Update()
    {
        if (towardsA)
        {
            transform.position = Vector3.MoveTowards(current: transform.position, target: pointA, speed * Time.deltaTime);
            if (Vector3.Distance(a: transform.position, b: pointA) < 2f)
                towardsA = false;
        }
        else
        {
            transform.position = Vector3.MoveTowards(current: transform.position, target: pointB, speed * Time.deltaTime);
            if (Vector3.Distance(a: transform.position, b: pointB) < 2f)
                towardsA = true;
        }
    }
}