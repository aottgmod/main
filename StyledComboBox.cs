using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class StyledComboBox : StyledItem
{
    public delegate void SelectionChangedHandler(StyledItem item);

    public StyledComboBoxPrefab containerPrefab;
    private bool isToggled;
    public StyledItem itemMenuPrefab;
    public StyledItem itemPrefab;

    [SerializeField]
    [HideInInspector]
    private List<StyledItem> items = new List<StyledItem>();

    public SelectionChangedHandler OnSelectionChanged;

    [SerializeField]
    [HideInInspector]
    private StyledComboBoxPrefab root;

    [SerializeField]
    private int selectedIndex;

    public int SelectedIndex
    {
        get => selectedIndex;
        set
        {
            if (value >= 0 && value <= items.Count)
            {
                selectedIndex = value;
                CreateMenuButton(data: items[index: selectedIndex].GetText().text);
            }
        }
    }

    public StyledItem SelectedItem
    {
        get
        {
            if (selectedIndex >= 0 && selectedIndex <= items.Count)
                return items[index: selectedIndex];
            return null;
        }
    }

    private void AddItem(object data)
    {
        if (itemPrefab != null)
        {
            var yf = new AddItemc__AnonStoreyF
            {
                f__this = this
            };
            var fourCornersArray = new Vector3[4];
            itemPrefab.GetComponent<RectTransform>().GetLocalCorners(fourCornersArray: fourCornersArray);
            var position = fourCornersArray[0];
            var num = position.y - fourCornersArray[2].y;
            position.y = items.Count * num;
            yf.styledItem = Instantiate(original: itemPrefab, position: position, rotation: root.itemRoot.rotation) as StyledItem;
            var component = yf.styledItem.GetComponent<RectTransform>();
            yf.styledItem.Populate(o: data);
            component.SetParent(parent: root.itemRoot.transform, false);
            component.pivot = new Vector2(0f, 1f);
            component.anchorMin = new Vector2(0f, 1f);
            component.anchorMax = Vector2.one;
            component.anchoredPosition = new Vector2(0f, y: position.y);
            items.Add(item: yf.styledItem);
            component.offsetMin = new Vector2(0f, position.y + num);
            component.offsetMax = new Vector2(0f, y: position.y);
            root.itemRoot.offsetMin = new Vector2(x: root.itemRoot.offsetMin.x, (items.Count + 2) * num);
            var button = yf.styledItem.GetButton();
            yf.curIndex = items.Count - 1;
            if (button != null)
                button.onClick.AddListener(call: yf.m__0);
        }
    }

    public void AddItems(params object[] list)
    {
        ClearItems();
        for (var i = 0; i < list.Length; i++)
            AddItem(list[i]);
        SelectedIndex = 0;
    }

    private void Awake()
    {
        InitControl();
    }

    public void ClearItems()
    {
        for (var i = items.Count - 1; i >= 0; i--)
            DestroyObject(obj: items[index: i].gameObject);
    }

    private void CreateMenuButton(object data)
    {
        if (root.menuItem.transform.childCount > 0)
            for (var i = root.menuItem.transform.childCount - 1; i >= 0; i--)
                DestroyObject(obj: root.menuItem.transform.GetChild(index: i).gameObject);
        if (itemMenuPrefab != null && root.menuItem != null)
        {
            var item = Instantiate(original: itemMenuPrefab) as StyledItem;
            item.Populate(o: data);
            item.transform.SetParent(parent: root.menuItem.transform, false);
            var component = item.GetComponent<RectTransform>();
            component.pivot = new Vector2(0.5f, 0.5f);
            component.anchorMin = Vector2.zero;
            component.anchorMax = Vector2.one;
            component.offsetMin = Vector2.zero;
            component.offsetMax = Vector2.zero;
            root.gameObject.hideFlags = HideFlags.HideInHierarchy;
            var button = item.GetButton();
            if (button != null)
                button.onClick.AddListener(call: TogglePanelState);
        }
    }

    public void InitControl()
    {
        if (root != null)
            DestroyImmediate(obj: root.gameObject);
        if (containerPrefab != null)
        {
            var component = GetComponent<RectTransform>();
            root = Instantiate(original: containerPrefab, position: component.position, rotation: component.rotation) as StyledComboBoxPrefab;
            root.transform.SetParent(parent: transform, false);
            var transform2 = root.GetComponent<RectTransform>();
            transform2.pivot = new Vector2(0.5f, 0.5f);
            transform2.anchorMin = Vector2.zero;
            transform2.anchorMax = Vector2.one;
            transform2.offsetMax = Vector2.zero;
            transform2.offsetMin = Vector2.zero;
            root.gameObject.hideFlags = HideFlags.HideInHierarchy;
            root.itemPanel.gameObject.SetActive(value: isToggled);
        }
    }

    public void OnItemClicked(StyledItem item, int index)
    {
        SelectedIndex = index;
        TogglePanelState();
        if (OnSelectionChanged != null)
            OnSelectionChanged(item: item);
    }

    public void TogglePanelState()
    {
        isToggled = !isToggled;
        root.itemPanel.gameObject.SetActive(value: isToggled);
    }

    [CompilerGenerated]
    private sealed class AddItemc__AnonStoreyF
    {
        internal int curIndex;
        internal StyledComboBox f__this;
        internal StyledItem styledItem;

        internal void m__0()
        {
            f__this.OnItemClicked(item: styledItem, index: curIndex);
        }
    }
}