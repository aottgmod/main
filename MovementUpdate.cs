using UnityEngine;

public class MovementUpdate : MonoBehaviour
{
    public bool disabled;
    private Vector3 lastPosition;
    private Quaternion lastRotation;
    private Vector3 lastVelocity;
    private Vector3 targetPosition;

    private void Start()
    {
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
        {
            disabled = true;
            enabled = false;
        }
        else if (networkView.isMine)
        {
            object[] args = { transform.position, transform.rotation, transform.localScale, Vector3.zero };
            networkView.RPC("updateMovement", mode: RPCMode.OthersBuffered, args: args);
        }
        else
        {
            targetPosition = transform.position;
        }
    }

    private void Update()
    {
        if (!disabled && Network.peerType != NetworkPeerType.Disconnected && Network.peerType != NetworkPeerType.Connecting)
        {
            if (networkView.isMine)
            {
                if (Vector3.Distance(a: transform.position, b: lastPosition) >= 0.5f)
                {
                    lastPosition = transform.position;
                    object[] args = { transform.position, transform.rotation, transform.localScale, rigidbody.velocity };
                    networkView.RPC("updateMovement", mode: RPCMode.Others, args: args);
                }
                else if (Vector3.Distance(a: transform.rigidbody.velocity, b: lastVelocity) >= 0.1f)
                {
                    lastVelocity = transform.rigidbody.velocity;
                    object[] objArray2 = { transform.position, transform.rotation, transform.localScale, rigidbody.velocity };
                    networkView.RPC("updateMovement", mode: RPCMode.Others, args: objArray2);
                }
                else if (Quaternion.Angle(a: transform.rotation, b: lastRotation) >= 1f)
                {
                    lastRotation = transform.rotation;
                    object[] objArray3 = { transform.position, transform.rotation, transform.localScale, rigidbody.velocity };
                    networkView.RPC("updateMovement", mode: RPCMode.Others, args: objArray3);
                }
            }
            else
            {
                transform.position = Vector3.Slerp(from: transform.position, to: targetPosition, Time.deltaTime * 2f);
            }
        }
    }

    [RPC]
    private void updateMovement(Vector3 newPosition, Quaternion newRotation, Vector3 newScale, Vector3 veloctiy)
    {
        targetPosition = newPosition;
        transform.rotation = newRotation;
        transform.localScale = newScale;
        rigidbody.velocity = veloctiy;
    }
}