using UnityEngine;

public class InputToEvent : MonoBehaviour
{
    public static Vector3 inputHitPos;
    public bool DetectPointedAtGameObject;
    private GameObject lastGo;

    public static GameObject goPointedAt { get; set; }

    private void Press(Vector2 screenPos)
    {
        lastGo = RaycastObject(screenPos: screenPos);
        if (lastGo != null)
            lastGo.SendMessage("OnPress", options: SendMessageOptions.DontRequireReceiver);
    }

    private GameObject RaycastObject(Vector2 screenPos)
    {
        RaycastHit hit;
        if (Physics.Raycast(camera.ScreenPointToRay(position: screenPos), hitInfo: out hit, 200f))
        {
            inputHitPos = hit.point;
            return hit.collider.gameObject;
        }

        return null;
    }

    private void Release(Vector2 screenPos)
    {
        if (lastGo != null)
        {
            if (RaycastObject(screenPos: screenPos) == lastGo)
                lastGo.SendMessage("OnClick", options: SendMessageOptions.DontRequireReceiver);
            lastGo.SendMessage("OnRelease", options: SendMessageOptions.DontRequireReceiver);
            lastGo = null;
        }
    }

    private void Update()
    {
        if (DetectPointedAtGameObject)
            goPointedAt = RaycastObject(screenPos: Input.mousePosition);
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
                Press(screenPos: touch.position);
            else if (touch.phase == TouchPhase.Ended)
                Release(screenPos: touch.position);
        }
        else
        {
            if (Input.GetMouseButtonDown(0))
                Press(screenPos: Input.mousePosition);
            if (Input.GetMouseButtonUp(0))
                Release(screenPos: Input.mousePosition);
        }
    }
}