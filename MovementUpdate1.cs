using UnityEngine;

public class MovementUpdate1 : MonoBehaviour
{
    public bool disabled;
    public Vector3 lastPosition;
    public Quaternion lastRotation;
    public Vector3 lastVelocity;

    private void Start()
    {
        if (IN_GAME_MAIN_CAMERA.gametype == GAMETYPE.SINGLE)
        {
            disabled = true;
            enabled = false;
        }
        else if (networkView.isMine)
        {
            object[] args = { transform.position, transform.rotation, transform.lossyScale };
            networkView.RPC("updateMovement1", mode: RPCMode.OthersBuffered, args: args);
        }
        else
        {
            enabled = false;
        }
    }

    private void Update()
    {
        if (!disabled)
        {
            object[] args = { transform.position, transform.rotation, transform.lossyScale };
            networkView.RPC("updateMovement1", mode: RPCMode.Others, args: args);
        }
    }

    [RPC]
    private void updateMovement1(Vector3 newPosition, Quaternion newRotation, Vector3 newScale)
    {
        transform.position = newPosition;
        transform.rotation = newRotation;
        transform.localScale = newScale;
    }
}