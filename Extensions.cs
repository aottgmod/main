using System;
using System.Collections;
using ExitGames.Client.Photon;
using UnityEngine;
using Hashtable = ExitGames.Client.Photon.Hashtable;

public static class Extensions
{
    public static bool AlmostEquals(this float target, float second, float floatDiff)
    {
        return Mathf.Abs(target - second) < floatDiff;
    }

    public static bool AlmostEquals(this Quaternion target, Quaternion second, float maxAngle)
    {
        return Quaternion.Angle(a: target, b: second) < maxAngle;
    }

    public static bool AlmostEquals(this Vector2 target, Vector2 second, float sqrMagnitudePrecision)
    {
        var vector = target - second;
        return vector.sqrMagnitude < sqrMagnitudePrecision;
    }

    public static bool AlmostEquals(this Vector3 target, Vector3 second, float sqrMagnitudePrecision)
    {
        var vector = target - second;
        return vector.sqrMagnitude < sqrMagnitudePrecision;
    }

    public static bool Contains(this int[] target, int nr)
    {
        if (target != null)
            for (var i = 0; i < target.Length; i++)
            {
                if (target[i] == nr)
                    return true;
            }

        return false;
    }

    public static PhotonView GetPhotonView(this GameObject go)
    {
        return go.GetComponent<PhotonView>();
    }

    public static PhotonView[] GetPhotonViewsInChildren(this GameObject go)
    {
        return go.GetComponentsInChildren<PhotonView>(true);
    }

    public static void Merge(this IDictionary target, IDictionary addHash)
    {
        if (addHash != null && !target.Equals(obj: addHash))
        {
            var enumerator = addHash.Keys.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current;
                    target[key: current] = addHash[key: current];
                }
            } finally
            {
                var disposable = enumerator as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
        }
    }

    public static void MergeStringKeys(this IDictionary target, IDictionary addHash)
    {
        if (addHash != null && !target.Equals(obj: addHash))
        {
            var enumerator = addHash.Keys.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current;
                    if (current is string)
                        target[key: current] = addHash[key: current];
                }
            } finally
            {
                var disposable = enumerator as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
        }
    }

    public static void StripKeysWithNullValues(this IDictionary original)
    {
        var objArray = new object[original.Count];
        var num = 0;
        var enumerator = original.Keys.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                var current = enumerator.Current;
                objArray[num++] = current;
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }

        for (var i = 0; i < objArray.Length; i++)
        {
            var key = objArray[i];
            if (original[key: key] == null)
                original.Remove(key: key);
        }
    }

    public static Hashtable StripToStringKeys(this IDictionary original)
    {
        var hashtable = new Hashtable();
        var enumerator = original.GetEnumerator();
        try
        {
            while (enumerator.MoveNext())
            {
                if (enumerator.Current != null)
                {
                    var current = (DictionaryEntry)enumerator.Current;
                    if (current.Key is string)
                        hashtable[key: current.Key] = current.Value;
                }
            }
        } finally
        {
            var disposable = enumerator as IDisposable;
            if (disposable != null)
                disposable.Dispose();
        }

        return hashtable;
    }

    public static string ToStringFull(this IDictionary origin)
    {
        return SupportClass.DictionaryToString(dictionary: origin, false);
    }
}