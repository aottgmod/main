using System.Collections;
using UnityEngine;

public class LoginFengKAI : MonoBehaviour
{
    public static PlayerInfoPHOTON player;
    private static string playerGUILDName = string.Empty;
    private static string playerName = string.Empty;
    private static string playerPassword = string.Empty;
    private readonly string ChangeGuildURL = "http://aotskins.com/version/guild.php";
    private readonly string ChangePasswordURL = "http://fenglee.com/game/aog/change_password.php";
    private readonly string CheckUserURL = "http://aotskins.com/version/login.php";
    private readonly string ForgetPasswordURL = "http://fenglee.com/game/aog/forget_password.php";
    private readonly string GetInfoURL = "http://aotskins.com/version/getinfo.php";
    private readonly string RegisterURL = "http://fenglee.com/game/aog/signup_check.php";
    public string formText = string.Empty;
    public PanelLoginGroupManager loginGroup;
    public GameObject output;
    public GameObject output2;
    public GameObject panelChangeGUILDNAME;
    public GameObject panelChangePassword;
    public GameObject panelForget;
    public GameObject panelLogin;
    public GameObject panelRegister;
    public GameObject panelStatus;

    public void cGuild(string name)
    {
        if (playerName == string.Empty)
        {
            logout();
            NGUITools.SetActive(go: panelChangeGUILDNAME, false);
            NGUITools.SetActive(go: panelLogin, true);
            output.GetComponent<UILabel>().text = "Please sign in.";
            return;
        }

        StartCoroutine(changeGuild(name: name));
    }

    private IEnumerator changeGuild(string name)
    {
        var wwwform = new WWWForm();
        wwwform.AddField("name", value: playerName);
        wwwform.AddField("guildname", value: name);
        var www = new WWW(url: ChangeGuildURL, form: wwwform);
        yield return www;
        if (www.error != null)
            goto IL_10B;
        output.GetComponent<UILabel>().text = www.text;
        if (!www.text.Contains("Guild name set."))
            goto IL_FE;
        NGUITools.SetActive(go: panelChangeGUILDNAME, false);
        NGUITools.SetActive(go: panelStatus, true);
        StartCoroutine(getInfo());
        IL_FE:
        www.Dispose();
        goto IL_11B;
        IL_10B:
        print(message: www.error);
        IL_11B: ;
    }

    private IEnumerator changePassword(string oldpassword, string password, string password2)
    {
        var wwwform = new WWWForm();
        wwwform.AddField("userid", value: playerName);
        wwwform.AddField("old_password", value: oldpassword);
        wwwform.AddField("password", value: password);
        wwwform.AddField("password2", value: password2);
        var www = new WWW(url: ChangePasswordURL, form: wwwform);
        yield return www;
        if (www.error != null)
            goto IL_123;
        output.GetComponent<UILabel>().text = www.text;
        if (!www.text.Contains("Thanks, Your password changed successfully"))
            goto IL_116;
        NGUITools.SetActive(go: panelChangePassword, false);
        NGUITools.SetActive(go: panelLogin, true);
        IL_116:
        www.Dispose();
        goto IL_133;
        IL_123:
        print(message: www.error);
        IL_133: ;
    }

    private void clearCOOKIE()
    {
        playerName = string.Empty;
        playerPassword = string.Empty;
    }

    public void cpassword(string oldpassword, string password, string password2)
    {
        if (playerName == string.Empty)
        {
            logout();
            NGUITools.SetActive(go: panelChangePassword, false);
            NGUITools.SetActive(go: panelLogin, true);
            output.GetComponent<UILabel>().text = "Please sign in.";
            return;
        }

        StartCoroutine(changePassword(oldpassword: oldpassword, password: password, password2: password2));
    }

    private IEnumerator ForgetPassword(string email)
    {
        var wwwform = new WWWForm();
        wwwform.AddField("email", value: email);
        var www = new WWW(url: ForgetPasswordURL, form: wwwform);
        yield return www;
        if (www.error != null)
            goto IL_C8;
        output.GetComponent<UILabel>().text = www.text;
        www.Dispose();
        NGUITools.SetActive(go: panelForget, false);
        NGUITools.SetActive(go: panelLogin, true);
        goto IL_D8;
        IL_C8:
        print(message: www.error);
        IL_D8:
        clearCOOKIE();
    }

    private IEnumerator getInfo()
    {
        var wwwform = new WWWForm();
        wwwform.AddField("userid", value: playerName);
        wwwform.AddField("password", value: playerPassword);
        var www = new WWW(url: GetInfoURL, form: wwwform);
        yield return www;
        if (www.error != null)
            goto IL_177;
        if (!www.text.Contains("Error,please sign in again."))
            goto IL_FF;
        NGUITools.SetActive(go: panelLogin, true);
        NGUITools.SetActive(go: panelStatus, false);
        output.GetComponent<UILabel>().text = www.text;
        playerName = string.Empty;
        playerPassword = string.Empty;
        goto IL_16A;
        IL_FF:
        char[] separator =
        {
            '|'
        };
        var array = www.text.Split(separator: separator);
        playerGUILDName = array[0];
        output2.GetComponent<UILabel>().text = array[1];
        player.name = playerName;
        player.guildname = playerGUILDName;
        IL_16A:
        www.Dispose();
        goto IL_187;
        IL_177:
        print(message: www.error);
        IL_187: ;
    }

    public void login(string name, string password)
    {
        StartCoroutine(Login(name: name, password: password));
    }

    private IEnumerator Login(string name, string password)
    {
        var wwwform = new WWWForm();
        wwwform.AddField("userid", value: name);
        wwwform.AddField("password", value: password);
        wwwform.AddField("version", value: UIMainReferences.version);
        var www = new WWW(url: CheckUserURL, form: wwwform);
        yield return www;
        clearCOOKIE();
        if (www.error != null)
            goto IL_175;
        output.GetComponent<UILabel>().text = www.text;
        formText = www.text;
        www.Dispose();
        if (!formText.Contains("Welcome back") || !formText.Contains("(^o^)/~"))
            goto IL_185;
        NGUITools.SetActive(go: panelLogin, false);
        NGUITools.SetActive(go: panelStatus, true);
        playerName = name;
        playerPassword = password;
        StartCoroutine(getInfo());
        goto IL_185;
        IL_175:
        print(message: www.error);
        IL_185: ;
    }

    public void logout()
    {
        clearCOOKIE();
        player = new PlayerInfoPHOTON();
        player.initAsGuest();
        output.GetComponent<UILabel>().text = "Welcome," + player.name;
    }

    private IEnumerator Register(string name, string password, string password2, string email)
    {
        var wwwform = new WWWForm();
        wwwform.AddField("userid", value: name);
        wwwform.AddField("password", value: password);
        wwwform.AddField("password2", value: password2);
        wwwform.AddField("email", value: email);
        var www = new WWW(url: RegisterURL, form: wwwform);
        yield return www;
        if (www.error != null)
            goto IL_124;
        output.GetComponent<UILabel>().text = www.text;
        if (!www.text.Contains("Final step,to activate your account, please click the link in the activation email"))
            goto IL_117;
        NGUITools.SetActive(go: panelRegister, false);
        NGUITools.SetActive(go: panelLogin, true);
        IL_117:
        www.Dispose();
        goto IL_134;
        IL_124:
        print(message: www.error);
        IL_134:
        clearCOOKIE();
    }

    public void resetPassword(string email)
    {
        StartCoroutine(ForgetPassword(email: email));
    }

    public void signup(string name, string password, string password2, string email)
    {
        StartCoroutine(Register(name: name, password: password, password2: password2, email: email));
    }

    private void Start()
    {
        if (player == null)
        {
            player = new PlayerInfoPHOTON();
            player.initAsGuest();
        }

        if (playerName != string.Empty)
        {
            NGUITools.SetActive(go: panelLogin, false);
            NGUITools.SetActive(go: panelStatus, true);
            StartCoroutine(getInfo());
            return;
        }

        output.GetComponent<UILabel>().text = "Welcome," + player.name;
    }
}