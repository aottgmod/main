using ExitGames.Client.Photon;
using UnityEngine;

public class Room : RoomInfo
{
    internal Room(string roomName, RoomOptions options) : base(roomName: roomName, null)
    {
        if (options == null)
            options = new RoomOptions();
        visibleField = options.isVisible;
        openField = options.isOpen;
        maxPlayersField = (byte)options.maxPlayers;
        autoCleanUpField = false;
        CacheProperties(propertiesToCache: options.customRoomProperties);
        propertiesListedInLobby = options.customRoomPropertiesForLobby;
    }

    public bool autoCleanUp => autoCleanUpField;

    public new int maxPlayers
    {
        get => maxPlayersField;
        set
        {
            if (!Equals(p: PhotonNetwork.room))
                Debug.LogWarning("Can't set maxPlayers when not in that room.");
            if (value > 0xff)
            {
                Debug.LogWarning("Can't set Room.maxPlayers to: " + value + ". Using max value: 255.");
                value = 0xff;
            }

            if (value != maxPlayersField && !PhotonNetwork.offlineMode)
            {
                var gameProperties = new Hashtable();
                gameProperties.Add((byte)0xff, (byte)value);
                PhotonNetwork.networkingPeer.OpSetPropertiesOfRoom(gameProperties: gameProperties, true, 0);
            }

            maxPlayersField = (byte)value;
        }
    }

    public new string name
    {
        get => nameField;
        internal set => nameField = value;
    }

    public new bool open
    {
        get => openField;
        set
        {
            if (!Equals(p: PhotonNetwork.room))
                Debug.LogWarning("Can't set open when not in that room.");
            if (value != openField && !PhotonNetwork.offlineMode)
            {
                var gameProperties = new Hashtable();
                gameProperties.Add((byte)0xfd, value: value);
                PhotonNetwork.networkingPeer.OpSetPropertiesOfRoom(gameProperties: gameProperties, true, 0);
            }

            openField = value;
        }
    }

    public new int playerCount
    {
        get
        {
            if (PhotonNetwork.playerList != null)
                return PhotonNetwork.playerList.Length;
            return 0;
        }
    }

    public string[] propertiesListedInLobby { get; private set; }

    public new bool visible
    {
        get => visibleField;
        set
        {
            if (!Equals(p: PhotonNetwork.room))
                Debug.LogWarning("Can't set visible when not in that room.");
            if (value != visibleField && !PhotonNetwork.offlineMode)
            {
                var gameProperties = new Hashtable();
                gameProperties.Add((byte)0xfe, value: value);
                PhotonNetwork.networkingPeer.OpSetPropertiesOfRoom(gameProperties: gameProperties, true, 0);
            }

            visibleField = value;
        }
    }

    public void SetCustomProperties(Hashtable propertiesToSet)
    {
        if (propertiesToSet != null)
        {
            customProperties.MergeStringKeys(addHash: propertiesToSet);
            customProperties.StripKeysWithNullValues();
            var gameProperties = propertiesToSet.StripToStringKeys();
            if (!PhotonNetwork.offlineMode)
                PhotonNetwork.networkingPeer.OpSetCustomPropertiesOfRoom(gameProperties: gameProperties, true, 0);
            object[] parameters = { propertiesToSet };
            NetworkingPeer.SendMonoMessage(methodString: PhotonNetworkingMessage.OnPhotonCustomRoomPropertiesChanged, parameters: parameters);
        }
    }

    public void SetPropertiesListedInLobby(string[] propsListedInLobby)
    {
        var gameProperties = new Hashtable();
        gameProperties[(byte)250] = propsListedInLobby;
        PhotonNetwork.networkingPeer.OpSetPropertiesOfRoom(gameProperties: gameProperties, false, 0);
        propertiesListedInLobby = propsListedInLobby;
    }

    public override string ToString()
    {
        object[] args = { nameField, !visibleField ? "hidden" : "visible", !openField ? "closed" : "open", maxPlayersField, playerCount };
        return string.Format("Room: '{0}' {1},{2} {4}/{3} players.", args: args);
    }

    public new string ToStringFull()
    {
        object[] args = { nameField, !visibleField ? "hidden" : "visible", !openField ? "closed" : "open", maxPlayersField, playerCount, customProperties.ToStringFull() };
        return string.Format("Room: '{0}' {1},{2} {4}/{3} players.\ncustomProps: {5}", args: args);
    }
}