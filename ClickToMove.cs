using UnityEngine;

public class ClickToMove : MonoBehaviour
{
    public int smooth;
    private Vector3 targetPosition;

    public void Main() { }

    public void Update()
    {
        if (Input.GetKeyDown(key: KeyCode.Mouse0))
        {
            var plane = new Plane(inNormal: Vector3.up, inPoint: transform.position);
            var ray = Camera.main.ScreenPointToRay(position: Input.mousePosition);
            var enter = 0f;
            if (plane.Raycast(ray: ray, enter: out enter))
            {
                var point = ray.GetPoint(distance: enter);
                targetPosition = ray.GetPoint(distance: enter);
                var quaternion = Quaternion.LookRotation(point - transform.position);
                transform.rotation = quaternion;
            }
        }

        transform.position = Vector3.Lerp(from: transform.position, to: targetPosition, Time.deltaTime * smooth);
    }
}